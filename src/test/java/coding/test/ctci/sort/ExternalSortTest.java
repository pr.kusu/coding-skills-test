package coding.test.ctci.sort;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ExternalSortTest {

    @Test
    public void testExternal() {
        List<Integer> original = new ArrayList<>();
        Random rand = new Random();

        for (int i = 1; i < 10; i++) {
            original.add(rand.nextInt(15));
        }

        System.out.println("Original list...");
        original.forEach(val -> System.out.print(val + ","));

        ExternalSort.externalSort(original, 3);
    }

    @Test
    public void testExternalSort() {
        int[] original = new int[10];
        int[] original2 = new int[10];

        Random rand = new Random();

        for (int i = 0; i < 10; i++) {
            original[i] = (rand.nextInt(15));
            original2[i] = (rand.nextInt(15));
        }

        System.out.println("Original list...");
        Arrays.sort(original);
        Arrays.sort(original2);
        Arrays.stream(original).forEach(val -> System.out.print(val + ","));
        System.out.println();
        Arrays.stream(original2).forEach(val -> System.out.print(val + ","));
        System.out.println();

        int[] sorted = MergeSortedArray.merge(original, original2);

        Arrays.stream(sorted).forEach(val -> System.out.print(val + ","));
    }

    @Test
    public void fileTrf() {
        int[][] grid = new int[][]{ //1,1
                {2, 1, 1},
                {0, 1, 1},
                {1, 0, 1}
        };

        int minTime = FileTransfer.transfer(grid);

        System.out.println(minTime);
    }
}
