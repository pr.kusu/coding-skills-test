package coding.test.ctci.sort;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class FileTransfer {
    public static int transfer(int[][] grid) {


        return minTransferTime(grid, grid.length, grid[0].length);
    }

    static class Pair<F, S> {
        F fst;
        S snd;

        public Pair(F fst, S snd) {
            this.fst = fst;
            this.snd = snd;
        }
    }

    public static int minTransferTime(int[][] serverGrid, int row, int col) {
        int minTime = 0;
        int solvedCount = 0;
        int emptyCount = 0;

        Set<Pair<Integer, Integer>> tofill = new HashSet<>();
        Set<Pair<Integer, Integer>> empty = new HashSet<>();

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (serverGrid[i][j] == 0) {
                    emptyCount++;
                } else if (serverGrid[i][j] == 1) {
                    if (checkGrid(serverGrid, i, j, 2)) {
                        tofill.add(new Pair<>(i, j));
                    } else if (checkGrid(serverGrid, i, j, 1)) {
                        empty.add(new Pair<>(i, j));
                    }
                } else {
                    solvedCount++;
                }
            }
        }

        minTime++;

        while (!empty.isEmpty() || !tofill.isEmpty()) {
            tofill.forEach(val -> {
                serverGrid[val.fst][val.snd] = 2;
            });

            solvedCount += tofill.size();

            tofill.clear();

            Set<Pair<Integer, Integer>> newEmpty = new HashSet<>();

            empty.forEach(val -> {
                if (checkGrid(serverGrid, val.fst, val.snd, 2)) {
                    tofill.add(val);
                } else if (checkGrid(serverGrid, val.fst, val.snd, 1)) {
                    newEmpty.add(val);
                }
            });

            empty = newEmpty;

            minTime++;

            if ((solvedCount + emptyCount) < row * col || minTime >= row * col) {
                return -1;
            }
        }

        if ((solvedCount + emptyCount) < row * col) {
            return -1;
        }

        return minTime - 1;
    }

    private static void printArr(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(Arrays.toString(arr[i]));
            System.out.println();
        }
    }

    private static boolean checkGrid(int[][] serverGrid, int row, int col, int target) {

        if (serverGrid[row][col] == 0) {
            return false;
        }

        List<Pair<Integer, Integer>> neighbors = Arrays.asList(new Pair<>(row - 1, col),
                new Pair<>(row + 1, col), new Pair<>(row, col + 1), new Pair<>(row, col - 1));

        for (Pair<Integer, Integer> neighbor : neighbors) {
            if (neighbor.fst >= 0 && neighbor.snd >= 0 && neighbor.fst < serverGrid.length && neighbor.snd < serverGrid[0].length) {
                int rowIndex = neighbor.fst;
                int colIndex = neighbor.snd;

                if (serverGrid[rowIndex][colIndex] == target) {
                    return true;
                }
            }
        }

        return false;
    }
}