package coding.test.ctci.sort;

import org.junit.Test;

import java.util.Random;

public class SearchDuplicateTest {

    @Test
    public void testSearchDups() {
        int[] array = new int[20];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10) + 1;
            System.out.print(array[i] + ",");
        }

        SearchDuplicate.printDuplicate(array);

    }
}
