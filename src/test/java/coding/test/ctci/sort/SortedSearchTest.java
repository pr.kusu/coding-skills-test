package coding.test.ctci.sort;

import org.junit.Test;

public class SortedSearchTest {

    @Test
    public void sortedSearchTest() {

        int[] array = new int[100];

        for (int i = 0; i < 100; i++) {
            array[i] = i;
        }

        int index = SortedSearch.sortedSearch(array, 75);

        System.out.println("Found At:" + index);
    }
}
