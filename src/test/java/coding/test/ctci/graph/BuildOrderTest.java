package coding.test.ctci.graph;

import org.junit.Test;

import java.util.Stack;

public class BuildOrderTest {

    @Test
    public void testBuildOrder() {

        String[] projects = new String[]{"a", "b", "c", "d", "e", "f"};

        // // (a, d), (f, b), (b, d), (f, a), (d, c)
        String[][] dependencies = new String[][]{{"a", "d"}, {"f", "b"}, {"b", "d"}, {"f", "a"}, {"d", "c"},};


        Stack<Project> stack = BuildOrder.findBuildOrder(projects, dependencies);

        stack.forEach(val -> System.out.print(val.name+" "));
    }
}
