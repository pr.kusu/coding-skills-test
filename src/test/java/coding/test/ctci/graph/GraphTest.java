package coding.test.ctci.graph;

import org.junit.Test;

public class GraphTest {

    @Test
    public void testDFSSearch() {

        DFSSearch dfsSearch = new DFSSearch();
        dfsSearch.search(buildGraph());

    }

    @Test
    public void testBFSSearch() {

        BFSSearch bfsSearch = new BFSSearch();
        bfsSearch.search(buildGraph());

    }

    private GraphNode buildGraph() {
        GraphNode node0 = new GraphNode(0);
        GraphNode node1 = new GraphNode(1);
        GraphNode node2 = new GraphNode(2);
        GraphNode node3 = new GraphNode(3);
        GraphNode node4 = new GraphNode(4);
        GraphNode node5 = new GraphNode(5);

        node0.addAll(node1, node4, node5);
        node1.addAll(node3, node4);
        node2.addAll(node1);
        node3.addAll(node2, node4);

        return node0;
    }
}
