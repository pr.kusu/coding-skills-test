package coding.test.ctci.graph;

import org.junit.Test;

import java.util.Arrays;

public class FloodFillTest {

    @Test
    public void floodFillTest() {

        int[][] grid = new int[][]{ //1,1
                {0, 0, 0},
                {0, 1, 1}
        };

        int[][] fill = FloodFill.floodFill(grid, 1, 1, 1);

        for (int[] grid2 : fill) {
            System.out.print(Arrays.toString(grid2));
            System.out.println();
        }

        int[][] dfsFill=FloodFill.floodFillDFS(grid,1,1,2);

        for (int[] grid2 : dfsFill) {
            System.out.print(Arrays.toString(grid2));
            System.out.println();
        }
    }
}
