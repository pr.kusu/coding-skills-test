package coding.test.ctci.dynamic;

import org.junit.Test;

import java.util.List;


public class PermutationsTest {

    @Test
    public void testPerms() {
        List<String> pers = Permutations.getPermutations("abcd");

        pers.forEach(System.out::println);
    }
}
