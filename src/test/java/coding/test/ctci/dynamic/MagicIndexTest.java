package coding.test.ctci.dynamic;

import org.junit.Test;

public class MagicIndexTest {

    @Test
    public void magicIndexTest() {
        int[] arr = {-4, -3, -2, -1, 0, 1, 2, 3, 4, 9, 10, 11};

        int index = MagicIndex.findMagicIndex(arr);

        System.out.println(index);
    }
}
