package coding.test.ctci.dynamic;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ParenTest {

    @Test
    public void testParen() {

        List<String> arr = new ArrayList<>();

        Parentheses.generate(arr, 3, 3, new char[6], 0);

        arr.forEach(val -> System.out.println(val));
    }
}
