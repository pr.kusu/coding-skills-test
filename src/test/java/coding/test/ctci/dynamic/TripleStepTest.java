package coding.test.ctci.dynamic;

import org.junit.Test;

public class TripleStepTest {

    @Test
    public void testTripleStep() {

        int[] memo = new int[100];

        int val = TripleStep.getWays(10, memo);

        System.out.println(val);
    }
}
