package coding.test.ctci.tree;

import org.junit.Assert;
import org.junit.Test;


public class ValidateBSTreeTest {

    @Test
    public void testValidateBST() {
        TreeNode<Integer> bstTree = buildBSTTree();

        Assert.assertTrue(ValidateBST.validate(bstTree));

        TreeNode<Integer> nonBst = buildTree();
        Assert.assertFalse(ValidateBST.validate(nonBst));
    }

    private TreeNode<Integer> buildTree() {
        int[] array = new int[]{11, 2, 3, 14, 5, 6, 7, 8, 9};
        TreeNode<Integer> root = MinHeightTree.buildTree(array, 0, array.length);

        BTreePrinter.printNode(root);
        return root;
    }

    private TreeNode<Integer> buildBSTTree() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};

        TreeNode<Integer> root = MinHeightTree.buildTree(array, 0, array.length);

        BTreePrinter.printNode(root);
        return root;
    }
}
