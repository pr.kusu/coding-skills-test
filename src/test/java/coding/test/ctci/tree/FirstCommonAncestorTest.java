package coding.test.ctci.tree;

import org.junit.Test;

public class FirstCommonAncestorTest {

    @Test
    public void testCommonAncestor() {

        int[] array2 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        TreeNode<Integer> root2 = MinHeightTree.buildTree(array2, 0, array2.length);

        BTreePrinter.printNode(root2);

        TreeNode<Integer> two = root2.left.left.right;

        TreeNode<Integer> five = root2.left.right.right;

        TreeNode<Integer> ancestor = FirstCommonAncestor.commonAncestor(root2, two, five);

        System.out.println("Common: " + ancestor.val);
    }
}
