package coding.test.job;


public class MarketAxess {

    public boolean reachingPoints(int sx, int sy, int tx, int ty) {

        if (sx == tx && sy == ty) return true;
        if (sx > tx || sy > ty || tx == ty) return false;

        return reachingPoints(sx, sx + sy, tx, ty) || reachingPoints(sx + sy, sy, tx, ty);

    }

}
