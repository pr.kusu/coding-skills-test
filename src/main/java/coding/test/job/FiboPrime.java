package coding.test.job;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FiboPrime {

    public static String solution(String word, String cipher) {
        // Type your solution here
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        Map<Character, Character> cipherMap = new HashMap<>();
        Set<Character> cipherSet=new HashSet<>();

        for (int i = 0; i < cipher.length(); i++) {
            if(cipherSet.contains(cipher.charAt(i))){
                return "";
            }

            cipherMap.put(alpha.charAt(i), cipher.charAt(i));
            cipherSet.add(cipher.charAt(i));

        }

        if (cipher.length() < 26) {
            return "";
        }

        StringBuilder builder = new StringBuilder();

        for (char c : word.toCharArray()) {
            if(!cipherMap.containsKey(c)){
                return "";
            }

            builder.append(cipherMap.get(c));
        }

        return builder.toString();
    }

    public static void main(String [] args){
        System.out.println(solution("helloworld","bjosxcqukmrhgeynazlwfpvti"));
    }
}
