package coding.test.job;

import java.util.ArrayList;
import java.util.List;

public class DFSTest {

    //A <-> B
    private List<Graph> visited = new ArrayList<>();

    class Graph {
        int val;
        List<Graph> parent;

        List<Graph> neighbors;
    }

    public boolean dfs(Graph start, Graph target) {
        if (start == null) {
            return false;
        }

        if (start == target) {
            return true;
        }

        if (visited.contains(start)) {
            return false;
        }

        boolean found = false;

        for (Graph next : start.neighbors) {
            found = dfs(next, target);

            if (found) {
                break;
            }
        }

        visited.add(start);

        return found;
    }

}
