package coding.test.job;

public class CommonSubString {

    public boolean commonSubString(String s1, String s2) {
        int l1 = s1.length();
        int l2 = s2.length();
        int len = Math.min(l1, l2);
        int[] hash = new int[26];

        for (int i = 0; i < len; i++) {
            hash[s1.charAt(i) - 'a']++;

            if (hash[s2.charAt(i) - 'a'] > 1) {
                return true;
            }
        }

        String s = l1 > l2 ? s1 : s2;

        for (int i = 0; i < s.length(); i++) {
            if (hash[s.charAt(i) - 'a'] > 1) {
                return true;
            }
        }

        return false;
    }
}
