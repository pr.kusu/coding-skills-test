package coding.test.job;

import java.util.Stack;

public class Brackets {

    public static String solution(String brackets) {
        Stack<Character> stack = new Stack<>();

        int closeCount = 0;

        for (int i = 0; i < brackets.toCharArray().length; i++) {

            char cur = brackets.toCharArray()[i];

            if (cur == '<') {
                stack.push(cur);
            } else {
                if (!stack.isEmpty()) {
                    stack.pop();
                } else {
                    closeCount++;
                }
            }
        }

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < closeCount; i++) {
            builder.append("<");
        }
        builder.append(brackets);

        while (!stack.isEmpty()) {
            builder.append(">");
            stack.pop();
        }

        return builder.toString();
    }

    public static void main(String[] args) {

        System.out.println(solution("<<>>>>><<<>>"));
    }
}
