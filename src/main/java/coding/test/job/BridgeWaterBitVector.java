package coding.test.job;

import java.util.*;

public class BridgeWaterBitVector {

    private static boolean isSpecial(String word) {
        return word.toLowerCase().matches("(?:[a-z]\\.){2,}") || word.toLowerCase().matches("^[a-z]\\.");
        //return word.toLowerCase().equals("i.e.") || word.toLowerCase().equals("e.g.")
          //      || word.toLowerCase().equals("mr.") || word.toLowerCase().equalsIgnoreCase("x.y.z.");

    }


    public static void generateAndPrintConcordance(List<String> inputLines) {
        TreeMap<String, List<Integer>> map = new TreeMap<>();
        int sentenceIndex = 1;

        for (String line : inputLines) {

            String[] words = line.trim().split(" ");

            for (String word : words) {

                boolean sentenceEnd = false;

                boolean special = isSpecial(word);

                word = word.replaceAll(",", "");
                if ((word.endsWith(".") || word.endsWith("!") || word.endsWith("?") )&& !special) {
                    word = word.substring(0,word.length() - 1);
                    sentenceEnd = true;
                }
                map.computeIfAbsent(word.toLowerCase(), k -> new ArrayList<>()).add(sentenceIndex);

                if (sentenceEnd) {
                    sentenceIndex++;
                }
            }

        }

        for (String word : map.keySet()) {

            if (word.equals(" ") || word.equals(".")) {
                continue;
            }

            System.out.print(word + ": {" + map.get(word).size() + ":");
            int i = 0;
            for (int line_num : map.get(word)) {
                i++;
                System.out.print(line_num);
                if (i < map.get(word).size()) {
                    System.out.print(",");
                }
            }
            System.out.println("}");
        }
    }

    private static void processSentence(Map<String, List<Integer>> sentenceMap, String sentence, int sentenceIndex) {

        String[] words = sentence.trim().split(" ");

        for (String word : words) {
            word = word.replaceAll(",", "");
            sentenceMap.computeIfAbsent(word.toLowerCase(), k -> new ArrayList<>()).add(sentenceIndex);
        }
    }

    public static void main(String[] args) {

        String text1 = "Does this solution handle";
        String text2 = "abbreviations e.g. or x.y.z.";
        String text3 = "or Mr. Smith correctly?";

        generateAndPrintConcordance(Arrays.asList(text1, text2, text3));
    }
}
