package coding.test.job;

public class Stock {

    public static long solution(long[] prices) {
        // Type your solution here
        long minBuyPrice = Long.MAX_VALUE;
        long profitSoFar = Long.MIN_VALUE;

        for (long price : prices) {

            long profit = price - minBuyPrice;

            if (profit > profitSoFar) {
                profitSoFar = profit;
            }

            if (price < minBuyPrice) {
                minBuyPrice = price;
            }
        }

        return profitSoFar;
    }
}
