package coding.test.job;

public class RoundPrice {

    private static long roundPrice(int price, int n) {
        //408581
        int div = (int) Math.pow(10, n);
        div *= price < 0 ? -1 : 1;
        int floor = (price / div) * div;

        if (price / Math.pow(10, n - 1) > 4) {
            floor += div;
        }

        return floor == 0 ? div : floor;
    }

    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            System.out.println(roundPrice(21, i));
        }

    }
}
