package coding.test.job;

import java.util.*;

public class LeastRepeat {

    public static long[] solution(long[] numbers) {
        // Type your solution here

        Map<Long, Integer> freqMap = new HashMap<>();

        for (long val : numbers) {
            if (freqMap.containsKey(val)) {
                freqMap.put(val, freqMap.get(val) + 1);
            } else {
                freqMap.put(val, 1);
            }
        }

        List<Long> result = new ArrayList<>();
        int minFreq = numbers.length;

        for (Map.Entry<Long, Integer> entry : freqMap.entrySet()) {

            int currentFreq = entry.getValue();

            if (currentFreq < minFreq) {
                result.clear();
            }

            if (currentFreq <= minFreq) {
                result.add(entry.getKey());
                minFreq = currentFreq;
            }

        }

        long[] finalResult = new long[result.size()];
        int i = 0;

        for (Long value : result) {
            finalResult[i++] = value;
        }

        return finalResult;
    }



    public static void main(String[] args) {
        long[] sol = solution(new long[]{1, 2, 2, 2, 3, 3});

        System.out.println(Arrays.toString(sol));
    }
}
