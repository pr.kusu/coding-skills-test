package coding.test.ebay;

public class DiagnolTraverse {

    public int[] findDiagonalOrder(int[][] matrix) {
        if (matrix.length == 0) {
            return new int[0];
        }

        int m = matrix.length;
        int n = matrix[0].length;
        int[] answer = new int[m * n];

        int row = 0;
        int col = 0;
        int index = 0;
        boolean up = true;

        while (index < answer.length) {
            answer[index++] = matrix[row][col];
            if (up) {
                if (col == n - 1) {
                    row++;
                    up = false;
                } else if (row == 0) {
                    col++;
                    up = false;
                } else {
                    row--;
                    col++;
                }
            } else {
                if (row == m - 1) {
                    col++;
                    up = true;
                } else if (col == 0) {
                    row++;
                    up = true;
                } else {
                    col--;
                    row++;
                }
            }
        }

        return answer;
    }
}
