package coding.test.ebay;

import coding.test.leetcode.curated.TreeNode;

public class UniValueTrees {

    public int countUnivalSubtrees(TreeNode root) {

        int[] res = new int[1];
        helper(root, res);

        return res[0];
    }

    private boolean helper(TreeNode root, int[] sum) {
        if (root == null) {
            return true;
        }

        boolean left = helper(root.left, sum);
        boolean right = helper(root.right, sum);

        if (left && right) {
            if (root.left != null && root.val != root.left.val) {
                return false;
            }

            boolean result = root.right == null || root.val == root.right.val;
            if (result) {
                sum[0]++;
            }
            return result;
        }

        return false;
    }
}
