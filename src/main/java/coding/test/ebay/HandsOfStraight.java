package coding.test.ebay;

import java.util.HashMap;
import java.util.Map;

public class HandsOfStraight {

    public boolean isNStraightHand(int[] hand, int W) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int num : hand) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        for (int num : hand) {
            // since the array is not sorted we check if there is a previous number of 'num' still to be processed.
            // if there is then we skip this 'num' as it will be processed in the sequence that will start at num-1 (or smaller).
            // this helps us to avoid the TreeMap

            if (map.getOrDefault(num - 1, 0) == 0 && map.get(num) != 0) {
                int count = 0;
                while (count < W) {
                    int next = num + count;
                    if (map.getOrDefault(next, 0) == 0) {
                        return false;
                    }
                    map.put(next, map.get(next) - 1);
                    count++;
                }
            }
        }
        return true;
    }
}
