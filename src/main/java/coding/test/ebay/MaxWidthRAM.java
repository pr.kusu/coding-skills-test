package coding.test.ebay;

import java.util.Stack;

public class MaxWidthRAM {

    public int maxWidthRamp(int[] A) {
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < A.length; i++) {
            if (stack.isEmpty() || A[i] <= A[stack.peek()]) {
                stack.push(i);
            }
        }

        int max = 0;
        int i = A.length - 1;

        while (!stack.isEmpty()) {
            if (A[i] >= A[stack.peek()]) {
                max = Math.max(max, i - stack.pop());
            } else {
                i--;
            }
        }

        return max;
    }
}
