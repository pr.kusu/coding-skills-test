package coding.test.ebay;

import coding.test.leetcode.curated.TreeNode;

import java.util.Deque;
import java.util.LinkedList;

public class MaxHeight {

    public int widthOfBinaryTree(TreeNode root) {
        if (root == null) {
            return 0;
        }

        Deque<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        int max = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            max = Math.max(max, size);

            for (int i = 0; i < size; i++) {
                TreeNode current = queue.poll();

                if (current != null) {
                    queue.add(current.left);
                    queue.add(current.right);
                } else {
                    queue.add(null);
                    queue.add(null);
                }
            }

            while (!queue.isEmpty() && queue.peekFirst() == null) {
                queue.pollFirst();
            }
            while (!queue.isEmpty() && queue.peekLast() == null) {
                queue.pollLast();
            }

        }

        return max;
    }
}
