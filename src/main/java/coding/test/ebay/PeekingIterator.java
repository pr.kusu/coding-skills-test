package coding.test.ebay;

import java.util.Iterator;

class PeekingIterator implements Iterator<Integer> {
    private Integer store;
    private Iterator<Integer> it;

    public PeekingIterator(Iterator<Integer> iterator) {
        // initialize any member here.
        this.it = iterator;
    }

    // Returns the next element in the iteration without advancing the iterator.
    public Integer peek() {
        if (store != null) {
            return store;
        } else if (hasNext()) {
            store = it.next();
        }

        return store;
    }

    // hasNext() and next() should behave the same as in the Iterator interface.
    // Override them if needed.
    @Override
    public Integer next() {
        if (store != null) {
            int val = store;
            store = null;
            return val;
        }
        return hasNext() ? it.next() : null;
    }

    @Override
    public boolean hasNext() {
        if (store != null) {
            return true;
        }

        return it.hasNext();
    }
}
