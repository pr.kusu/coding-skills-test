package coding.test.ebay;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClosestKElements {

    public List<Integer> findClosestElements(int[] arr, int k, int x) {
        int left = 0;
        int right = arr.length - k;

        while (left < right) {

            int mid = (left + right) / 2;

            if (x - arr[mid] < arr[mid + k] - x) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }

        return Arrays.stream(arr, left, left + k).boxed().collect(Collectors.toList());
    }
}
