package com.me.leetcode.algo.easy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author kusu
 * <p>
 * https://leetcode.com/problems/valid-palindrome/
 */
public class ValidPalindrome {

    public static boolean isAlpha(char c) {
        return Character.isDigit(c) || Character.isLetter(c);
    }

    public static boolean isPalindrome(String s) {
        char[] arr = s.toCharArray();
        int len = arr.length;

        int start = 0;
        int end = s.length() - 1;
        int mid = s.length() / 2;

        for (int i = 0; i < mid; i++) {
            while (start < len && !isAlpha(arr[start])) {
                start++;
            }

            while (end >= 0 && !isAlpha(arr[end])) {
                end--;
            }

            if (start > end) {
                return true;
            }

            System.out.println(arr[start] + "+ " + arr[end]);

            if (Character.toLowerCase(arr[start]) != Character.toLowerCase(arr[end])) {
                return false;
            } else {
                start++;
                end--;
            }

        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome("A man, a plan, a  canal: Panama"));
        //	System.out.println(isPalindrome("0P"));
        String[] pals = new String[]{"level", "radar", "darconian"};
        List<String> list=finPalindromeAndSort(pals,"rad");
        System.out.println(Arrays.toString(list.toArray()));
    }

    public static List<String> finPalindromeAndSort(String[] arr, String substring) {
        List<String> palindromes = new ArrayList<>();
        for (String s : arr) {
            if (isPalindrome(s) && s.toLowerCase().contains(substring.toLowerCase())) {
                palindromes.add(s);
            }
        }

        try{
            System.out.println();
        }catch (NullPointerException e){
            e.getMessage();
        }
        catch (Exception e){
            e.getMessage();
        }

        Collections.sort(palindromes);
        return palindromes;
    }

}
