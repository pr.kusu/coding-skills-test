package coding.test.leetcode.curated;

public class IntegerToWord {

    private final String[] belowTen = new String[]{"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
    private final String[] belowTwenty = new String[]{"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen",
            "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
    private final String[] belowHundred = new String[]{"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
            "Seventy", "Eighty", "Ninety"};

    public String numberToWords(int num) {
        if (num == 0) {
            return "Zero";
        }
        if (num < 10) {
            return belowTen[num % 10];
        }
        if (num < 20) {
            return belowTwenty[num % 10];
        }
        if (num < 100) {
            String ten = "";
            if (num % 10 > 0) {
                ten = " " + numberToWords(num % 10);
            }
            return belowHundred[num / 10] + ten;
        }

        int billion = num / 1000000000;
        int millions = num % 1000000000;
        int million = millions / 1000000;
        int thousands = millions % 1000000;
        int thousand = thousands / 1000;
        int hundreds = thousands % 1000;
        int hundred = hundreds / 100;
        int tens = hundreds % 100;

        StringBuilder result = new StringBuilder();

        if (billion > 0) {
            result.append(numberToWords(billion)).append(" Billion ");
        }
        if (million > 0) {
            result.append(numberToWords(million)).append(" Million ");
        }
        if (thousand > 0) {
            result.append(numberToWords(thousand)).append(" Thousand ");
        }
        if (hundred > 0) {
            result.append(numberToWords(hundred)).append(" Hundred ");
        }
        if (tens > 0) {
            result.append(numberToWords(tens)).append(" ");
        }

        return result.toString().substring(0, result.length() - 1);
    }

    public static void main(String[] args) {
        String result = new IntegerToWord().numberToWords(1234567891);
        System.out.println(result);
    }
}
