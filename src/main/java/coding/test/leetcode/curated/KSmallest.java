package coding.test.leetcode.curated;

import coding.test.ctci.tree.BTreePrinter;
import coding.test.ctci.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class KSmallest {

    int max = Integer.MAX_VALUE;

    public static int kthSmallest(TreeNode<Integer> root, int k) {

        if (root == null) {
            return 0;
        }

        List<Integer> sortedSet = new ArrayList<>();

        insert(root, sortedSet, k);


        return sortedSet.get(k);
    }

    private static void insert(TreeNode<Integer> node, List<Integer> sortedSet, int k) {

        if (node == null) {
            return;
        }

        insert(node.left, sortedSet, k);
        sortedSet.add(node.val);
        insert(node.right, sortedSet, k);
    }

    public static void main(String[] args) {

        TreeNode<Integer> treeNode = new TreeNode<>(5);
        treeNode.left = new TreeNode<>(3);
        treeNode.right = new TreeNode<>(6);
        treeNode.right.left = new TreeNode<>(1);
        treeNode.left.left = new TreeNode<>(2);
        treeNode.left.right = new TreeNode<>(4);

        BTreePrinter.printNode(treeNode);


        int ret = kthSmallest(treeNode, 3);
        System.out.println(ret);
    }
}
