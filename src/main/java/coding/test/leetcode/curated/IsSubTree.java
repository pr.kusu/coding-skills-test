package coding.test.leetcode.curated;

public class IsSubTree {

    public boolean isSubtree(TreeNode source, TreeNode target) {
        if (source == null) {
            return false;
        }

        return isEqual(source, target) || isSubtree(source.left, target) || isSubtree(source.right, target);

    }

    public boolean isEqual(TreeNode source, TreeNode target) {
        if (source == target) {
            return true;
        }

        if (source == null || target == null) {
            return false;
        }

        return source.val == target.val && isEqual(source.left, target.left)
                && isEqual(source.right, target.right);
    }
}
