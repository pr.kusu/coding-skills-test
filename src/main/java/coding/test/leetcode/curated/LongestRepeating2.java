package coding.test.leetcode.curated;

public class LongestRepeating2 {

    public static int characterReplacement(String s, int k) {
        int[] count = new int[26];
        int maxRepeated = 0;
        int start = 0;
        int maxLength = 0;

        for (int end = 0; end < s.length(); end++) {
            int currentFreq = ++count[s.charAt(end) - 'A'];
            maxRepeated = Math.max(maxRepeated, currentFreq);
            int currentLength = end - start + 1;

            while (currentLength - maxRepeated > k) {
                count[s.charAt(start) - 'A']--;
                start++;
                currentLength = end - start + 1;
            }

            maxLength = Math.max(maxLength, currentLength);
        }

        return maxLength;
    }

    public static int characterReplacement2(String s, int k) {
        int[] count = new int[26];
        int maxRepeated = 0;
        int start = 0;

        for (int end = 0; end < s.length(); end++) {
            int currentFreq = ++count[s.charAt(end) - 'A'];
            maxRepeated = Math.max(maxRepeated, currentFreq);
            int currentLength = end - start + 1;

            if (currentLength - maxRepeated > k) {
                count[s.charAt(start) - 'A']--;
                start++;
            }
        }

        return s.length() - start;
    }

    public static void main(String[] args) {
        int result = characterReplacement("AABABBA", 1);
        System.out.print("Max=" + result);
    }
}
