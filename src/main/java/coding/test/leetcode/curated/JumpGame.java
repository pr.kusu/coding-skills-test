package coding.test.leetcode.curated;

public class JumpGame {

    public static boolean canJump(int[] nums) {

        int lastPos = nums.length - 1;

        for (int i = nums.length - 1; i >= 0; i--) {

            if (i + nums[i] >= lastPos) {
                lastPos = i;
            }

        }

        return lastPos == 0;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{2, 3, 1, 1, 4};

        System.out.println(canJump(nums));
    }
}
