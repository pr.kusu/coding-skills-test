package coding.test.leetcode.curated;

import java.util.ArrayDeque;
import java.util.Deque;

public class SlidingMax {

    public static int[] maxSlidingWindow(int[] nums, int k) {
        //Input: nums = [1,3,-1,-3,5,3,6,7], and k = 3

        if (nums.length == 0) {
            return new int[0];
        }

        int[] result = new int[nums.length - k + 1];
        Deque<Integer> deque = new ArrayDeque<>();

        for (int i = 0; i < nums.length; i++) {
            if (!deque.isEmpty() && deque.peek() < i - k + 1) {
                System.out.println("Polling on: " + deque.peek() + " i=" + i + "k=" + k);
                deque.poll();
            }

            while (!deque.isEmpty() && nums[deque.peekLast()] < nums[i]) {
                deque.pollLast();
            }

            deque.offer(i);

            if (i >= k - 1) {
                result[i - k + 1] = nums[deque.peek()];
            }
        }

        return result;

    }

    public static void main(String[] args) {
        maxSlidingWindow(new int[]{1, 3, -1, -3, 5, 3, 6, 7}, 3);
    }


}
