package coding.test.leetcode.curated;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class NumWithSameConeDiff {
    //https://leetcode.com/problems/numbers-with-same-consecutive-differences/

    public int[] numsSameConsecDiff(int N, int K) {
        int[] nums = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        if (N == 1) {
            return nums;
        }

        List<Integer> result = new ArrayList<>();
        for (int num = 1; num < 10; num++) {
            dfs(N - 1, K, result, num);
        }

        return result.stream().mapToInt(i -> i).toArray();
    }

    private void dfs(int N, int K, List<Integer> result, int num) {
        if (N == 0) {
            result.add(num);
            return;
        }

        List<Integer> next = new ArrayList<>();
        int tailDigit = num % 10;

        if (tailDigit + K < 10)
            next.add(tailDigit + K);
        if (K != 0 && tailDigit - K >= 0)
            next.add(tailDigit - K);

        for (Integer n : next) {
            dfs(N - 1, K, result, num * 10 + n);
        }
    }

    private void bfs(int N, int K, List<Integer> result, int num) {

        Queue<Integer> queue = new ArrayDeque<>();
        queue.add(num);

        while (N > 0) {

            for (int i = queue.size(); i > 0; i--) {

                int nextNum = queue.poll();

                List<Integer> next = new ArrayList<>();

                int tailDigit = nextNum % 10;

                if (tailDigit + K < 10) {
                    next.add(tailDigit + K);
                }
                if (K != 0 && tailDigit - K >= 0) {
                    next.add(tailDigit - K);
                }

                for (int tail : next) {
                    queue.add(nextNum * 10 + tail);
                }
            }

            N--;
        }
        result.addAll(queue);
    }
}
