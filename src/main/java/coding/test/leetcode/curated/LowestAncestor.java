package coding.test.leetcode.curated;

import coding.test.ctci.tree.TreeNode;

public class LowestAncestor {

    public TreeNode<Integer> lowestCommonAncestor(TreeNode<Integer> root, TreeNode<Integer> p, TreeNode<Integer> q) {
        if (root == null) {
            return null;
        }

        if ((p.val > root.val && q.val > root.val)) {
            return lowestCommonAncestor(root.right, p, q);
        } else if ((p.val < root.val && q.val < root.val)) {
            return lowestCommonAncestor(root.left, p, q);
        }

        return root;
    }

    public TreeNode<Integer> lowestCommonAncestor2(TreeNode<Integer> root, TreeNode<Integer> p,
                                                   TreeNode<Integer> q) {
        if (root == null) {
            return null;
        }

        TreeNode<Integer> start = root;

        while (start != null) {

            if ((p.val > start.val && q.val > start.val)) {
                start = start.right;
            } else if ((p.val < root.val && q.val < start.val)) {
                start = start.left;
            }
        }

        return start;
    }
}
