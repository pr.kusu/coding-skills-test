package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.List;

public class MedianDataStream {

    /**
     * initialize your data structure here.
     * <p>
     * 1,100,100,3,50,5,2,4,40,25,21
     */
    private List<Integer> integers = new ArrayList<>();

    public MedianDataStream() {

    }

    public void addNum(int num) {
        integers.add(num);
        sort();

        // integers.forEach(val -> System.out.print(val+","));
    }

    void sort() {
        int n = integers.size();
        for (int i = 1; i < n; ++i) {
            int key = integers.get(i);
            int j = i - 1;

            /* Move elements of arr[0..i-1], that are
               greater than key, to one position ahead
               of their current position */
            while (j >= 0 && integers.get(j) > key) {
                integers.set(j + 1, integers.get(j));
                j = j - 1;
            }
            integers.set(j + 1, key);
        }
    }

    public double findMedian() {
        if (integers.size() == 0) {
            return 0;
        }

        if (integers.size() == 1) {
            return integers.get(0);
        }

        int size = integers.size();

        if (size % 2 == 0) {
            float val = integers.get(size / 2 - 1) + integers.get(size / 2);
            return val / 2;
        }
        return integers.get(size / 2);
    }

    public static void main(String[] args) {
        MedianDataStream stream = new MedianDataStream();

        stream.addNum(2);
        stream.addNum(1);

        System.out.println(stream.findMedian());
        stream.addNum(3);

        System.out.println(stream.findMedian());
    }
}
