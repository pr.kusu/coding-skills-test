package coding.test.leetcode.curated;

public class ReverseLinkedList {

    public static ListNode reverseList(ListNode head) {

        if (head == null || head.next == null) {
            return head;
        }

        ListNode first = head;
        ListNode next = head.next;

        //1->2->3->4->5

        while (next.next != null) {
            ListNode tmp = next.next;
            next.next = first;
            first = next;
            next = tmp;
        }

        return next;
    }

    public static void main(String[] args) {
        ListNode node = new ListNode(1);
        node.next = new ListNode(2);
        node.next.next = new ListNode(3);
        node.next.next.next = new ListNode(4);
        node.next.next.next.next = new ListNode(5);

        reverseList(node);

        System.out.println();
    }
}
