package coding.test.leetcode.curated;

public class MinStack {

    private Node head;

    class Node {
        int val;
        int min;
        Node next;

        public Node(int val, int min, Node next) {
            this.val = val;
            this.min = min;
            this.next = next;
        }
    }

    public void push(int x) {
        if (head == null) {
            head = new Node(x, x, null);
        } else {
            head = new Node(x, Math.min(x, head.min), head);
        }
    }

    public int pop() {
        int val = -1;

        if (head != null) {
            val = head.val;
            head = head.next;
        }

        return val;
    }

    public int top() {
        if (head == null) {
            return -1;
        }

        return head.val;
    }

    public int getMin() {
        if (head == null) {
            return -1;
        }

        return head.min;
    }
}
