package coding.test.leetcode.curated;

public class TwoKeyKeyboard {

    public int minSteps(int n) {
        Integer[][] dp = new Integer[n + 1][n + 1];
        return countSteps(n, 0, 1, 0, dp);
    }

    public int minSteps2(int n) {
        int ans = 0, d = 2;
        while (n > 1) {
            while (n % d == 0) {
                ans += d;
                n /= d;
            }
            d++;
        }
        return ans;
    }

    private int countSteps(int n, int clipBoard, int document, int steps, Integer[][] cache) {
        if (document == n) {
            return steps;
        }

        if (document > n) {
            return Integer.MAX_VALUE;
        }

        if (cache[clipBoard][document] != null) {
            return cache[clipBoard][document];
        }

        int copy = Integer.MAX_VALUE;
        if (document != clipBoard) {
            copy = countSteps(n, document, document, steps + 1, cache);
        }

        int paste = Integer.MAX_VALUE;
        if (clipBoard > 0) {
            paste = countSteps(n, clipBoard, document + clipBoard, steps + 1, cache);
        }

        cache[clipBoard][document] = Math.min(copy, paste);

        return cache[clipBoard][document];
    }
}
