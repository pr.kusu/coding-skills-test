package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.List;

public class EncodeDecodeString {

    // Encodes a list of strings to a single string.
    public String encode(List<String> strs) {
        StringBuilder builder = new StringBuilder();

        for (String s : strs) {
            builder.append(s.length()).append(":").append(s);
        }

        return builder.toString();
    }

    // Decodes a single string to a list of strings.
    public List<String> decode(String s) {
        List<String> result = new ArrayList<>();
        int right = 0;
        //"a:b:ccc"->4:12346:1:2:3:
        int i = 0;
        while (i < s.length()) {
            int j = i;
            while (j < s.length() && Character.isDigit(s.charAt(j))) {
                j++;
            }
            int len = Integer.parseInt(s.substring(i, j));
            int start = j + 1;
            result.add(s.substring(start, start + len));
            i = start + len;
        }

        return result;
    }

    public static void main(String[] args) {

    }
}
