import java.util.ArrayDeque;
import java.util.Queue;

class Trie {

    TriNode head;

    /**
     * Initialize your data structure here.
     */
    public Trie() {
        head = new TriNode('0');
    }

    public TriNode getHead() {
        return head;
    }

    /**
     * Inserts a word into the trie.
     */
    public void insert(String word) {
        TriNode start = head;

        for (int i = 0; i < word.length(); i++) {
            if (!start.contains(word.charAt(i))) {
                start.insert(word.charAt(i));
                start = start.linkAt(word.charAt(i));
            } else {
                start = start.linkAt(word.charAt(i));
            }
        }

        start.end = true;
        start.word = word;
    }
}

class TriNode {

    final TriNode[] links = new TriNode[26];
    char ch;
    boolean end;
    String word;

    public TriNode(char ch) {
        this.ch = ch;
    }

    public boolean contains(char ch) {
        return links[ch - 'a'] != null;
    }

    public TriNode linkAt(char ch) {
        return links[ch - 'a'];
    }

    public void insert(char ch) {
        links[ch - 'a'] = new TriNode(ch);
    }

    public boolean isEnd() {
        return end;
    }
}

class Solution {

    public String longestWord(String[] words) {
        Trie trie = new Trie();

        for (String word : words) {
            trie.insert(word);
        }

        return bfs(trie);
    }

    private String bfs(Trie trie) {
        Queue<TriNode> queue = new ArrayDeque<>();
        TriNode head = trie.head;
        head.word = "";
        queue.offer(trie.head);

        String res = "";
        while (!queue.isEmpty()) {
            TriNode current = queue.poll();

            if (current == head || current.end) {
                String currentWord = current.word;
                if (currentWord.length() > res.length()) {
                    res = currentWord;
                } else if (currentWord.length() == res.length() && currentWord.compareTo(res) < 0) {
                    res = currentWord;
                }

                for (TriNode node : current.links) {
                    if (node != null) {
                        queue.add(node);
                    }
                }
            }

        }

        return res;
    }
}