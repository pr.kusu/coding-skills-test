package coding.test.leetcode.curated;

import java.util.Comparator;
import java.util.PriorityQueue;

public class MergeTwoList {

    public ListNode mergeTwoLists2(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }

        PriorityQueue<ListNode> queue = new PriorityQueue<>(Comparator.comparingInt(o -> o.val));

        queue.add(l1);
        queue.add(l2);

        ListNode head = new ListNode(0);
        ListNode start = head;
        while (!queue.isEmpty()) {
            start.next = queue.poll();
            start = start.next;

            if (start.next != null) {
                queue.add(start.next);
            }

        }


        return head.next;
    }

    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode head;
        ListNode first = l1;
        ListNode second = l2;

        if (l1.val < l2.val) {
            head = l1;
            first = l1.next;
        } else {
            head = l2;
            second = l2.next;
        }

        ListNode node = head;

        while (first != null || second != null) {
            if (first == null) {
                node.next = second;
                second = second.next;
            } else if (second == null) {
                node.next = first;
                first = first.next;
            } else if (first.val <= second.val) {
                node.next = first;
                first = first.next;
            } else {
                node.next = second;
                second = second.next;
            }
        }

        return head;
    }


}
