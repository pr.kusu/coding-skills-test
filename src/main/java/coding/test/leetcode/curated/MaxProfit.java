package coding.test.leetcode.curated;

public class MaxProfit {

    //[7,1,5,3,6,4]

    public int maxProfit(int[] prices) {
        int profit = 0;
        int minSoFar = prices[0];

        for (int i = 1; i < prices.length; i++) {
            if (prices[i] < minSoFar) {
                minSoFar = prices[i];
            }
            int currentProfit = prices[i] - minSoFar;
            profit = Math.max(profit, currentProfit);
        }

        return profit;
    }

    public int maxProfit2(int[] prices) {
        int profit = 0;

        for (int i = 1; i < prices.length; i++) {

            int currentProfit = prices[i - 1] - prices[i];

            if (currentProfit > 0) {
                profit += currentProfit;
            }
        }
        return profit;
    }
}
