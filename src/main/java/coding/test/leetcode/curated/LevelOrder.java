package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.List;

public class LevelOrder {

    /**
     * 3
     * / \
     * 9  20
     * /  \
     * 15   7
     * <p>
     * [
     * [3],
     * [9,20],
     * [15,7]
     * ]
     *
     * @param root
     * @return
     */

    public List<List<Integer>> levelOrder(SameTree.TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();

        helper(root, result, 0);

        return result;
    }

    private void helper(SameTree.TreeNode root, List<List<Integer>> nodes, int level) {

        if (nodes.size() <= level) {
            nodes.add(new ArrayList<>());
        }

        if (root == null) {
            return;
        }

        List<Integer> targetList = nodes.get(level);
        targetList.add(root.val);

        helper(root.left, nodes, level + 1);

        helper(root.right, nodes, level + 1);
    }
}
