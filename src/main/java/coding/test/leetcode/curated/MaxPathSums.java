package coding.test.leetcode.curated;

import coding.test.ctci.tree.TreeNode;

public class MaxPathSums {
    int max = Integer.MIN_VALUE;

    public boolean hasPathSum(TreeNode<Integer> root, int sum) {
        if (root == null) {
            return sum == 0;
        }

        return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val);
    }

    public int maxPathSum(TreeNode<Integer> root) {

        helper(root);
        return max;
    }

    public int helper(TreeNode<Integer> root) {

        if (root == null) {
            return 0;
        }

        int left = Math.max(helper(root.left), 0);
        int right = Math.max(helper(root.right), 0);

        max = Math.max(root.val, Math.max(left, right));

        return root.val + left + right;
    }

    public static void main(String[] args) {

        {

        }
    }
}
