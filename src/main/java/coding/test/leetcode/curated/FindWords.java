package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.List;

public class FindWords {

    //board = [
    //  ['o','a','a','n'],
    //  ['e','t','a','e'],
    //  ['i','h','k','r'],
    //  ['i','f','l','v']
    //]
    //      words = ["oath","pea","eat","rain"]
    //
    //      Output: ["eat","oath"]

    public static List<String> findWords(char[][] board, String[] words) {
        List<String> result = new ArrayList<>();

        for (String s : words) {
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[0].length; j++) {
                    if (board[i][j] == s.charAt(0) && dfs(board, i, j, s, 0)) {
                        result.add(s);
                    }
                }
            }
        }

        return result;
    }

    public static boolean dfs(char[][] board, int x, int y, String s, int index) {
        if (index == s.length()) {
            return true;
        }

        if (x < 0 || y < 0 || x >= board.length || y >= board[0].length) {
            return false;
        }

        char tmp = board[x][y];
        boolean result = false;

        if (s.charAt(index) == tmp) {
            board[x][y] = 'X';
            result = dfs(board, x - 1, y, s, index + 1) || dfs(board, x + 1, y, s, index + 1)
                    || dfs(board, x, y - 1, s, index + 1) || dfs(board, x, y + 1, s, index + 1);
        }

        board[x][y] = tmp;

        return result;
    }

    public static void main(String[] args) {
        long start = System.nanoTime();
        List<String> result2 = findWords(new char[][]{
                {'a', 'b', 'c','d'},
                {'a', 'e', 'd','h'},
                {'g', 'f', 'g','f'},
                {'a', 'e', 'd','o'}
        }, new String[]{"abcdhdeagfgfodea", "bcdhdeagfgfodea", "cdhdeagfgfodea"});

        long end = System.nanoTime();

        System.out.println(end - start);
    }
}
