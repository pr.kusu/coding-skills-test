package coding.test.leetcode.curated;

import coding.test.ctci.tree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class KthMin {

    public static int kthSmallest(TreeNode<Integer> root, int k) {

        LinkedList<TreeNode<Integer>> stack = new LinkedList<>();

        while (true) {
            while (root != null) {
                stack.add(root);
                root = root.left;
            }
            root = stack.removeLast();
            if (--k == 0) return root.val;
            root = root.right;
        }
    }

    public static int kthSmallest2(TreeNode<Integer> root, int k) {

        if (root == null) {
            return 0;
        }

        List<Integer> sortedSet = new ArrayList<>();

        insert(root, sortedSet, k);


        return sortedSet.get(k-1);
    }

    private static void insert(TreeNode<Integer> node, List<Integer> sortedSet, int k) {

        if (node == null|| sortedSet.size()>=k) {
            return;
        }

        insert(node.left, sortedSet, k);
        sortedSet.add(node.val);
        insert(node.right, sortedSet, k);
    }
}
