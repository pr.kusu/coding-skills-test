package coding.test.leetcode.curated;

public class WordSearchDFS {

    public boolean exist(char[][] board, String word) {
        int row = board.length;
        int col = board[0].length;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                //Perform dfs for each character in grid,if String found return true
                if (dfs(board, word, i, j, 0))
                    return true;
            }
        }
        return false;
    }

    public static boolean dfs(char[][] board, String word, int i, int j, int count) {
        int row = board.length;
        int col = board[0].length;
        // If count equals to length of string , then found the string in grid
        if (count == word.length())
            return true;

        // to check boundary of traversal
        if (i < 0 || i >= row || j < 0 || j >= col)
            return false;

        //If we found string[count] at the grid position, check for next char in all four           //directions
        if (board[i][j] == word.charAt(count)) {
            char tmp = board[i][j];
            // Marking this cell as visited
            board[i][j] = '#';

            // finding subpattern in 4 directions
            if (dfs(board, word, i + 1, j, count + 1) || dfs(board, word, i - 1, j, count + 1)
                    || dfs(board, word, i, j + 1, count + 1) || dfs(board, word, i, j - 1, count + 1))
                return true;

            // marking this cell
            // as unvisited again
            board[i][j] = tmp;
        }
        return false;
    }
}
