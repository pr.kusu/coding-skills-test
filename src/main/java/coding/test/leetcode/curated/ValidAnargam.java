package coding.test.leetcode.curated;

public class ValidAnargam {

    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }

        int[] freq = new int[26];

        char[] charArray = s.toCharArray();

        for (char c : charArray) {
            freq[c - 'a']++;
        }
        for (char c : t.toCharArray()) {
            if (freq[c - 'a'] < 0) {
                return false;
            }
        }

        return true;
    }
}
