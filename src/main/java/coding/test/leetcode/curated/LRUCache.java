package coding.test.leetcode.curated;

import java.util.HashMap;
import java.util.Map;

class DoublyLinkedList {

    ListNode head;
    ListNode tail;

    static class ListNode {
        ListNode prev;
        ListNode next;

        int key;
        int val;

        public ListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }

    }

    public ListNode addLast(int key, int val) {
        if (head == null) {
            head = new ListNode(key, val);
            tail = head;
        } else {
            ListNode tmp = tail;
            tail.next = new ListNode(key, val);
            tail = tail.next;
            tail.prev = tmp;
        }
        return tail;
    }

    public ListNode removeFirst() {
        return remove(head);
    }

    public ListNode remove(ListNode node) {
        if (node == head) {
            head = head.next;

            if (head != null) {
                head.prev = null;
            }
        } else if (node == tail) {
            node.prev.next = node.next;
            tail = tail.prev;
        } else {
            node.prev.next = node.next;

            if (node.next != null) {
                node.next.prev = node.prev;
            }

        }

        return node;
    }

    public void printList() {
        ListNode start = head;

        while (start != null) {
            System.out.print(start.key + ">");
            start = start.next;
        }

        System.out.println();
    }

}

public class LRUCache {

    private Map<Integer, DoublyLinkedList.ListNode> data = new HashMap<>();
    private DoublyLinkedList deque = new DoublyLinkedList();
    int capacity;
    int size = 0;


    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    public int get(int key) {
        DoublyLinkedList.ListNode value = data.get(key);

        if (value == null) {
            return -1;
        }

        deque.remove(value);
        DoublyLinkedList.ListNode inserted = deque.addLast(value.key, value.val);
        data.put(key, inserted);
        return value.val;

    }

    public void put(int key, int value) {
        if (data.containsKey(key)) {
            DoublyLinkedList.ListNode current = data.get(key);
            deque.remove(current);
        } else {
            size++;
        }

        DoublyLinkedList.ListNode inserted = deque.addLast(key, value);
        data.put(key, inserted);

        if (size > capacity) {
            DoublyLinkedList.ListNode first = deque.removeFirst();
            data.remove(first.key);
            size--;
        }
    }

    public static void main(String[] args) {

        LRUCache cache = new LRUCache(3);
        cache.put(1, 1);
        cache.put(2, 2);
        cache.put(3, 3);
        cache.put(4, 4);

        System.out.println(cache.get(4));
        System.out.println(cache.get(3));
        System.out.println(cache.get(2));
        System.out.println(cache.get(1));
        cache.put(5, 5);
        System.out.println(cache.get(1));
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));
        System.out.println(cache.get(4));
        System.out.println(cache.get(5));

    }
}

