package coding.test.leetcode.curated;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class ServerFile {

    static int getHours(int[][] serverFile) {
        Queue<Integer> queue = new LinkedList<>();
        int current = 0;

        for (int i = 0; i < serverFile.length; i++) {
            for (int j = 0; j < serverFile[0].length; j++) {
                if (serverFile[i][j] == 0) {
                    queue.add(j + i * serverFile[0].length);
                } else {
                    current++;
                }
            }
        }

        return bfs(serverFile, queue, current);
    }

    public static int bfs(int[][] files, Queue<Integer> queue, int totalFiles) {

        int hours = 1;
        Queue<Integer> backLog = new LinkedList<>();

        int version = 1;
        while (!queue.isEmpty() || !backLog.isEmpty()) {
            if (queue.isEmpty()) {
                queue.addAll(backLog);
                backLog.clear();
                hours++;
                version++;

                printArr(files);
            }
            int current = queue.poll();

            int x = current / files[0].length;
            int y = current % files[0].length;

            if (files[x][y] == 0) {
                if (x - 1 >= 0 && files[x - 1][y] > 0 && files[x - 1][y] <= version) {
                    files[x][y] = version + 1;
                } else if (x + 1 < files.length && files[x + 1][y] > 0 && files[x + 1][y] <= version) {
                    files[x][y] = version + 1;
                } else if (y - 1 >= 0 && files[x][y - 1] > 0 && files[x][y - 1] <= version) {
                    files[x][y] = version + 1;
                } else if (y + 1 < files[0].length && files[x][y + 1] > 0 && files[x][y + 1] <= version) {
                    files[x][y] = version + 1;
                } else {
                    backLog.add(current);
                }
            }

        }

        printArr(files);

        return hours;
    }

    private static void printArr(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(Arrays.toString(arr[i]));
        }
    }

    public static void main(String[] args) {
        int[][] arr = new int[][]{
                {0, 1, 1, 0, 1},
                {0, 1, 0, 1, 0},
                {0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0}

        };

        int hrs = getHours(arr);
        System.out.println(hrs);
    }

}
