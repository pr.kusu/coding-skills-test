package coding.test.leetcode.curated;

public class ConChangeII {

    public int change(int amount, int[] coins) {
        Integer[][] dp = new Integer[coins.length][amount + 1];

        return ways(0, amount, coins, dp);
    }

    private int ways(int i, int amount, int[] coins, Integer[][] dp) {
        if (amount == 0) {
            return 1;
        }
        if (amount < 0 || i >= coins.length) {
            return 0;
        }
        if (dp[i][amount] != null) {
            return dp[i][amount];
        }

        int w1 = ways(i + 1, amount, coins, dp);
        int w2 = ways(i, amount - coins[i], coins, dp);

        dp[i][amount] = w1 + w2;

        return w1 + w2;
    }

}
