package coding.test.leetcode.curated;

import java.util.*;
import java.util.stream.Collectors;

public class NumIslands {

    public static class Pair {
        public int x;
        public int y;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair pair = (Pair) o;
            return x == pair.x &&
                    y == pair.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    public int numIslands(char[][] grid) {
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {
                    bfs(grid, i, j);
                    count++;
                }
            }
        }

        return count;
    }

    private void bfs(char[][] grid, int x, int y) {
        Queue<Pair> queue = new LinkedList<>();

        queue.add(new Pair(x, y));

        while (!queue.isEmpty()) {
            Pair current = queue.poll();
            List<Pair> adj = getAdjcent(grid, current);

            for (Pair next : adj) {
                if (grid[next.x][next.y] == '1') {
                    queue.add(next);
                    grid[next.x][next.y] = 'X';
                }
            }

            grid[current.x][current.y] = 'X';
        }
    }

    private List<Pair> getAdjcent(char[][] grid, Pair current) {
        List<Pair> result = new ArrayList<>();

        result.add(new Pair(current.x - 1, current.y));
        result.add(new Pair(current.x + 1, current.y));
        result.add(new Pair(current.x, current.y + 1));
        result.add(new Pair(current.x, current.y - 1));

        return result.stream().filter(val -> {
            return val.x >= 0 && val.y >= 0 && val.x < grid.length && val.y < grid[0].length;
        }).collect(Collectors.toList());
    }

    public int numIslands2(char[][] grid) {
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {
                    dfs(grid, i, j);
                    count++;
                }
            }
        }

        return count;
    }

    public void dfs(char[][] grid, int x, int y) {
        if (x < 0 || y < 0 || x >= grid.length || y >= grid[0].length) {
            return;
        }
        if (grid[x][y] == '1') {
            return;
        }
        grid[x][y] = 'X';
        dfs(grid, x - 1, y);
        dfs(grid, x + 1, y);
        dfs(grid, x, y - 1);
        dfs(grid, x, y + 1);
    }

    public static void main(String[] args) {
        char[][] grid = new char[][]{
                {'1', '1', '0', '0', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '1', '0', '0'},
                {'0', '0', '0', '1', '1'}};


        int val = new NumIslands().numIslands2(grid);
        int va2 = new NumIslands().numIslands(grid);
        System.out.println(val + "," + va2);
    }
}
