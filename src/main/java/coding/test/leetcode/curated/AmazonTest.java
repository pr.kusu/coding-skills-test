package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

public class AmazonTest {

    static class Feature implements Comparable<Feature> {
        String feature;
        Integer freq;

        public Feature(String feature, Integer freq) {
            this.feature = feature;
            this.freq = freq;
        }

        public boolean equals(Object o) {
            if (o == null) {
                return false;

            }

            Feature that = (Feature) o;
            return that.feature.equalsIgnoreCase(this.feature);
        }


        @Override
        public int compareTo(Feature o) {
            int value = Integer.compare(o.freq, this.freq);

            if (value == 0) {
                return this.feature.compareTo(o.feature);
            } else {
                return value;
            }
        }
    }

    public static ArrayList<String> popularNFeatures(int numFeatures, int topFeatures, List<String> possibleFeatures,
                                                     int numFeatureRequests, List<String> featureRequest) {

        Map<String, Integer> freqMap = new HashMap<>();

        PriorityQueue<Feature> pQueue =
                new PriorityQueue<>(topFeatures);

        for (String s : featureRequest) {
            String[] features = s.trim().split(" ");
            Set<String> duplicate = new HashSet<>();

            for (String featureStr : features) {

                if (featureStr == null || featureStr.isEmpty()) {
                    continue;
                }

                String feature = featureStr.toLowerCase();

                if (possibleFeatures.contains(feature) && !duplicate.contains(feature)) {
                    if (!freqMap.containsKey(feature)) {
                        freqMap.put(feature, 1);
                        pQueue.add(new Feature(feature, 1));
                    } else {
                        freqMap.put(feature, freqMap.get(feature) + 1);
                        pQueue.remove(new Feature(feature, freqMap.get(feature)));
                        pQueue.add(new Feature(feature, freqMap.get(feature) + 1));
                    }

                    duplicate.add(feature);
                }

            }
        }

        ArrayList<String> topFeaturesList = new ArrayList<>();

        for (int i = 0; i < topFeatures; i++) {

            if (!pQueue.isEmpty()) {
                topFeaturesList.add(pQueue.poll().feature);
            }
        }


        return topFeaturesList;
    }

    public static void main(String[] args) {

        List<String> possibleFeatures1 = new ArrayList<String>() {{
            add("storage");
            add("battery");
            add("hover");
            add("alexa");
            add("waterproof");
            add("solar");
        }};
        List<String> featureRequest1 = new ArrayList<String>() {{
            add("I wish my Kindle had even more storage!");
            add("I wish the battery life on my Kindle Lasted 2 years");
            add("I read in the bath and would enjoy a waterproof Kindle");
            add("Waterproof and waterproof proff increased battery");
            add("I want to take my Kindle into the shower. Waterproof please waterproof");
            add("It would be neat if my Kinde would hover on my desk when not in use");
            add("How coool would it be if my Kindle charged in the sun via solar power");
        }};


        List<String> possibleFeatures = new ArrayList<String>() {{
            add("anacell");
            add("betacellular");
            add("centracular");
            add("deltacellular");
            add("eurocell");
        }};
        List<String> featureRequest = new ArrayList<String>() {{
            add("I love anacell best service provided by anacell in town");
            add("deltacellular provides  has great betacellular");
            add("centracular is worse than eurocell");
            add("betacellular is worse than deltacellular");
        }};

        List<String> result = popularNFeatures(featureRequest.size(), 2, possibleFeatures, featureRequest.size(), featureRequest);

        result.forEach(val -> System.out.print(val + " ,"));
    }
}
