package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.List;

public class InsertInterval {

    public static int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> result = new ArrayList<>();
        if (intervals.length == 0) {
            result.add(newInterval);
            return result.toArray(new int[result.size()][]);
        }

        int i = 0;
        boolean added = false;
        List<int[]> list = new ArrayList<>();
        while (i < intervals.length) {
            if (!added && newInterval[0] < intervals[i][0]) {
                list.add(newInterval);
                added = true;
            }
            list.add(intervals[i]);
            i++;
        }

        if (!added) {
            list.add(newInterval);
        }

        for (i = 0; i < list.size() - 1; i++) {
            int[] first = list.get(i);
            int[] second = list.get(i + 1);

            if (first[1] >= second[0]) {
                second[0] = Math.min(first[0], second[0]);
                second[1] = Math.max(first[1], second[1]);
            } else {
                result.add(first);
            }
        }
        result.add(list.get(list.size() - 1));
        return result.toArray(new int[result.size()][]);
    }


    public static void main(String[] args) {
        insert(new int[][]{{2, 5}, {6, 7}, {8, 9}}, new int[]{0, 1});
    }
}
