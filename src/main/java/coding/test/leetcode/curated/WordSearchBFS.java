package coding.test.leetcode.curated;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

public class WordSearchBFS {

    private static class GraphNode {
        int row;
        int col;
        boolean visited;
        GraphNode source;

        public GraphNode(int row, int col) {
            this.row = row;
            this.col = col;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }

            if (o instanceof GraphNode) {
                GraphNode that = (GraphNode) o;
                return this.row == that.row && this.col == that.col;
            }

            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.row, this.col);
        }
    }

    public static boolean exist(char[][] board, String word) {
        int row = board.length;
        int col = board[0].length;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (bfs(board, new GraphNode(i, j), word)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static boolean bfs(char[][] board, GraphNode start, String word) {
        char startChar = board[start.row][start.col];
        if (startChar != word.charAt(0)) {
            return false;
        }
        Map<GraphNode, Map<GraphNode, Integer>> countMap = new HashMap<>();

        if (1 == word.length()) {
            return true;
        }
        Queue<GraphNode> queue = new ArrayBlockingQueue<>(10000);
        start.visited = true;
        queue.add(start);
        countMap.put(start, new HashMap<>());
        countMap.get(start).put(start, 1);

        while (!queue.isEmpty()) {
            GraphNode current = queue.poll();
            if (!countMap.containsKey(current)) {
                countMap.put(current, new HashMap<>());
            }

            char ch = board[current.row][current.col];

            List<GraphNode> neighbors = getNext(current, board.length, board[0].length);
            for (GraphNode neighbour : neighbors) {

                char boardChar = board[neighbour.row][neighbour.col];
                int count = countMap.get(current).getOrDefault(current.source, 1);

                if (boardChar == word.charAt(count)) {
                    queue.add(neighbour);
                    count++;
                    neighbour.source = current;
                    if (!countMap.containsKey(neighbour)) {
                        countMap.put(neighbour, new HashMap<>());
                    }
                    countMap.get(neighbour).put(current, count);
                }
                if (count == word.length()) {
                    return true;
                }
            }
        }

        return false;
    }

    private static List<GraphNode> getNext(GraphNode node, int maxRow, int maxCol) {
        GraphNode left = new GraphNode(node.row, node.col - 1);
        GraphNode right = new GraphNode(node.row, node.col + 1);
        GraphNode top = new GraphNode(node.row - 1, node.col);
        GraphNode bottom = new GraphNode(node.row + 1, node.col);

        List<GraphNode> list = Arrays.asList(left, right, top, bottom);

        return list.stream().filter(val -> {
            return (node.source == null || !node.source.equals(val))
                    && val.col >= 0 && val.row >= 0 && val.row < maxRow && val.col < maxCol;
        }).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        /**
         * board =
         * [
         *   ['A','B','C','E'],
         *   ['S','F','C','S'],
         *   ['A','D','E','E']
         * ]
         *
         * Given word = "ABCCED", return true.
         * Given word = "SEE", return true.
         * Given word = "ABCB", return false.
         */
        char[][] board = new char[][]{

                {'A', 'B', 'C', 'E'},
                {'S', 'F', 'C', 'S'},
                {'A', 'D', 'E', 'E'}
        };

        char[][] board2 = new char[][]{

                {'A', 'A', 'A', 'A'},
                {'A', 'A', 'A', 'A'},
                {'A', 'A', 'A', 'A'}
        };
//[["a","a","a","a"],["a","a","a","a"],["a","a","a","a"]]
//"aaaaaaaaaaaa"
        ///[["a","a","a","a"],["a","a","a","a"],["a","a","a","a"]]
        //"aaaaaaaaaaaaa"
         System.out.println(exist(board2, "aaaaaaaaaaaaa".toUpperCase()));
        System.out.println(exist(board, "ABCCED".toUpperCase()));
    }
}
