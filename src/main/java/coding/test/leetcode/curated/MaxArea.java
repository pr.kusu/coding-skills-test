package coding.test.leetcode.curated;

public class MaxArea {

    public int maxArea(int[] height) {
        int start = 0;
        int end = height.length - 1;
        int area = 0;

        while (start < end) {
            int width = Math.min(height[start], height[end]);

            int currentArea = width * (end - start);

            area = Math.max(area, currentArea);

            if (height[start] < height[end]) {
                start++;
            } else {
                end--;
            }
        }

        return area;
    }
}

