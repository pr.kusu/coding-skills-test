package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.Arrays;

public class ProductExceptSelf {

    public static int[] productExceptSelf(int[] nums) {

        //Input:  [1,2,3,4,5]
        //Output: [120,60,40,30,24]

        // 1,  2, 6, 24, 120
        // 5, 20, 60, 120, 1
        // int[] freq = new int[10];

        int[] dp1 = new int[nums.length + 1];
        int[] dp2 = new int[nums.length + 1];

        dp1[0] = 1;
        dp2[nums.length] = 1;

        int[] result = new int[nums.length];

        int i = 1;
        int j = nums.length - 1;

        while (i < nums.length) {
            dp1[i] = dp1[i - 1] * nums[i - 1];
            dp2[j] = dp2[j + 1] * nums[j];
            i++;
            j--;
        }

        for (i = 1; i <= nums.length; i++) {
            result[i - 1] = dp2[i] * dp1[i - 1];
        }

        return result;

    }

    public static void main(String[] args) {
        productExceptSelf(new int[]{1, 2, 3, 4, 5});
    }
}
