package coding.test.leetcode.curated;

import java.util.LinkedList;
import java.util.Queue;

public class Deserialize {

    // Encodes a tree to a single string.
    public static String serialize(TreeNode root) {
        if (root == null) {
            return null;
        }

        StringBuilder result = new StringBuilder();

        Queue<TreeNode> priorityQueue = new LinkedList<>();
        priorityQueue.add(root);

        while (!priorityQueue.isEmpty()) {

            TreeNode node = priorityQueue.poll();

            if (node.val != Integer.MIN_VALUE) {
                result.append(node.val + ",");
            } else {
                result.append("null,");
            }

            if (node.left != null) {
                priorityQueue.add(node.left);
            } else if (node.val != Integer.MIN_VALUE) {
                priorityQueue.add(new TreeNode(Integer.MIN_VALUE));
            }

            if (node.right != null) {
                priorityQueue.add(node.right);
            } else if (node.val != Integer.MIN_VALUE) {
                priorityQueue.add(new TreeNode(Integer.MIN_VALUE));
            }
        }

        return result.toString();
    }

    // Decodes your encoded data to tree.
    public static TreeNode deserialize(String data) {

        if (data == null || data.isEmpty()) {
            return null;
        }

        String[] values = data.split(",");
        int[] intValues = new int[values.length];

        int size = 0;
        for (String s : values) {
            if (!s.isEmpty()) {
                if (s.equals("null")) {
                    intValues[size++] = Integer.MIN_VALUE;
                } else {
                    intValues[size++] = Integer.parseInt(s);
                }
            }
        }

        if (size == 0) {
            return null;
        }

        TreeNode root = new TreeNode(intValues[0]);

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        int start = 1;

        while (!queue.isEmpty()) {
            TreeNode current = queue.poll();

            if (start < size && intValues[start] != Integer.MIN_VALUE) {
                current.left = new TreeNode(intValues[start++]);
            } else {
                start++;
            }
            if (start < size && intValues[start] != Integer.MIN_VALUE) {
                current.right = new TreeNode(intValues[start++]);
            } else {
                start++;
            }
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }

        return root;
    }

    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(1);
        treeNode.left = new TreeNode(2);
        treeNode.right = new TreeNode(3);
        treeNode.right.left = new TreeNode(4);
        treeNode.right.right = new TreeNode(5);


        String value = serialize(treeNode);

        System.out.println(value);

        TreeNode node = deserialize(value);
    }
}
