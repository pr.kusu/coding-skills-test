package coding.test.leetcode.curated;

public class FindMinimum {

    public static int findMin(int[] nums) {

        // [14,15,0,1,2,3,4,5,6,7,8,9]

        //[14,15,16,17,18,19,20,5,6,7,8,9]
        return minHelper(nums, 0, nums.length - 1);
    }


    public static int minHelper(int[] nums, int low, int high) {

        if (low == high) {
            return nums[low];
        }
        int mid = (high + low) / 2;

        if (nums[mid] > nums[low]) {
            return minHelper(nums, mid + 1, high);
        }

        return minHelper(nums, low, mid);
    }

    public static void main(String[] args) {

        System.out.println(findMin(new int[]{4, 5, 6, 7, 0, 1, 2}));
    }
}
