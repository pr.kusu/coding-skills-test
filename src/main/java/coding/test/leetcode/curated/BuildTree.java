package coding.test.leetcode.curated;

import coding.test.ctci.tree.BTreePrinter;
import coding.test.ctci.tree.TreeNode;

import java.util.HashMap;
import java.util.Map;

public class BuildTree {

    public static TreeNode buildTree(int[] preorder, int[] inorder) {
        Map<Integer, Integer> inorderMap = new HashMap<>();

        for (int i = 0; i < inorder.length; i++) {
            inorderMap.put(inorder[i], i);
        }

        return buildTree(0, inorder.length - 1, 0, preorder.length - 1,
                preorder, inorderMap);
    }

    public static TreeNode buildTree(int inStart, int inEnd, int preStart, int preEnd,
                                     int[] preorder,Map<Integer, Integer> inOrderMap) {

        if (inStart > inEnd || preStart > preEnd) {
            return null;
        }

        TreeNode root = new TreeNode(preorder[preStart]);

        int leftSpace = inOrderMap.get(preorder[preStart]) - inStart;

        root.left = buildTree(inStart, inStart + leftSpace, preStart + 1, preStart + leftSpace,
                preorder, inOrderMap);


        root.right = buildTree(inStart + leftSpace + 1, inEnd, preStart + 1 + leftSpace, preEnd,
                preorder, inOrderMap);

        return root;
    }

    public static void main(String[] args) {
        TreeNode<Integer> node = buildTree(new int[]{3, 9, 20, 15, 7}, new int[]{9, 7,8,3, 15, 20, 7});

        BTreePrinter.printNode(node);
    }
}
