package coding.test.leetcode.curated;

import java.util.*;

public class FindWordsUsingTri {

    public static class TriNode {

        final List<TriNode> links = new ArrayList<>();
        char ch;
        boolean end;

        public TriNode(char ch) {
            this.ch = ch;
        }

    }

    public static List<String> findWords(char[][] board, String[] words) {
        TriNode root = new TriNode('0');

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                char ch = board[i][j];
                TriNode link = new TriNode(ch);
                root.links.add(link);
            }
        }

        List<String> result = new ArrayList<>();

        int index = 0;
        int[][] dir = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

        for (TriNode link : root.links) {
            int x = index / board[0].length;
            int y = index % board[0].length;

            for (int[] direction : dir) {
                setLinks(board, link, root.links, x + direction[0], y + direction[1]);
            }

            index++;
        }

        for (String s : words) {
            for (TriNode link : root.links) {
                if (link.ch == s.charAt(0) && search(link, s, 0)) {
                    result.add(s);
                    break;
                }
            }
        }

        return result;
    }

    private static boolean search(TriNode node, String word, int index) {

        if (index == word.length()) {
            return true;
        }

        if (node == null) {
            return true;
        }

        char tmp = node.ch;

        boolean found = false;
        if (node.ch == word.charAt(index)) {
            node.ch = 'X';

            if (!node.links.isEmpty()) {
                for (TriNode link : node.links) {
                    found = search(link, word, index + 1);

                    if (found) {
                        break;
                    }
                }
            } else {
                return index + 1 == word.length();
            }
        }

        node.ch = tmp;

        return found;
    }

    private static void setLinks(char[][] board, TriNode child, List<TriNode> rootLinks, int x, int y) {
        if (x < 0 || y < 0 || x >= board.length || y >= board[0].length) {
            return;
        }

        int childIndex = x * board[0].length + y;
        child.links.add(rootLinks.get(childIndex));
    }

    public static void main(String[] args) {

        List<String> list = findWords(new char[][]{{'a'}
        }, new String[]{"a"});
        list.forEach(System.out::println);


        List<String> result2 = findWords(new char[][]{
                {'a', 'b', 'c'},
                {'a', 'e', 'd'},
                {'a', 'f', 'g'}
        }, new String[]{"eaabcdgfa"});

        List<String> result = findWords(new char[][]{
                        {'o', 'a', 'a', 'n'},
                        {'e', 't', 'a', 'e'},
                        {'i', 'h', 'k', 'r'},
                        {'i', 'f', 'l', 'v'}},
                new String[]{"oath", "pea", "eat", "rain"});

        result.forEach(System.out::println);

        result2.forEach(System.out::println);
    }
}
