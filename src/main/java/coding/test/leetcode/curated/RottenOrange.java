package coding.test.leetcode.curated;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class RottenOrange {

    static int orangesRotting(int[][] oranges) {
        Queue<Integer> queue = new LinkedList<>();
        int empty = 0;
        int total = 0;

        for (int i = 0; i < oranges.length; i++) {
            for (int j = 0; j < oranges[0].length; j++) {
                if (oranges[i][j] == 1) {
                    queue.add(j + i * oranges[0].length);
                } else if (oranges[i][j] == 0) {
                    empty++;
                }

                total++;
            }
        }

        if (queue.isEmpty() || (queue.size() + empty) == total) {
            return -1;
        }

        return bfs(oranges, queue);
    }

    public static int bfs(int[][] files, Queue<Integer> queue) {

        int hours = 0;
        int rem = queue.size();
        int version = 1;

        int[][] dir = new int[][]{{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

        while (!queue.isEmpty()) {
            hours++;
            version++;

            for (int c = queue.size(); c > 0; c--) {

                printArr(files);

                boolean found = false;
                Integer current = queue.poll();

                int x = current / files[0].length;
                int y = current % files[0].length;

                if (files[x][y] == 0) {
                    continue;
                }

                if (impossible(files, x, y)) {
                    return -1;
                }

                for (int[] ints : dir) {
                    int nextX = x + ints[0];
                    int nextY = y + ints[1];

                    if (nextX >= 0 && nextX < files.length && nextY >= 0 && nextY < files[0].length) {
                        if (files[nextX][nextY] > 1 && files[nextX][nextY] <= version) {
                            files[x][y] = version + 1;
                            rem--;
                            found = true;
                            break;
                        }
                    }
                }

                if (rem == 0) {
                    return hours;
                }

                if (!found) {
                    queue.add(current);
                }
            }

        }

        return -1;
    }

    public static boolean impossible(int[][] arr, int x, int y) {
        boolean left = false, right = false, top = false, bottom = false;

        if (x - 1 < 0 || arr[x - 1][y] == 0) {
            left = true;
        }
        if (x + 1 >= arr.length || arr[x + 1][y] == 0) {
            right = true;
        }
        if (y - 1 < 0 || arr[x][y - 1] == 0) {
            top = true;
        }
        if (y + 1 >= arr[0].length || arr[x][y + 1] == 0) {
            bottom = true;
        }

        return top && left && right && bottom;
    }

    private static void printArr(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(Arrays.toString(arr[i]));
        }
    }

    public static void main(String[] args) {
        int[][] arr = new int[][]{
                {2, 1, 1},
                {0, 1, 1},
                {1, 0, 1}
        };

        int hrs = orangesRotting(arr);
        System.out.println(hrs);
    }

}
