package coding.test.leetcode.curated;

import java.util.Arrays;
import java.util.Comparator;

public class EraseInterval {

    //[[1,2],[1,3],[2,3],[3,4]]

    public static int eraseOverlapIntervals(int[][] intervals) {

        int count = 0;
        Arrays.sort(intervals, Comparator.comparingInt(o -> o[0]));

        int prevEnd = intervals[0][1];

        for (int i = 1; i < intervals.length; i++) {
            int currentStart = intervals[i][0];

            if (prevEnd > currentStart) {
                prevEnd = Math.min(prevEnd, intervals[i][1]);
                count++;
            } else {
                prevEnd = intervals[i][1];
            }

        }

        return count;
    }

    public static void main(String[] args) {
        int count = eraseOverlapIntervals(new int[][]{{1, 100}, {1, 11}, {2, 12}, {11, 22}});
        System.out.println(count);
    }
}
