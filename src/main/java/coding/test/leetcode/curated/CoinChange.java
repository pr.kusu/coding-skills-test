package coding.test.leetcode.curated;

import java.util.Arrays;

public class CoinChange {

    public int coinChange(int[] coins, int n) {
        int[] countMap = new int[n + 1];
        Arrays.fill(countMap, Integer.MAX_VALUE);

        int val = helper(coins, n, countMap, 0);

        return val != Integer.MAX_VALUE ? val : -1;
    }

    private int helper(int[] coins, int rem, int[] countMap, int count) {

        if (countMap[rem] <= count) {
            return countMap[rem];
        }

        if (rem == 0) {
            int currPath = countMap[rem];
            countMap[rem] = Math.min(currPath, count);
            return countMap[0];
        }

        if (count >= countMap[0]) {
            return countMap[0];
        }

        countMap[rem] = count;

        for (int i = coins.length - 1; i >= 0; i--) {
            int next = rem - coins[i];
            if (next < 0 || countMap[next] <= count) {
                continue;
            }
            helper(coins, next, countMap, count + 1);
        }


        return countMap[0];
    }

}
