package coding.test.leetcode.curated;

import coding.test.ctci.linkedlist.ListNode;

public class LinkedListCycle {

    public boolean hasCycle(ListNode head) {

        if (head == null) {
            return false;
        }
        ListNode fast = head;
        ListNode slow = head.next;


        while (fast != null) {

            if (fast == slow) {
                return true;
            }

            if (fast.next != null && slow.next != null) {
                fast = fast.next.next;
                slow = slow.next;
            }
        }
        return false;
    }
}
