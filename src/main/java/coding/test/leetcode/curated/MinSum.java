package coding.test.leetcode.curated;

import java.util.*;

public class MinSum {

    public static int findSmallestDivisor(String s, String t) {
        if(!canForm(t,s)){
            return -1;
        }

        int i = 1;
        while (i <= t.length() / 2) {
            if (canForm(t.substring(0, i), t)) {
                return i;
            }

            i++;
        }

        return t.length();
    }

    private static boolean canForm(String s, String t) {
        StringBuilder stringBuilder = new StringBuilder();
        if (t.length() % s.length() != 0) {
            return false;
        }

        int rem = t.length() / s.length();

        for (int i = 0; i < rem; i++) {
            stringBuilder.append(s);
        }

        return stringBuilder.toString().equals(t);
    }

    public static int minSum(List<Integer> num, int k) {

        Queue<Integer> queue = new PriorityQueue<>(Collections.reverseOrder());

        num.forEach(val -> {
            queue.add(val);
        });

        for (int i = 0; i < k; i++) {
            int current = queue.poll();
            int next = (int) Math.ceil(current / 2.0);
            queue.add(next);
        }
        int sum = 0;
        while (!queue.isEmpty()) {
            sum += queue.poll();
        }

        return sum;
    }

    public static void main(String[] args) {
        int sum = minSum(Arrays.asList(10, 20, 7), 4);
        System.out.println(sum);

        int len = findSmallestDivisor("rbrb", "rbrb");

        System.out.println(len);
    }
}
