package coding.test.leetcode.curated;

public class MaxDepth {

    public int maxDepth(SameTree.TreeNode root) {
        return maxDepthHelper(root, 0);
    }

    private int maxDepthHelper(SameTree.TreeNode root, int depth) {

        if (root == null) {
            return 0;
        }

        int left = maxDepthHelper(root.left, depth + 1);
        int right = maxDepthHelper(root.right, depth + 1);

        return Math.max(left, right);
    }
}
