package coding.test.leetcode.curated;

import java.util.Arrays;

public class WordDictionary {

    private TriNode head = new TriNode('0');

    /**
     * Initialize your data structure here.
     */
    public WordDictionary() {

    }

    /**
     * Adds a word into the data structure.
     */
    public void addWord(String word) {
        TriNode start = head;

        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);

            if (!start.contains(ch)) {
                start.insert(ch);
            }
            start = start.linkAt(ch);
        }
        start.end = true;
    }

    /**
     * Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter.
     */
    public boolean search(String word) {
        TriNode start = head;

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != '.') {
                builder.append(word.charAt(i));
            }
        }

        word = builder.toString();

        for (int i = 0; i < word.length(); i++) {
            if (start.contains(word.charAt(i))) {
                start = start.linkAt(word.charAt(i));
            } else {
                return false;
            }
        }

        return start != null && start.isEnd();


    }

    public static void main(String[] args) {
        WordDictionary trie = new WordDictionary();

        trie.addWord("dad");
        trie.addWord("mad");
        boolean result = trie.search(".ad");
        // returns true
        boolean result2 = trie.search("ad.");     // returns false
        boolean result5 = trie.search("b..");

        System.out.println(Arrays.toString(new boolean[]{result, result2, result5}));
    }
}
