package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ThreeSum {

    static class Triplet {
        int a;
        int b;
        int c;

        public Triplet(int a, int b, int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }

            Triplet cast = (Triplet) o;

            boolean value = (cast.a == a || cast.a == b || cast.a == c) &&
                    (cast.b == a || cast.b == b || cast.b == c) &&
                    (cast.c == a || cast.c == b || cast.c == c);

            return value;
        }

        @Override
        public int hashCode() {
            return Integer.hashCode(a) + Integer.hashCode(b) + Integer.hashCode(c);
        }
    }

    public static List<List<Integer>> threeSum(int[] nums) {
        Map<Integer, Integer> freq = new HashMap<>();
        for (int num : nums) {
            if (!freq.containsKey(num)) {
                freq.put(num, 1);
            } else {
                freq.put(num, freq.get(num) + 1);
            }
        }

        List<List<Integer>> result = new ArrayList<>();

        Set<Triplet> triplets = new HashSet<>();

        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {

                int c = -(nums[i] + nums[j]);

                if(c!=0 &&( i==j || c==nums[i] || c==nums[j])){
                    continue;
                }

                if (freq.containsKey(c) && freq.get(c) > 0 && !triplets.contains(new Triplet(c, nums[i], nums[j])))                    {
                    result.add(Arrays.asList(c, nums[i], nums[j]));
                    triplets.add(new Triplet(c, nums[i], nums[j]));

                    freq.put(c, freq.get(c) - 1);
                }
            }
        }


        return result;
    }

    public static void main(String[] args) {
        List<List<Integer>> sums = threeSum(new int[]{0,0,0});
    }
}
