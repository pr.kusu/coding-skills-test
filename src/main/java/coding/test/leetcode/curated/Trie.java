package coding.test.leetcode.curated;

import java.util.Arrays;

public class Trie {

     TriNode head;

    /**
     * Initialize your data structure here.
     */
    public Trie() {
        head = new TriNode('0');
    }

    public TriNode getHead(){
        return head;
    }

    /**
     * Inserts a word into the trie.
     */
    public void insert(String word) {
        TriNode start = head;

        for (int i = 0; i < word.length(); i++) {
            if (!start.contains(word.charAt(i))) {
                start.insert(word.charAt(i));
                start = start.linkAt(word.charAt(i));
            } else {
                start = start.linkAt(word.charAt(i));
            }
        }

        start.end = true;
        start.word = word;
    }

    /**
     * Returns if the word is in the trie.
     */
    public boolean search(String word) {
        TriNode node = searchNode(word);

        return node != null && node.isEnd();
    }

    /**
     * Returns if there is any word in the trie that starts with the given prefix.
     */
    public boolean startsWith(String prefix) {
        TriNode node = searchNode(prefix);

        return node != null;
    }

    private TriNode searchNode(String word) {
        TriNode start = head;

        for (int i = 0; i < word.length(); i++) {
            if (start.contains(word.charAt(i))) {
                start = start.linkAt(word.charAt(i));
            } else {
                return null;
            }
        }

        return start;
    }

    public static void main(String[] args) {
        Trie trie = new Trie();

        trie.insert("apple");
        trie.insert("appre");
        boolean result = trie.search("apple");
        // returns true
        boolean result2 = trie.search("app");     // returns false
        boolean resul3 = trie.startsWith("app"); // returns true
        boolean result5 = trie.search("app");

        System.out.println(Arrays.toString(new boolean[]{result, result2, resul3, result5}));
    }
}
