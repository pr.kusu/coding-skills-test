package coding.test.leetcode.curated;

import java.util.Arrays;

public class MaxProduct {

    public static int maxProduct(int[] nums) {

        int[] maxProduct = new int[nums.length];

        Arrays.fill(maxProduct, Integer.MIN_VALUE);
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < nums.length; i++) {
            int product = nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                maxProduct[i] = Math.max(maxProduct[i], product);
                product *= nums[j];
            }
            maxProduct[i] = Math.max(maxProduct[i], product);

            max = Math.max(maxProduct[i], max);
        }

        return max;

    }

    public static void main(String[] args) {

        int val = maxProduct(new int[]{-3, 0, 1, -2});

        System.out.println(val);
    }
}
