package coding.test.leetcode.curated;

import java.util.*;

public class MergeInterval {

    private static class IntervalComparator implements Comparator<int[]> {
        @Override
        public int compare(int[] a, int[] b) {
            return a[0] < b[0] ? -1 : a[0] == b[0] ? 0 : 1;
        }
    }

    public static int[][] merge(int[][] intervals) {

        Collections.sort(Arrays.asList(intervals), new IntervalComparator());


        if (intervals.length < 2) {
            return intervals;
        }

        List<int[]> result = new ArrayList<>();

        for (int i = 0; i < intervals.length - 1; i++) {
            int[] first = intervals[i];
            int[] second = intervals[i + 1];

            if (first[1] >= second[0]) {
                second[0] = Math.min(first[0], second[0]);
                second[1] = Math.max(first[1], second[1]);
            } else {
                result.add(first);
            }

        }


        result.add(intervals[intervals.length - 1]);

        return result.toArray(new int[result.size()][]);
    }
}
