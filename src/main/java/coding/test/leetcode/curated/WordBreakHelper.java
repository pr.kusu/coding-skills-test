package coding.test.leetcode.curated;

import java.util.Arrays;
import java.util.List;

public class WordBreakHelper {

    public static boolean wordBreak(String s, List<String> dict) {

        boolean[] f = new boolean[s.length() + 1];

        f[0] = true;

        //Second DP
        for(int i=1; i <= s.length(); i++){
            for(int j=0; j < i; j++){
                if(f[j] && dict.contains(s.substring(j, i))){
                    f[i] = true;
                    break;
                }
            }
        }

        return f[s.length()];
    }

    public static void main(String[] args) {

        List<String> dict = Arrays.asList("cats", "dog", "sand", "and", "cat");

        List<String> dict2 = Arrays.asList("a", "abc", "b", "cd");

        boolean result = wordBreak("abcd", dict);

        System.out.println(result);
    }
}
