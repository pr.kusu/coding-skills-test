package coding.test.leetcode.curated;

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode next;

    public TreeNode(int num) {
        this.val = num;
    }
}
