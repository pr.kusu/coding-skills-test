package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public class NumIslands2 {

    public int numIslands(char[][] grid) {
        int count = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = i + 1; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {
                    bfs(i, j, grid);
                    count++;
                }
            }
        }

        return count;
    }

    private void bfs(int i, int j, char[][] grid) {
        Queue<int[]> list = new LinkedList<>();

        list.add(new int[]{i, j});
        while (!list.isEmpty()) {
            int[] current = list.poll();
            int x = current[0];
            int y = current[1];

            List<int[]> adjs = getAdjcents(x, y, grid);

            for (int[] adj : adjs) {
                int xx = adj[0];
                int yy = adj[1];
                if (grid[xx][yy] == '1') {
                    list.add(adj);
                    grid[xx][yy] = 'X';
                }
            }

            grid[x][y] = 'X';
        }
    }

    private List<int[]> getAdjcents(int x, int y, char[][] grid) {
        List<int[]> list = new ArrayList<>();
        list.add(new int[]{x + 1, y});
        list.add(new int[]{x - 1, y});
        list.add(new int[]{x, y - 1});
        list.add(new int[]{x, y + 1});

        return list.stream().filter(value -> value[0] >= 0 && value[0] < grid.length
                && value[1] >= 0 && value[1] < grid[0].length).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        char[][] grid = new char[][]{
                {'1', '1', '1', '1', '0'},
                {'1', '1', '0', '1', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '0', '0', '0'}};

        int val = new NumIslands2().numIslands(grid);
        System.out.println(val);
    }
}
