package coding.test.leetcode.curated;

public class CountPalindromes2 {

    static int count = 0;

    public static int countSubstrings(String s) {
        for (int i = 0; i < s.length(); i++) {

            countPalindrome(i, i, s);
            countPalindrome(i, i + 1, s);
        }

        return count;
    }

    public static void countPalindrome(int start, int end, String s) {
        while (start >= 0 && end < s.length()) {
            if (s.charAt(start) == s.charAt(end)) {
                count++;
            } else {
                break;
            }
            count++;
            start--;
            end++;
        }
    }

    public static void main(String[] args) {

        // System.out.println(isPalindrome(0, 4, "abcba"));
        // System.out.println(isPalindrome(0, 4, "casdfsd"));
        //System.out.println(isPalindrome(0, 3, "abba"));

        System.out.println(countSubstrings("fdsklf"));
    }
}
