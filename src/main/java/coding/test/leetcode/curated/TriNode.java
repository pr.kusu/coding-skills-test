package coding.test.leetcode.curated;

public class TriNode {

    final TriNode[] links = new TriNode[26];
    char ch;
    boolean end;
    String word;

    public TriNode(char ch) {
        this.ch = ch;
    }

    public boolean contains(char ch) {
        return links[ch - 'a'] != null;
    }

    public TriNode linkAt(char ch) {
        return links[ch - 'a'];
    }

    public void insert(char ch) {
        links[ch - 'a'] = new TriNode(ch);
    }

    public boolean isEnd() {
        return end;
    }
}
