package coding.test.leetcode.curated;

public class LinkedListInTree {

    public static boolean isSubPath(ListNode head, TreeNode root) {
        if (root == null) return false;
        if (head == null) return true;

        return helper(root, head) || isSubPath(head, root.left) || isSubPath(head, root.right);
    }

    private static boolean helper(TreeNode root, ListNode head) {
        if (head == null) return true;
        if (root == null) return false;

        return root.val == head.val && (helper(root.left, head.next) || helper(root.right, head.next));
    }
}
