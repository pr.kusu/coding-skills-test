package coding.test.leetcode.curated;

import coding.test.ctci.tree.TreeNode;

public class PathSum {

    public boolean hasPathSum(TreeNode<Integer> root, int sum) {

        if (root == null) {
            return false;
        }

        return hasPathSumHelper(root, sum);
    }

    public boolean hasPathSumHelper(TreeNode<Integer> root, int sum) {

        if (root == null) {
            return sum == 0;
        }

        int rem = sum - root.val;

        if (root.left == null) {
            return hasPathSumHelper(root.right, rem);
        } else if (root.right == null) {
            return hasPathSumHelper(root.left, rem);
        }
        return hasPathSumHelper(root.left, rem) ||
                hasPathSumHelper(root.right, rem);
    }
}
