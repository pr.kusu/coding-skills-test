package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MaxNonRepeating {

    public static int optimized(String s) {

        //abcadebc

        Map<Character, Integer> indexMap = new HashMap<>();

        int startIndex = 0;
        int currentIndex = 0;
        int maxSoFar = 0;

        while (startIndex < s.length() && currentIndex < s.length()) {

            char ch = s.charAt(startIndex);

            if (indexMap.containsKey(ch)) {
                currentIndex = Math.max(currentIndex, indexMap.get(ch));
            }

            maxSoFar = Math.max(maxSoFar, (startIndex - currentIndex) + 1);

            indexMap.put(ch, startIndex + 1);

        }

        return maxSoFar;
    }

    public int bruteForce(String s) {

        List<String> subStrings = new ArrayList<>();

        int maxLen = 0;

        for (int j = 0; j < s.length(); j++) {
            for (int i = j + 1; i <= s.length(); i++) {
                String sub = s.substring(j, i);

                if (hasUnique(sub) && sub.length() > maxLen) {
                    maxLen = sub.length();
                }
            }
        }

        return maxLen;
    }

    public boolean hasUnique(String s) {
        Set<Character> charSet = new HashSet<>();

        for (char c : s.toCharArray()) {
            if (charSet.contains(c)) {
                return false;
            }
            charSet.add(c);
        }

        return true;
    }
}
