package coding.test.leetcode.curated;

public class IncreasingSubSequence {

    public static int lengthOfLIS(int[] nums) {

        if (nums.length == 0) {
            return 0;
        }

        return 0;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{10, 9, 2, 5, 3, 7, 101, 18};

        int len = lengthOfLIS(arr);

        int len2 = lengthOfLIS(new int[]{2, 2, 2});

        System.out.println(len + "," + len2);
    }
}
