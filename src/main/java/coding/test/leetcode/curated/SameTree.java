package coding.test.leetcode.curated;

import java.util.LinkedList;
import java.util.Queue;

public class SameTree {


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public boolean isSameTree(TreeNode p, TreeNode q) {

        String s1 = inorderHelper(p, new StringBuilder());
        String s2 = inorderHelper(q, new StringBuilder());

        return s1.equals(s2);
    }

    private String inorderHelper(TreeNode node, StringBuilder builder) {
        if (node == null) {
            builder.append("X");
            return builder.toString();
        }

        inorderHelper(node.left, builder);

        builder.append(node.val);

        inorderHelper(node.right, builder);

        return builder.toString();
    }

    private boolean check(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }

        if (p == null || q == null) {
            return false;
        }

        return p.val == q.val;

    }


    public boolean isSameTreeItr(TreeNode p, TreeNode q) {
        Queue<TreeNode> pQ = new LinkedList<>();
        Queue<TreeNode> qQ = new LinkedList<>();

        pQ.add(p);
        qQ.add(q);

        while (!pQ.isEmpty()) {

            TreeNode pCurrent = pQ.poll();
            TreeNode qCurrent = qQ.poll();

            if (!check(pCurrent, qCurrent)) {
                return false;
            }

            if (pCurrent != null) {

                if (!check(pCurrent.left, qCurrent.left)) {
                    return false;
                }

                pQ.add(pCurrent.left);
                qQ.add(qCurrent.left);

                if (!check(pCurrent.right, qCurrent.right)) {
                    return false;
                }

                pQ.add(pCurrent.right);
                qQ.add(qCurrent.right);

            }

        }

        return true;

    }
}
