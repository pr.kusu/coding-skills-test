package coding.test.leetcode.curated;

public class ValidateBST {

    public boolean isValidBST(TreeNode root) {

        return helper(root, null, null);

    }

    private boolean helper(TreeNode root, Integer lower, Integer upper) {

        if (root == null) {
            return true;
        }

        if (root.left != null && root.left.val > lower) {
            return false;
        }
        if (root.right != null && root.right.val < upper) {
            return false;
        }

        return helper(root.left, root.val, null) && helper(root.right, null, root.val);
    }
}
