package coding.test.leetcode.curated;

import java.util.*;

public class LongestConsecutive {

    // [100, 4, 200, 1, 3, 2]

    public static int longestConsecutive2(int[] nums) {
        if (nums.length < 1) {
            return 0;
        }

        Set<Integer> hash = new HashSet<>();

        for (int i = 0; i < nums.length; i++) {
            hash.add(nums[i]);
        }

        int max = 0;

        for (int i = 0; i < nums.length; i++) {

            if (!hash.contains(nums[i] - 1)) {
                int current = 1;

                while (hash.contains(nums[i] + 1)) {
                    current++;
                    max = Math.max(current, max);
                }
            }
        }

        return max;
    }

    public static int longestConsecutive(int[] nums) {
        Arrays.sort(nums);
        int maxCons = 1;
        int currentLength = 1;

        System.out.println(Arrays.toString(nums));

        for (int i = 1; i < nums.length; i++) {
            int diff = nums[i] - nums[i - 1];

            if (diff == 1) {
                currentLength++;
                maxCons = Math.max(currentLength, maxCons);
            } else if (diff == 0) {
                continue;
            } else {
                currentLength = 0;
            }
        }

        return maxCons;
    }

    public static void main(String[] args) {
        System.out.println(longestConsecutive(new int[]{9, 1, 4, 7, 3, -1, 0, 5, 8, -1, 6}));
    }
}
