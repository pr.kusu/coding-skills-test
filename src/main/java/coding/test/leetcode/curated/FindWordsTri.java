package coding.test.leetcode.curated;

import java.util.ArrayList;
import java.util.List;

public class FindWordsTri {

    public static List<String> findWords(char[][] board, String[] words) {
        List<String> res = new ArrayList<>();
        TrieNode root = buildTrie(words);
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                dfs(board, i, j, root, res);
            }
        }
        return res;
    }

    public static void dfs(char[][] board, int i, int j, TrieNode p, List<String> res) {
        char c = board[i][j];
        if (c == '#' || p.next[c - 'a'] == null) return;
        p = p.next[c - 'a']; // abcde, abcdeg
        if (p.word != null) {   // found one
            res.add(p.word);
            p.word = null;     // de-duplicate
        }

        board[i][j] = '#';
        if (i > 0) dfs(board, i - 1, j, p, res);
        if (j > 0) dfs(board, i, j - 1, p, res);
        if (i < board.length - 1) dfs(board, i + 1, j, p, res);
        if (j < board[0].length - 1) dfs(board, i, j + 1, p, res);
        board[i][j] = c;
    }

    public static TrieNode buildTrie(String[] words) {
        TrieNode root = new TrieNode();
        for (String w : words) {
            TrieNode p = root;
            for (char c : w.toCharArray()) {
                int i = c - 'a';
                if (p.next[i] == null) p.next[i] = new TrieNode();
                p = p.next[i];
            }
            p.word = w;
        }
        return root;
    }

    static class TrieNode {
        TrieNode[] next = new TrieNode[26];
        String word;
    }

    public static void main(String[] args) {

        List<String> list = findWords(new char[][]{{'a'}
        }, new String[]{"a"});
        list.forEach(System.out::println);


        long start = System.nanoTime();
        List<String> result2 = findWords(new char[][]{
                {'a', 'b', 'c','d'},
                {'a', 'e', 'd','h'},
                {'g', 'f', 'g','f'},
                {'a', 'e', 'd','o'}
        }, new String[]{"bcdhdeagdasfd", "bcdhdeagfgfodea", "cdhdeagfgfodea"});

        long end = System.nanoTime();

        System.out.println(end - start);

        List<String> result = findWords(new char[][]{
                        {'o', 'a', 'a', 'n'},
                        {'e', 't', 'a', 'e'},
                        {'i', 'h', 'k', 'r'},
                        {'i', 'f', 'l', 'v'}},
                new String[]{"oath", "pea", "eat", "rain"});

        result.forEach(System.out::println);

        result2.forEach(System.out::println);
    }
}
