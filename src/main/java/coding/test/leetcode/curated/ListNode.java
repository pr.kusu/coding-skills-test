package coding.test.leetcode.curated;

public  class ListNode {
    public int val;
    public ListNode next;
    public ListNode random;

    public ListNode(int x) {
        val = x;
    }
}