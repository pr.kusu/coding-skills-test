package coding.test.leetcode.curated;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class CloneGraph {

    class Node {
        public int val;
        public List<Node> neighbors;

        public Node(int val) {
            this.val = val;
        }
    }

    private Map<Node, Node> visited = new HashMap<>();


    public Node cloneGraphRec(Node node) {
        if (node == null) {
            return null;
        }

        if (visited.containsKey(node)) {
            return visited.get(node);
        }

        Node clone = new Node(node.val);
        visited.put(node, clone);

        List<Node> childs = new ArrayList<>();

        if (node.neighbors != null) {
            for (Node neighbor : node.neighbors) {

                Node cloned = cloneGraph(neighbor);
                childs.add(cloned);
            }
        }
        clone.neighbors = childs;

        return clone;
    }

    public Node cloneGraph(Node node) {

        Queue<Node> queue = new ArrayBlockingQueue<>(1000);

        queue.add(node);

        while (!queue.isEmpty()) {

            Node item = queue.poll();

            for (Node childs : item.neighbors) {

                if (!visited.containsKey(childs)) {
                    Node child = new Node(childs.val);

                    visited.put(childs, child);
                    queue.add(childs);
                }

                if (visited.get(item).neighbors == null) {
                    visited.get(item).neighbors = new ArrayList<>();
                }

                visited.get(item).neighbors.add(visited.get(childs));

            }

        }

        return visited.get(node);
    }

}
