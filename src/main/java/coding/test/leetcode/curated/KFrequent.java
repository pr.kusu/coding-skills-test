package coding.test.leetcode.curated;

import java.util.*;

public class KFrequent {

    public static int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], map.getOrDefault(nums[i], 0) + 1);
        }

        Queue<int[]> queue = new PriorityQueue<>(new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return Integer.compare(o1[1], o2[1]);
            }
        });

        map.entrySet().forEach(
                val -> {
                    if (queue.size() < k) {
                        queue.add(new int[]{val.getKey(), val.getValue()});
                    } else {
                        int[] current = queue.peek();

                        if (val.getValue() >= current[1]) {
                            queue.poll();
                            queue.add(new int[]{val.getKey(), val.getValue()});
                        }
                    }
                }
        );

        int[] result = new int[queue.size()];

        int i = 0;
        while (i < k) {
            result[i++] = queue.poll()[0];
        }
        System.out.println(Arrays.toString(result));
        return result;
    }

    public static void main(String[] args) {
        topKFrequent(new int[]{4, 1, -1, 2, -1, 2, 3}, 2);
    }
}
