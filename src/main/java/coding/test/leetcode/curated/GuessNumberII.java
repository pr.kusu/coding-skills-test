package coding.test.leetcode.curated;

import java.util.Arrays;

public class GuessNumberII {

    public int getMoneyAmount(int n) {
        Integer[][] dp = new Integer[n + 1][n + 1];
        for (Integer[] row : dp) {
            Arrays.fill(row, Integer.MAX_VALUE);
        }

        return helper(1, n, dp);
    }

    private int helper(int start, int end, Integer[][] dp) {
        if (start >= end) {
            return 0;
        }
        if (dp[start][end] != Integer.MAX_VALUE) {
            return dp[start][end];
        }

        for (int i = start; i <= end; i++) {
            dp[start][end] = Math.min(dp[start][end],
                    Math.max(i + helper(start, i - 1, dp),
                            i + helper(i + 1, end, dp)));
        }

        return dp[start][end];
    }

}
