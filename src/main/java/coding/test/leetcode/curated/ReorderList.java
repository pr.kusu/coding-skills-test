package coding.test.leetcode.curated;


import java.util.ArrayList;
import java.util.List;

public class ReorderList {

    //Given 1->2->3->4->5, reorder it to 1->5->2->4->3.

    public static void reorderList(ListNode head) {

        List<ListNode> list = new ArrayList<>();
        int i = 0;

        ListNode start = head;
        while (start != null) {
            list.add(start);
            start = start.next;
        }

        ListNode begin = head;
        ListNode unchanged = head.next;
        int n = list.size() - 1;
        int startIndex = 1;
        while (unchanged.next != null && n > list.size() / 2) {
            unchanged = list.get(startIndex);

            begin.next = list.get(n);
            n--;
            begin.next.next = unchanged;

            begin = unchanged;
            startIndex++;
        }

        list.get(list.size() - startIndex).next = null;
    }

    public static void main(String[] args) {
        ListNode node = new ListNode(1);
        node.next = new ListNode(2);
        node.next.next = new ListNode(3);
        node.next.next.next = new ListNode(4);
        // node.next.next.next.next = new ListNode(5);

        reorderList(node);

        System.out.println();
    }

}
