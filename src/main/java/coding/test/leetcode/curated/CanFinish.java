package coding.test.leetcode.curated;

import java.util.LinkedList;
import java.util.Queue;

public class CanFinish {

    public static boolean canFinish(int numCourses, int[][] prerequisites) {
        int[] indegree = new int[numCourses];

        for (int i = 0; i < prerequisites.length; i++) {
            indegree[prerequisites[i][0]]++;
        }

        Queue<Integer> queue = new LinkedList<>();

        int courses = 0;
        for (int i = 0; i < indegree.length; i++) {
            if (indegree[i] == 0) {
                queue.add(i);
                courses++;
            }
        }

        while (!queue.isEmpty()) {
            int currePre = queue.poll();

            for (int i = 0; i < prerequisites.length; i++) {

                if (prerequisites[i][1] == currePre) {
                    indegree[prerequisites[i][0]]--;

                    if (indegree[prerequisites[i][0]] == 0) {
                        courses++;
                        queue.add(prerequisites[i][0]);
                    }
                }

            }

        }

        return courses == numCourses;
    }

    public static void main(String[] args) {
        canFinish(3, new int[][]{{1, 0}, {2, 1}});

    }
}
