package coding.test.leetcode.dynamic;

public class Triangulated {

    public int minScoreTriangulation1(int[] A) {
        int l = A.length;
        Integer[][] dp = new Integer[l][l];

        return dp(A, 0, A.length - 1, dp);
    }

    public int dp(int[] A, int i, int j, Integer[][] dpVal) {
        if (i + 2 > j) {
            dpVal[i][j] = 0;
            return 0;
        }
        if (dpVal[i][j] != null)
            return dpVal[i][j];
        int min = Integer.MAX_VALUE;
        for (int k = i + 1; k < j; k++) {
            min = Math.min(min, dp(A, i, k, dpVal) + dp(A, k, j, dpVal) + A[i] * A[j] * A[k]);
        }
        dpVal[i][j] = min;
        return min;
    }

    public int minScoreTriangulation(int[] A) {
        int l = A.length;
        int[][] dp = new int[l][l];

        for (int i = l - 3; i >= 0; i--) {
            for (int j = i + 2; j < l; j++) {
                for (int k = i + 1; k < j; k++) {
                    if (dp[i][j] == 0) {
                        dp[i][j] = dp[i][k] + dp[k][j] + A[i] * A[j] * A[k];
                    } else {
                        dp[i][j] = Math.min(dp[i][k] + dp[k][j] + A[i] * A[j] * A[k], dp[i][j]);
                    }
                }
            }
        }

        return dp[0][l - 1];
    }
}
