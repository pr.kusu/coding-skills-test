package coding.test.leetcode.dynamic;

public class OutOfBoundryPaths {

    private final int M = (int) Math.pow(10, 9) + 7;

    public int findPaths(int m, int n, int N, int i, int j) {
        Integer[][][] memo = new Integer[m][n][N + 1];

        return helper(m, n, N, i, j, memo);
    }

    private int helper(int m, int n, int N, int i, int j, Integer[][][] memo) {

        if (i < 0 || j < 0 || i >= m || j >= n) {
            return 1;
        }

        if (N == 0) {
            return 0;
        }

        if (memo[i][j][N] != null) {
            return memo[i][j][N];
        }

        int ways = 0;

        int[][] dir = new int[][]{{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        for (int[] d : dir) {
            ways = ways % M + helper(m, n, N - 1, i + d[0], j + d[1], memo) % M;
        }

        memo[i][j][N] = ways % M;
        return ways % M;
    }
}
