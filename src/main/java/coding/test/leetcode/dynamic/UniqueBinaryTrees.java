package coding.test.leetcode.dynamic;

public class UniqueBinaryTrees {

    public int numTrees(int n) {

        int[] dp = new int[n + 1];

        dp[0] = 1;
        dp[1] = 1;

        // dp[4]=dp[1]*dp[3]
        // dp[i]=dp[i-j]*dp[j-1]
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j < i; j++) {
                dp[i] += dp[i - j] * dp[j - 1];
            }
        }

        return dp[n];
    }

    public int numTreesDy(int n) {
        int[] dp = new int[n + 1];

        return helper(n, dp);
    }

    public int helper(int n, int[] dp) {
        if (n == 0) {
            return 1;
        }

        if (n < 3) {
            return n;
        }

        if (dp[n] != 0) {
            return dp[n];
        }

        int sum = 0;

        for (int i = 1; i <= n; i++) {
            sum += helper(n - i, dp) * helper(i - 1, dp);
        }

        dp[n] = sum;

        return dp[n];
    }
}
