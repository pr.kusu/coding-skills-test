package coding.test.leetcode.dynamic;

public class BestTimeToBuyWithCool {

    //[1,2,3,0,2]
    public int maxProfit(int[] prices) {
        int held = Integer.MIN_VALUE;
        int reset = 0;
        int sold = Integer.MIN_VALUE;

        for (int price : prices) {
            int preHeld = held;

            held = Math.max(preHeld, reset - price);
            reset = Math.max(reset, sold);
            sold = preHeld + price;
        }

        return Math.max(sold, reset);
    }

    public int maxProfit2(int[] prices) {
        int[] mp = new int[prices.length + 2];

        for (int i = prices.length - 1; i >= 0; i--) {
            int c1 = 0;
            for (int j = i + 1; j < prices.length; j++) {
                int profit = prices[j] - prices[i] + mp[j + 2];
                c1 = Math.max(c1, profit);
            }

            int c2 = mp[i + 1];
            mp[i] = Math.max(c1, c2);
        }

        return mp[0];
    }
}
