package coding.test.leetcode.dynamic;

import java.util.Deque;
import java.util.LinkedList;

public class MinCostTreeToLeaf {

    public int mctFromLeafValues(int[] A) {
        int res = 0;
        Deque<Integer> stack = new LinkedList<>();
        stack.push(Integer.MAX_VALUE);

        for (int val : A) {
            while (stack.peek() <= val) {
                res += stack.pop() * Math.min(stack.peek(), val);
            }

            stack.push(val);
        }
        while (stack.size() > 2) {
            res += stack.pop() * stack.peek();
        }

        return res;
    }
}
