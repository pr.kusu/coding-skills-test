package coding.test.leetcode.dynamic;

public class MaxTurbulentSubArray {

    public int maxTurbulenceSize2(int[] A) {
        int inc = 1, dec = 1, result = 1;
        for (int i = 1; i < A.length; i++) {
            if (A[i] < A[i - 1]) {
                dec = inc + 1;
                inc = 1;
            } else if (A[i] > A[i - 1]) {
                inc = dec + 1;
                dec = 1;
            } else {
                inc = 1;
                dec = 1;
            }
            result = Math.max(result, Math.max(dec, inc));
        }
        return result;
    }

    public int maxTurbulenceSize(int[] A) {
        int[] dp = new int[A.length];
        int max;
        if (A.length == 1) {
            return 1;
        }

        if (A[0] == A[1]) {
            dp[0] = 1;
            dp[1] = 1;
        } else {
            dp[0] = 1;
            dp[1] = 2;
        }

        max = Math.max(dp[0], dp[1]);

        for (int i = 2; i < A.length; i++) {
            if (A[i] == A[i - 1]) {
                dp[i] = 1;
            } else if ((A[i - 1] > A[i - 2] && A[i - 1] > A[i]) ||
                    (A[i - 1] < A[i - 2] && A[i - 1] < A[i])) {
                dp[i] = dp[i - 1] + 1;
            } else {
                dp[i] = 2;
            }
            max = Math.max(max, dp[i]);
        }

        return max;
    }
}
