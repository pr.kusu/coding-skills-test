package coding.test.leetcode.dynamic;

import java.util.Arrays;

public class MinFallingPath {

    public int minFallingPathSum(int[][] A) {
        int[][] dp = new int[A.length + 1][A[0].length];

        for (int i = A.length - 1; i >= 0; i--) {

            for (int j = 0; j < A[0].length; j++) {
                int min = dp[i + 1][j];
                for (int k = -1; k <= 1; k++) {
                    int curj = j + k;

                    if (curj >= 0 && curj < A[0].length) {
                        min = Math.min(min, dp[i + 1][curj]);
                    }
                }
                dp[i][j] = A[i][j] + min;
            }

        }

        return Arrays.stream(dp[0]).min().getAsInt();
    }

    public int minFallingPathSum2(int[][] A) {
        Integer[][] dp = new Integer[A.length][A[0].length];

        int min = Integer.MAX_VALUE;
        for (int i = 0; i < A[0].length; i++) {
            int ret = helper(A, dp, 0, i);
            min = Math.min(min, ret);
        }

        return min;
    }

    private int helper(int[][] A, Integer[][] dp, int i, int j) {
        if (j < 0 || j >= A[0].length) {
            return Integer.MAX_VALUE / 2;
        }

        if (i == A.length - 1) {
            return A[i][j];
        }

        if (dp[i][j] != null) {
            return dp[i][j];
        }

        int left = helper(A, dp, i + 1, j - 1) + A[i][j];
        int mid = helper(A, dp, i + 1, j) + A[i][j];
        int right = helper(A, dp, i + 1, j + 1) + A[i][j];
        int min = Math.min(left, Math.min(mid, right));

        dp[i][j] = min;

        return dp[i][j];
    }
}
