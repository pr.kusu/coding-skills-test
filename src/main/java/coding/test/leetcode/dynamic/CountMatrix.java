package coding.test.leetcode.dynamic;

public class CountMatrix {

    public int countSquares(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;

        int[][] dp = new int[m + 1][n + 1];
        int count = 0;

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (matrix[i - 1][j - 1] != 0) {
                    dp[i][j] = 1 + Math.min(Math.min(dp[i - 1][j], dp[i][j - 1]), dp[i - 1][j - 1]);
                    count += dp[i][j];
                }
            }
        }

        return count;
    }

    public static void main(String[] args) {
        // [1,1,0]
        // [1,2,1]
        // [1,2,1]
        // ]

        int[][] marix = new int[][]{
                {0, 1, 1, 1},
                {1, 1, 1, 1},
                {0, 1, 1, 1}};
        new MatrixBlockSum().matrixBlockSum(marix, 1);
    }
}
