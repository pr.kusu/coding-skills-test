package coding.test.leetcode.dynamic;

import java.util.HashMap;
import java.util.Map;

public class LongestDiffSubsequence {

    public int longestSubsequence(int[] arr, int difference) {
        int result = 1;

        Map<Integer, Integer> map = new HashMap<>();
        for (int i : arr) {
            int tmp = map.getOrDefault(i - difference, 0) + 1;
            map.put(i, tmp);
            result = Math.max(result, tmp);
        }
        return result;
    }
}
