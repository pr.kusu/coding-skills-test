package coding.test.leetcode.dynamic;

public class LongestPalindromicSubSeq {

    public int longestPalindromeSubseq2(String s) {
        int[][] dp = new int[s.length() + 1][s.length() + 1];

        for (int i = s.length() - 1; i >= 0; i--) {
            dp[i][i] = 1;
            for (int j = i + 1; j < s.length(); j++) {
                if (s.charAt(i) == s.charAt(j)) {
                    dp[i][j] = 2 + dp[i + 1][j - 1];
                } else {
                    dp[i][j] = Math.max(dp[i + 1][j], dp[i][j - 1]);
                }
            }
        }

        return dp[0][s.length() - 1];
    }


    public int longestPalindromeSubseq(String s) {
        int[][] dp = new int[s.length() + 1][s.length() + 1];

        return helper(s, 0, s.length() - 1, dp);
    }

    private int helper(String str, int s, int e, int[][] dp) {
        if (s > e) {
            return 0;
        }

        if (s == e) {
            return 1;
        }

        if (dp[s][e] != 0) {
            return dp[s][e];
        }

        int len = 0;
        if (str.charAt(s) == str.charAt(e)) {
            len = 2 + helper(str, s + 1, e - 1, dp);
        } else {
            int left = helper(str, s + 1, e, dp);
            int right = helper(str, s, e - 1, dp);
            len = Math.max(left, right);
        }
        dp[s][e] = len;

        return len;
    }
}
