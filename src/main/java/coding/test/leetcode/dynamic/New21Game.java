package coding.test.leetcode.dynamic;

public class New21Game {

    public double new21Game(int N, int K, int W) {
        double[] dp = new double[N + W + 1];

        for (int k = K; k <= N; k++) {
            dp[k] = 1.0;
        }
        double S = Math.min(N - K + 1, W);

        for (int i = K - 1; i >= 0; i--) {
            dp[i] = S / W;

            S += dp[i] - dp[i + W];
        }

        return dp[0];
    }

    public double new21Game2(int N, int K, int W) {
        if (K == 0 || K + W <= N) {
            return 1;
        }
        if (N < K) {
            return 0;
        }
        double base = 1.0 / W;
        double[] dp = new double[N + 1];
        double sum = 0;
        double res = 0;
        for (int i = 1; i <= N; ++i) {
            dp[i] = sum * base + (i <= W ? base : 0);
            if (i < K) {
                sum += dp[i];
            }
            if (i > W) {
                sum -= dp[i - W];
            }
            if (i >= K) {
                res += dp[i];
            }
        }
        return res;
    }
}
