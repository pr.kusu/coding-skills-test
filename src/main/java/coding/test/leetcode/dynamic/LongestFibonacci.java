package coding.test.leetcode.dynamic;

public class LongestFibonacci {

    public int lenLongestFibSubseq(int[] A) {
        int[][] dp = new int[A.length][A.length];
        int max = 0;

        for (int i = 2; i < A.length; i++) {
            int l = 0;
            int r = i - 1;

            while (l < r) {
                int sum = A[l] + A[r];

                if (sum < A[i]) {
                    l++;
                } else if (sum > A[i]) {
                    r--;
                } else {
                    dp[r][i] = dp[l][r] + 1;
                    max = Math.max(max, dp[r][i]);
                    l++;
                    r--;
                }

            }
        }

        return max == 0 ? max : max + 2;
    }
}
