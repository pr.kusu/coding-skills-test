package coding.test.leetcode.dynamic;

public class StoneGame {

    public boolean stoneGame(int[] piles) {
        Integer[][] dp = new Integer[piles.length][piles.length];

        return score(piles, 0, piles.length - 1, dp) >= 0;
    }

    private int score(int[] nums, int s, int e, Integer[][] dp) {
        if (s >= e) {
            return nums[s];
        }
        if (dp[s][e] != null) {
            return dp[s][e];
        }

        int left = nums[s] - score(nums, s + 1, e, dp);
        int right = nums[e] - score(nums, s, e - 1, dp);

        dp[s][e] = Math.max(left, right);

        return dp[s][e];
    }

}
