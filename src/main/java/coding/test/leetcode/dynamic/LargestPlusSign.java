package coding.test.leetcode.dynamic;

import java.util.Arrays;

public class LargestPlusSign {

    public int orderOfLargestPlusSign(int N, int[][] mines) {
        if (mines == null || mines.length == 0) return N / 2;

        int[][] matrix = new int[N][N];

        for (int i = 0; i < N; i++) Arrays.fill(matrix[i], 1);

        for (int[] mine : mines) {
            matrix[mine[0]][mine[1]] = 0;
        }

        int[][] up = new int[N][N];
        int[][] left = new int[N][N];

        int result = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                up[i][j] = matrix[i][j] == 0 ? 0 : i == 0 ? 1 : up[i - 1][j] + 1;
                left[i][j] = matrix[i][j] == 0 ? 0 : j == 0 ? 1 : left[i][j - 1] + 1;
            }
        }

        for (int i = N - 1; i >= 0; i--) {
            for (int j = N - 1; j >= 0; j--) {
                up[i][j] = matrix[i][j] == 0 ? 0 : i == N - 1 ? 1 : Math.min(up[i + 1][j] + 1, up[i][j]);
                left[i][j] = matrix[i][j] == 0 ? 0 : j == N - 1 ? 1 : Math.min(left[i][j + 1] + 1, left[i][j]);
                result = Math.max(result, Math.min(up[i][j], left[i][j]));
            }
        }
        return result;
    }
}
