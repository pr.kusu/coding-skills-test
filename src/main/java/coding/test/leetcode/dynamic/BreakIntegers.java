package coding.test.leetcode.dynamic;

public class BreakIntegers {

    public int integerBreak2(int n) {
        int[] dp = new int[n + 1];
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j < i; j++) {
                dp[i] = Math.max(dp[i], Math.max(j * dp[i - j], j * (i - j)));
            }
        }
        return dp[n];
    }

    public int integerBreak(int n) {
        int[] dp = new int[n + 1];

        return helper(n, dp);
    }

    private int helper(int n, int[] dp) {
        if (n == 2) {
            dp[2] = 1;
            return 1;
        }

        if (dp[n] != 0) {
            return dp[n];
        }

        for (int i = 2; i < n; i++) {
            dp[n] = Math.max(dp[n], i * (n - i));
            dp[n] = Math.max(dp[n], i * helper(n - i, dp));
        }

        return dp[n];
    }


    public static void main(String[] args) {

    }
}
