package coding.test.leetcode.dynamic;

import java.util.List;

public class TriangeSum {

    public int minimumTotal2(List<List<Integer>> triangle) {

        return helper(0, 0, triangle, new Integer[triangle.size()][triangle.size()]);
    }

    private int helper(int start, int depth, List<List<Integer>> triangle, Integer[][] dp) {
        if (depth == triangle.size()) {
            return 0;
        }

        if (dp[depth][start] != null) {
            return dp[depth][start];
        }

        int current = triangle.get(depth).get(start);

        int left = helper(start, depth + 1, triangle, dp);
        int right = helper(start + 1, depth + 1, triangle, dp);

        dp[depth][start] = Math.min(left, right) + current;

        return dp[depth][start];
    }

    public int minimumTotal(List<List<Integer>> triangle) {
        //[
        //     [2],     [11]
        //    [3,4],   [9,9]
        //   [6,5,7], [7,6,10]
        //  [4,1,8,3]
        //]

        int[] dp = new int[triangle.size() + 1];

        for (int i = triangle.size() - 1; i >= 0; i--) {

            for (int j = 0; j <= i; j++) {
                int min = Math.min(dp[j], dp[j + 1]);

                dp[j] = triangle.get(i).get(j) + min;

            }
        }

        return dp[0];
    }


}
