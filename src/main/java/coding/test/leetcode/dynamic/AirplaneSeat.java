package coding.test.leetcode.dynamic;

public class AirplaneSeat {

    public double nthPersonGetsNthSeat(int n) {
        double[] dp = new double[n];
        dp[0] = 1;

        for (int i = 1; i < n; i++) {
            int totalSeats = (i + 1);
            double probFirstPickingFirst = 1.0 / totalSeats;
            double probFirstPickingOther = (totalSeats - 1.0) / totalSeats;

            dp[i] = probFirstPickingFirst + probFirstPickingOther * dp[i - 1];
        }

        return dp[n - 1];
    }
}
