package coding.test.leetcode.dynamic;

public class LongestHappy {

    public String longestDiverseString(int a, int b, int c) {
        int[] counts = new int[]{a, b, c};
        StringBuilder sb = new StringBuilder();
        //init chars for two previous steps
        int lastUsed1 = -1;
        int lastUsed2 = -1;

        int index = 0;
        while (true) {
            int max = 0;
            //searching for char with max qty left and one that haven't used in two last steps consecutively
            // (to avoid 3(a) or b or c
            for (int i = 0; i < 3; i++) {
                if (counts[i] > max && !(i == lastUsed1 && i == lastUsed2)) {
                    max = counts[i];
                    index = i;
                }
            }

            if (max == 0) {
                break;
            }

            sb.append((char) ('a' + index));
            lastUsed2 = lastUsed1;
            lastUsed1 = index;
            counts[index]--;

        }

        return sb.toString();
    }
}
