package coding.test.leetcode.dynamic;

public class UniquePaths {

    public int uniquePaths(int m, int n) {
        int[][] ways = new int[m][n];

        for (int i = 0; i < m; i++) {
            ways[i][0] = 1;
        }
        for (int i = 0; i < n; i++) {
            ways[0][i] = 1;
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                ways[i][j] = ways[i - 1][j] + ways[i][j - 1];
            }
        }

        return ways[m - 1][n - 1];
    }

    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;

        int[][] ways = new int[m][n];
        ways[0][0] = obstacleGrid[0][0] == 0 ? 1 : 0;


        for (int i = 1; i < m; i++) {
            ways[i][0] = obstacleGrid[i][0] == 0 ? ways[i - 1][0] : 0;
        }
        for (int i = 1; i < n; i++) {
            ways[0][i] = obstacleGrid[0][i] == 0 ? ways[0][i - 1] : 0;
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (obstacleGrid[i][j] == 1) {
                    ways[i][j] = 0;
                } else {
                    ways[i][j] = ways[i - 1][j] + ways[i][j - 1];
                }
            }
        }

        return ways[m - 1][n - 1];
    }
}
