package coding.test.leetcode.dynamic;

public class StoneGamesII {

    public int stoneGameII(int[] piles) {
        int[][] dp = new int[piles.length][100];
        int totalSum = 0;
        for (int i = 0; i < piles.length; i++) {
            totalSum += piles[i];
        }
        return findMax(piles, 0, dp, 1, totalSum);
    }

    int findMax(int[] piles, int start, int[][] dp, int M, int totalSum) {
        if (start >= piles.length) return 0;
        if (dp[start][M] > 0) return dp[start][M];

        int localMax = 0;
        int sum = 0;

        for (int j = start; j < Math.min(piles.length, start + 2 * M); j++) {
            // Alex's current taken
            sum += piles[j];
            int newM = Math.max(M, j - start + 1);
            // Lee's Turn
            int rightSum = findMax(piles, j + 1, dp, newM, totalSum - sum);
            localMax = Math.max(localMax, totalSum - rightSum);
        }
        dp[start][M] = localMax;
        return dp[start][M];
    }
}
