package coding.test.leetcode.dynamic;

import java.util.Arrays;

public class PaintFence {

    public int minCost2(int[][] costs) {
        if (costs.length == 0) {
            return 0;
        }

        for (int i = 1; i < costs.length; i++) {
            costs[i][0] = Math.min(costs[i - 1][1], costs[i - 1][2]);
            costs[i][1] = Math.min(costs[i - 1][0], costs[i - 1][2]);
            costs[i][2] = Math.min(costs[i - 1][0], costs[i - 1][1]);
        }

        return Arrays.stream(costs[costs.length - 1]).min().getAsInt();
    }

    public int minCost1(int[][] costs) {
        if (costs == null || costs.length == 0) return 0;
        // Assume all costs are positive
        int n = costs.length; // number of houses
        int[][] dp = new int[n][3];
        // Init
        dp[0][0] = costs[0][0];
        dp[0][1] = costs[0][1];
        dp[0][2] = costs[0][2];
        // DP
        for (int i = 1; i < n; ++i) {
            dp[i][0] = Math.min(dp[i - 1][1], dp[i - 1][2]) + costs[i][0];
            dp[i][1] = Math.min(dp[i - 1][0], dp[i - 1][2]) + costs[i][1];
            dp[i][2] = Math.min(dp[i - 1][0], dp[i - 1][1]) + costs[i][2];
        }
        return Math.min(dp[n - 1][0], Math.min(dp[n - 1][1], dp[n - 1][2]));
    }

    public int minCostM(int[][] costs) {
        if (costs.length == 0) {
            return 0;
        }

        int[][] dp=new int[costs.length][3];

        for(int i=0; i<costs.length;i++){
            dp[0][i]=costs[0][i];
        }

        for (int i = 1; i < costs.length; i++) {
            dp[i][0] = costs[i][0]+Math.min(dp[i - 1][1], dp[i - 1][2]);
            dp[i][1] = costs[i][1]+Math.min(dp[i - 1][0], dp[i - 1][2]);
            dp[i][2] =costs[i][2]+ Math.min(dp[i - 1][0], dp[i - 1][1]);
        }

        return Arrays.stream(dp[dp.length - 1]).min().getAsInt();
    }

    public int minCost(int[][] costs) {
        if (costs.length == 0) {
            return 0;
        }

        int[][] dp = new int[costs.length][3];

        int total = Integer.MAX_VALUE;
        for (int i = 0; i < 3; i++) {
            int cur = paint(0, costs, dp, i);
            total = Math.min(total, cur);
        }

        return total;
    }

    private int paint(int house, int[][] costs, int[][] dp, int color) {
        if (house >= costs.length) {
            return dp[house - 1][color];
        }

        if (dp[house][color] > 0) {
            return dp[house][color];
        }

        int totalCost = costs[house][color];

        if (house != costs.length - 1) {
            if (color == 0) {
                totalCost += Math.min(paint(house + 1, costs, dp, 1),
                        paint(house + 1, costs, dp, 2));
            } else if (color == 1) {
                totalCost += Math.min(paint(house + 1, costs, dp, 0),
                        paint(house + 1, costs, dp, 2));
            } else if (color == 2) {
                totalCost += Math.min(paint(house + 1, costs, dp, 0),
                        paint(house + 1, costs, dp, 1));
            }
        }

        dp[house][color] = totalCost;

        return totalCost;
    }
}
