package coding.test.leetcode.dynamic;

import coding.test.leetcode.curated.TreeNode;

import java.util.LinkedList;
import java.util.List;

public class GenerateSubTrees {

    public List<TreeNode> generateTrees(int n) {
        return helper(1, n);
    }

    private List<TreeNode> helper(int start, int end) {
        List<TreeNode> result = new LinkedList<>();
        if (start < end) {
            return result;
        }

        for (int i = start; i <= end; i++) {
            List<TreeNode> leftSubTree = helper(start, i - 1);
            List<TreeNode> rightSubTree = helper(i + 1, end);

            for (TreeNode left : leftSubTree) {
                for (TreeNode right : rightSubTree) {
                    TreeNode root = new TreeNode(i);
                    root.left = left;
                    root.right = right;
                    result.add(root);
                }
            }
        }

        return result;
    }
}
