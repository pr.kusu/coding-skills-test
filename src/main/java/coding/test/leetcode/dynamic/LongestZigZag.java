package coding.test.leetcode.dynamic;

import coding.test.leetcode.curated.TreeNode;

public class LongestZigZag {

    int gl;

    public int longestZigZag(TreeNode root) {
        if (root == null || root.left == null && root.right == null) return 0;
        helper(root.left, 0, 0);
        helper(root.right, 1, 0);
        return gl + 1;
    }

    void helper(TreeNode root, int d, int distTillNow) {
        if (root == null) return;
        gl = Math.max(distTillNow, gl);

        helper(root.left, 0, d == 0 ? 0 : distTillNow + 1);
        helper(root.right, 1, d == 1 ? 0 : distTillNow + 1);
    }

}
