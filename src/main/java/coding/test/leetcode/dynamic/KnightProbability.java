package coding.test.leetcode.dynamic;

public class KnightProbability {
    final int[][] dir = new int[][]{{2, 1}, {2, -1}, {-2, 1}, {-2, -1}, {1, -2}, {1, 2}, {-1, 2}, {-1, -2}};

    public double knightProbability(int N, int K, int r, int c) {

        return helper(N, K, r, c, new double[K + 1][N][N]);
    }

    public double helper(int N, int move, int row, int col, double[][][] dp) {
        if (row < 0 || col < 0 || row >= N || col >= N) {
            return 0;
        }

        if (move == 0) {
            return 1;
        }
        if (dp[move][row][col] != 0) {
            return dp[move][row][col];
        }

        double moves = 0;

        for (int[] d : dir) {
            int newX = d[0] + row;
            int newY = d[1] + col;

            moves += helper(N, move - 1, newX, newY, dp);
        }

        dp[move][row][col] = moves / 8.0;

        return moves / 8.0;
    }

    public static void main(String[] args) {
        new KnightProbability().knightProbability(3, 2, 0, 0);
    }
}
