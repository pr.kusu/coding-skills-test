package coding.test.leetcode.dynamic;

public class PaintHouse {

    public int numWays(int n, int k) {
        if (n == 0 || k == 0) {
            return 0;
        }
        if (n == 1) {
            return k;
        }

        int[] ways = new int[n];
        ways[0] = k;
        ways[1] = k * (k - 1) + k;


        for (int i = 2; i < n; i++) {
            ways[i] = ways[i - 1] * (k - 1) + ways[i - 2] * (k - 1);
        }

        return ways[n - 1];
    }
}
