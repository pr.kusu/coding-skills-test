package coding.test.leetcode.dynamic;

import java.util.HashMap;

public class LongestAirthimetic {

    public int longestArithSeqLength2(int[] a) {
        if (a == null) return 0;
        int[][] dp = new int[a.length][20001];

        int res = 1;
        for (int i = 1; i < a.length; i++) {
            for (int j = 0; j <i; j++) {
                int diff = a[i] - a[j] + 10000;
                dp[i][diff] = Math.max(dp[i][diff], dp[j][diff] + 1);   // DON'T forget to compare

                res = Math.max(res, dp[i][diff]);
            }
        }

        return res + 1;
    }

    public int longestArithSeqLength(int[] A) {
        int res = 2;
        HashMap<Integer, Integer>[] dp = new HashMap[A.length];

        for (int i = 0; i < A.length; i++) {
            dp[i] = new HashMap<>();

            for (int j = 0; j < i; j++) {
                int d = A[i] - A[j];
                dp[i].put(d, dp[j].getOrDefault(d, 1) + 1);
                res = Math.max(res, dp[i].get(d));
            }
        }

        return res;
    }


}
