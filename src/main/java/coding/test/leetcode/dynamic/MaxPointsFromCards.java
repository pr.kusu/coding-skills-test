package coding.test.leetcode.dynamic;

public class MaxPointsFromCards {

    public int maxScore(int[] cardPoints, int k) {
        if (cardPoints == null || cardPoints.length == 0) return 0;

        // leftSum[n]: n elements sum from left
        // rightSum[n]: n elements sum from right
        int[] leftSum = new int[k + 1];
        int[] rightSum = new int[k + 1];

        leftSum[0] = 0;
        rightSum[0] = 0;

        for (int i = 0; i < k; i++) {
            leftSum[i + 1] = leftSum[i] + cardPoints[i];
            rightSum[i + 1] = rightSum[i] + cardPoints[cardPoints.length - 1 - i];
        }

        int result = 0;
        for (int i = 0; i <= k; i++) {
            result = Math.max(result, leftSum[i] + rightSum[k - i]);
        }
        return result;
    }
}
