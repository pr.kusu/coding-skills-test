package coding.test.leetcode.dynamic;

public class DiceRollTarget {

    int MOD = 1000000000 + 7;

    public int numRollsToTarget2(int d, int f, int target) {
        int[][] dp = new int[d + 1][target + 1];
        dp[0][0] = 1;

        for (int i = 1; i <= d; i++) {
            for (int j = 1; j <= target; j++) {
                for (int k = 1; k <= f; k++) {
                    if (j >= k) {
                        dp[i][j] = (dp[i][j] + dp[i - 1][j - k]) % MOD;
                    } else {
                        break;
                    }
                }
            }
        }

        return dp[d][target];
    }

    public int numRollsToTarget(int d, int f, int target) {
        Integer[][] dp = new Integer[d + 1][target + 1];

        return helper(d, f, target, dp);
    }

    private int helper(int d, int f, int target, Integer[][] dp) {
        if (d == 0 && target == 0) {
            return 1;
        }

        if (d == 0 || target <= 0) {
            return 0;
        }

        if (dp[d][target] != null) {
            return dp[d][target];
        }

        int ways = 0;

        for (int i = 1; i <= f; i++) {
            if (target >= i) {
                ways = (ways + helper(d - 1, f, target - i, dp)) % MOD;
            } else {
                break;
            }
        }

        dp[d][target] = ways;

        return dp[d][target];
    }

}
