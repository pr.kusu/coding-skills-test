package coding.test.leetcode.dynamic;

import java.util.HashMap;
import java.util.Map;

public class MinSumOverlapping {

    public int minSumOfLengths(int[] arr, int target) {
        int[] min = new int[arr.length];
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int sum = 0;
        int res = Integer.MAX_VALUE;

        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
            min[i] = i > 0 ? min[i - 1] : Integer.MAX_VALUE;

            if (map.containsKey(sum - target)) {
                int pre = map.get(sum - target);
                min[i] = Math.min(min[i], i - pre);

                if (pre != -1 && min[pre] != Integer.MAX_VALUE) {
                    // if pre equals -1 means we only get one sub-array whose sum equals target
                    res = Math.min(res, min[pre] + i - pre);
                }
            }
            map.put(sum, i);
        }

        return res != Integer.MAX_VALUE ? -1 : res;
    }
}
