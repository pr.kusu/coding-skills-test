package coding.test.leetcode.dynamic;

public class TargetSum {

    public int findTargetSumWays2(int[] nums, int S) {
        int[][] dp = new int[nums.length][2001];
        dp[0][nums[0] + 1000] = 1;
        dp[0][-nums[0] + 1000] = 1;
        for (int i = 1; i < nums.length; i++) {
            for (int sum = -1000; sum <= 1000; sum++) {
                if (dp[i - 1][sum + 1000] > 0) {
                    dp[i][sum + nums[i] + 1000] += dp[i - 1][sum + 1000];
                    dp[i][sum - nums[i] + 1000] += dp[i - 1][sum + 1000];
                }
            }
        }
        return S > 1000 ? 0 : dp[nums.length - 1][S + 1000];
    }

    public int findTargetSumWays(int[] nums, int S) {
        Integer[][] memo = new Integer[nums.length][2001];

        return helper(nums, S, 0, 0, memo);
    }

    private int helper(int[] nums, int target, int sum, int i, Integer[][] memo) {
        if (sum == target && i == nums.length) {
            return 1;
        }

        if (i == nums.length) {
            return 0;
        }

        if (memo[i][sum + 1000] != null) {
            return memo[i][sum + 1000];
        }

        int ways1 = helper(nums, target, sum + nums[i], i + 1, memo);
        int ways2 = helper(nums, target, sum - nums[i], i + 1, memo);

        memo[i][sum + 1000] = ways1 + ways2;

        return memo[i][sum + 1000];
    }
}
