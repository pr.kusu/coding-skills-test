package coding.test.leetcode.dynamic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingOffer {

    public int shoppingOffers(List<Integer> price, List<List<Integer>> special, List<Integer> needs) {
        Map<List<Integer>, Integer> memo = new HashMap<>();
        return helper(price, special, needs, 0, memo);
    }

    private int helper(List<Integer> price, List<List<Integer>> special, List<Integer> needs, int i,
                       Map<List<Integer>, Integer> memo) {
        int localMin = fullPrice(price, needs);

        if (memo.containsKey(needs)) {
            return memo.get(needs);
        }

        for (int j = i; j < special.size(); j++) {

            List<Integer> offer = special.get(j);
            List<Integer> temp = new ArrayList<>();

            for (int k = 0; k < needs.size(); k++) {
                if (offer.get(k) > needs.get(k)) {
                    temp = null;
                    break;
                }

                temp.add(needs.get(k) - offer.get(k));
            }

            if (temp != null) {
                int offerPrice = offer.get(offer.size() - 1);
                localMin = Math.min(localMin, offerPrice + helper(price, special, temp, j, memo));
            }
        }

        memo.put(needs, localMin);

        return localMin;
    }

    private int fullPrice(List<Integer> price, List<Integer> needs) {
        int total = 0;

        for (int i = 0; i < needs.size(); i++) {
            total += price.get(i) * needs.get(i);
        }

        return total;
    }
}
