package coding.test.leetcode.dynamic;

public class PartitionEqualSum {

    public boolean canPartition(int[] nums) {

        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        if (sum % 2 == 1) return false;
        Boolean[][] dp = new Boolean[nums.length][sum / 2 + 1];

        return helper(nums, 0, dp, sum / 2);
    }

    private boolean helper(int[] nums, int i, Boolean[][] dp, int target) {
        if (i == nums.length) {
            return target == 0;
        }
        if (target < 0) {
            return false;
        }

        if (dp[i][target] != null) {
            return dp[i][target];
        }

        boolean canSum = helper(nums, i + 1, dp, target - nums[i]) ||
                helper(nums, i + 1, dp, target);

        dp[i][target] = canSum;

        return canSum;
    }

}
