package coding.test.leetcode.dynamic;

public class MinSwapRequired {
    //https://leetcode.com/problems/minimum-swaps-to-make-sequences-increasing

    public int minSwap(int[] A, int[] B) {
        Integer[][] dp = new Integer[A.length][2];

        return helper(A, B, 0, -1, -1, 0, dp);
    }

    private int helper(int[] A, int[] B, int i, int prevA, int prevB, int swapped, Integer[][] dp) {
        if (i == A.length) {
            return 0;
        }

        if (dp[i][swapped] != null) {
            return dp[i][swapped];
        }

        int min = Integer.MAX_VALUE;

        if (A[i] > prevA && B[i] > prevB) {
            min = Math.min(min, helper(A, B, i + 1, A[i], B[i], 0, dp));
        }

        if (B[i] > prevA && A[i] > prevB) {
            min = Math.min(min, helper(A, B, i + 1, B[i], A[i], 1, dp) + 1);
        }

        dp[i][swapped] = min;

        return min;
    }
}
