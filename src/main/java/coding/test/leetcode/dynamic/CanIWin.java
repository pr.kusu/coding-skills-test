package coding.test.leetcode.dynamic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CanIWin {

    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        if (desiredTotal <= 0) {
            return true;
        }
        int sum = (1 + maxChoosableInteger) * maxChoosableInteger / 2;
        if (sum < desiredTotal) return false;

        Map<String, Boolean> memo = new HashMap<>();
        char[] used = new char[maxChoosableInteger + 1];
        Arrays.fill(used, '0');
        return helper(maxChoosableInteger, used, memo, desiredTotal);
    }

    private boolean helper(int max, char[] used, Map<String, Boolean> memo, int desiredTotal) {

        String key = new String(used);
        if (desiredTotal <= 0) {
            return false;
        }

        if (memo.containsKey(key)) {
            return memo.get(key);
        }

        for (int i = 1; i < used.length; i++) {
            if (used[i] == '0') {
                used[i] = '1';
                if (!helper(max, used, memo, desiredTotal - i)) {
                    memo.put(key, true);
                    used[i] = '0';
                    return true;
                }
                used[i] = '0';
            }
        }

        memo.put(key, false);

        return memo.get(key);
    }
}
