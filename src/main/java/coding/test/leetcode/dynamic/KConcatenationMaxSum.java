package coding.test.leetcode.dynamic;

public class KConcatenationMaxSum {

    private static int M = (int) Math.pow(10, 9) + 7;

    public int kConcatenationMaxSum2(int[] arr, int k) {
        if (arr == null || arr.length == 0) return 0;
        long maxOfEnd = arr[0] > 0 ? arr[0] : 0L, maxSoFar = maxOfEnd, sum = arr[0];
        for (int i = 1; i < Math.min(k, 2) * arr.length; i++) {
            maxOfEnd = Math.max(maxOfEnd + arr[i % arr.length], arr[i % arr.length]);
            maxSoFar = Math.max(maxOfEnd, maxSoFar);
            if (i < arr.length) sum += arr[i];
        }
        while (sum > 0 && --k >= 2)
            maxSoFar = (maxSoFar + sum) % 1000000007;
        return (int) maxSoFar;
    }

    public static int kConcatenationMaxSum(int[] arr, int k) {
        int[] result = new int[arr.length * k];
        int i = 0;
        while (i < arr.length * k) {
            result[i] = arr[i % arr.length];
            i++;
        }
        int[] dp = new int[result.length];
        dp[0] = result[0];
        int max = Math.max(0, dp[0]);


        for (int j = 1; j < result.length; j++) {
            dp[j] = Math.max(result[j], result[j] + dp[j - 1]);
            max = Math.max(max, dp[j]);
        }

        return max;
    }

    public static void main(String[] args) {
        kConcatenationMaxSum(new int[]{1, 2}, 3);
    }

}
