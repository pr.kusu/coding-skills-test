package coding.test.leetcode.dynamic;

public class TargetSum4 {

    public int combinationSum4(int[] nums, int target) {
        Integer[] dp = new Integer[target + 1];

        return dfs(target, nums, dp, 0);
    }

    private int dfs(int rem, int[] nums, Integer[] memo, int index) {
        if (rem == 0) {
            return 1;
        }
        if (rem < 0) {
            return 0;
        }

        if (memo[rem] != null) {
            return memo[rem];
        }

        int ans = 0;

        for (int i = 0; i < nums.length; i++) {
            ans += dfs(rem - nums[i], nums, memo, index + 1);
        }

        memo[rem] = ans;

        return ans;
    }
}
