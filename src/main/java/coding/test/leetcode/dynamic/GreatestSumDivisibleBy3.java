package coding.test.leetcode.dynamic;

import java.util.Arrays;

public class GreatestSumDivisibleBy3 {

    public int maxSumDivThree(int[] nums) {
        return maxSumDivK(nums, 3);
    }

    /**
     *  Transition:
     *         dp_cur[(rem + num) % 3]
     *             = max(dp_prev[(rem + num) % 3], dp_prev[rem]+num)
     *             where "rem" stands for remainder for shorter naming
     *         meaning:
     *             "Current max sum with remainder 0 or 1 or 2" could be from
     *             EITHER prevSum with remainder 0 or 1 or 2 consecutively
     *             OR     prevSum with some remainder "rem" + current number "num"
     * @param nums
     * @param k
     * @return
     */
    public int maxSumDivK(int[] nums, int k) {
        if (k == 0) return -1;

        int[] dp = new int[k];

        for (int num : nums) {
            int[] tmp = Arrays.copyOf(dp, k);

            for (int i = 0; i < tmp.length; i++) {
                int next = (num + tmp[i]) % k;
                dp[next] = Math.max(dp[next], num + tmp[i]);
            }
        }

        return dp[0];
    }
}
