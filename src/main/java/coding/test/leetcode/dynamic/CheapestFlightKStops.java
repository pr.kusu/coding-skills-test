package coding.test.leetcode.dynamic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheapestFlightKStops {

    private int min = Integer.MAX_VALUE;

    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
        Map<Integer, List<int[]>> graph = new HashMap<>();

        for (int[] flight : flights) {
            graph.computeIfAbsent(flight[0], k -> new ArrayList<>()
            ).add(new int[]{flight[1], flight[2]});
        }

        dfs(n, K + 1, graph, src, dst, 0);
        return min == Integer.MAX_VALUE ? -1 : min;
    }

    private void dfs(int n, int K, Map<Integer, List<int[]>> graph, int src, int dest, int cost) {
        if (K < 0) {
            return;
        }

        if (src == dest) {
            min = cost;
            return;
        }

        if (!graph.containsKey(src)) {
            return;
        }

        List<int[]> nextStops = graph.get(src);

        for (int[] stop : nextStops) {

            if (stop[1] + cost > min) {
                continue;
            }

            dfs(n, K - 1, graph, stop[0], dest, cost + stop[1]);

        }
    }
}
