package coding.test.leetcode.dynamic;

public class LongestPalidrome {

    public String longestPalindrome(String s) {
        boolean[][] dp = new boolean[s.length()][s.length()];
        int len = s.length();
        String result = "";

        for (int i = len - 1; i >= 0; i--) {
            // aabaa
            for (int j = i; j < len; j++) {
                if (s.charAt(i) == s.charAt(j)) {
                    int currentLen = j - i;
                    dp[i][j] = currentLen < 3 || dp[i + 1][j - 1];

                    if (dp[i][j] && currentLen + 1 > result.length()) {
                        result = s.substring(i, j + 1);
                    }
                }
            }
        }

        return result;
    }
}
