package coding.test.leetcode.dynamic;

public class SubStringInWrapedString {

    public int findSubstringInWraproundString(String p) {
        int[] dp = new int[26];
        int sum = 0;
        int len = 0;

        for (int i = 0; i < p.length(); i++) {

            if (i > 0 && (p.charAt(i) - p.charAt(i - 1) == 1 ||
                    p.charAt(i - 1) - p.charAt(i) == 25)) {
                len++;
            } else {
                len = 1;
            }

            int index = p.charAt(i) - 'a';
            dp[index] = Math.max(dp[index], len);
        }

        for (int j : dp) {
            sum += j;
        }

        return sum;
    }
}
