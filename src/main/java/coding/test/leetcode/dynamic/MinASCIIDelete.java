package coding.test.leetcode.dynamic;

public class MinASCIIDelete {

    public int minimumDeleteSum2(String s1, String s2) {
        // delete leet
        int[][] dp = new int[s1.length() + 1][s2.length() + 1];

        for (int i = 1; i <= s1.length(); i++) {
            dp[i][0] = s1.codePointAt(i - 1) + dp[i - 1][0];
        }
        for (int j = 1; j <= s2.length(); j++) {
            dp[0][j] = s2.codePointAt(j - 1) + dp[0][j - 1];
        }

        for (int i = 1; i <= s1.length(); i++) {
            for (int j = 1; j <= s2.length(); j++) {
                if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] = Math.min(dp[i - 1][j] + s1.codePointAt(i - 1), dp[i][j - 1] + s2.codePointAt(j - 1));
                }
            }
        }

        return dp[s1.length()][s2.length()];
    }

    public int minimumDeleteSum(String s1, String s2) {

        return helper(0, 0, s1, s2, new Integer[s1.length()][s2.length()]);
    }

    private int helper(int p1, int p2, String s1, String s2, Integer[][] dp) {
        if (p1 == s1.length() && p2 == s2.length()) {
            return 0;
        }

        if (p1 == s1.length()) {
            // s1 becomes empty, cost is to remove all s2 characters
            int cost = 0;
            for (int i = p2; i < s2.length(); i++) {
                cost += s2.codePointAt(i);
            }
            return cost;
        }

        if (p2 == s2.length()) {
            // s2 becomes empty, cost is to remove all s1 characters
            int cost = 0;
            for (int i = p1; i < s1.length(); i++) {
                cost += s1.codePointAt(i);
            }
            return cost;
        }
        if (dp[p1][p2] != null) {
            return dp[p1][p2];
        }
        int cost;
        if (s1.charAt(p1) == s2.charAt(p2)) {
            cost = helper(p1 + 1, p2 + 1, s1, s2, dp);
        } else {
            int cost1 = s1.codePointAt(p1) + helper(p1 + 1, p2, s1, s2, dp);
            int cost2 = s2.codePointAt(p2) + helper(p1, p2 + 1, s1, s2, dp);
            cost = Math.min(cost1, cost2);
        }
        dp[p1][p2] = cost;
        return dp[p1][p2];
    }
}
