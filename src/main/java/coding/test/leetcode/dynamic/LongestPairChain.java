package coding.test.leetcode.dynamic;

import java.util.Arrays;
import java.util.Comparator;

public class LongestPairChain {
    //[[1,2], [2,3], [3,4], [5,6]

    public int findLongestChain(int[][] pairs) {
        int[] dp = new int[pairs.length];
        int max = 0;
        Arrays.sort(pairs, Comparator.comparingInt(a -> a[0]));
        Arrays.fill(dp, 1);
        for (int i = 1; i < pairs.length; i++) {
            for (int j = 0; j < i; j++) {
                int b = pairs[j][1];
                int c = pairs[i][0];
                if (b < c) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }

            max = Math.max(max, dp[i]);
        }

        return max;
    }
}
