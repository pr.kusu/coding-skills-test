package coding.test.leetcode.dynamic;

public class KnightDialer {
    private final int M = (int) Math.pow(10, 9) + 1;
    final int[][] dir = new int[][]{{2, 1}, {2, -1}, {-2, 1}, {-2, -1}, {1, -2}, {1, 2}, {-1, 2}, {-1, -2}};

    public int knightDialer(int N) {
        Integer[][][] memo = new Integer[4][4][N + 1];
        int ways = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                ways = (ways + helper(N - 1, memo, i, j)) % M;
            }
        }
        ways = (ways + helper(N - 1, memo, 3, 1)) % M;

        return ways;
    }

    private int helper(int N, Integer[][][] memo, int x, int y) {
        if (x < 0 || y < 0 || x >= 3 || y >= 3) {

            if (!(x == 3 && y == 1)) {
                return 0;
            }
        }

        if (N == 0) {
            return 1;
        }

        if (memo[x][y][N] != null) {
            return memo[x][y][N];
        }

        int ways = 0;
        for (int[] d : dir) {
            int newX = d[0] + x;
            int newY = d[1] + y;

            ways = (ways + helper(N - 1, memo, newX, newY)) % M;
        }

        memo[x][y][N] = ways;
        return ways;
    }
}
