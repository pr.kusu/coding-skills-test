package coding.test.leetcode.dynamic;

import java.util.*;

public class LongestStringChain {

    public int longestStrChain(String[] words) {

        Arrays.sort(words, Comparator.comparingInt(String::length));
        Map<String, Integer> hash = new HashMap<>();
        for (String word : words) {
            hash.put(word, -1);
        }
        int max = 0;
        for (int i = 0; i < words.length; i++) {
            if (hash.get(words[i]) == -1) {
                int val = helper(words[i], hash);
                max = Math.max(max, val);
            }
        }
        return max;
    }

    private int helper(String current, Map<String, Integer> hash) {
        if (hash.get(current) != -1) {
            return hash.get(current);
        }

        int max = 1;
        for (int i = 0; i < current.length(); i++) {
            //bcd
            StringBuilder builder = new StringBuilder();
            if (i > 0) {
                builder.append(current, 0, i);
            }
            builder.append(current.substring(i + 1));

            if (hash.containsKey(builder.toString())) {
                int res = 1 + helper(builder.toString(), hash);
                max = Math.max(max, res);
            }

        }

        hash.put(current, max);
        return hash.get(current);
    }

    public static void main(String[] args) {
        new LongestStringChain().longestStrChain(new String[]{"a", "b", "ba", "bca", "bda", "bdca"});
    }
}
