package coding.test.leetcode.dynamic;

public class DiceRollSimulation {

    final int M = 1000000007;

    public int dieSimulator(int n, int[] rollMax) {
        int[][][] dp = new int[5000][6][rollMax.length + 1];

        return dfs(n, rollMax, -1, 0, dp);
    }

    private int dfs(int n, int[] rollMax, int last, int len, int[][][] dp) {
        if (n == 0) {
            return 1;
        }

        if (last >= 0 && dp[n][last][len] > 0) {
            return dp[n][last][len];
        }

        int ans = 0;

        for (int i = 0; i < 6; i++) {
            if (i == last && len >= rollMax[i]) {
                continue;
            }

            ans = (ans + dfs(n - 1, rollMax, i, i == last ? len + 1 : 1, dp)) % M;
        }
        if (last >= 0) {
            dp[n][last][len] = ans;
        }

        return ans;
    }

}
