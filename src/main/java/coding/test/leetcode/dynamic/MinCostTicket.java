package coding.test.leetcode.dynamic;

public class MinCostTicket {

    public int mincostTickets(int[] days, int[] costs) {
        int size = days[days.length - 1] + 1;

        int[] dp = new int[size];
        boolean[] travel = new boolean[size];
        for (int day : days) {
            travel[day] = true;
        }

        for (int i = 1; i < size; i++) {
            if (travel[i]) {
                int cost1 = dp[i - 1] + costs[0];
                int cost2 = dp[Math.max(0, i - 7)] + costs[1];
                int cost3 = dp[Math.max(0, i - 30)] + costs[2];
                dp[i] = Math.min(cost1, Math.min(cost2, cost3));
            } else {
                dp[i] = dp[i - 1];
            }
        }

        return dp[dp.length - 1];
    }
}
