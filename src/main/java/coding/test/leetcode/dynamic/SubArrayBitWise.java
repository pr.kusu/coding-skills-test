package coding.test.leetcode.dynamic;

import java.util.HashSet;
import java.util.Set;

public class SubArrayBitWise {

    public int subarrayBitwiseORs(int[] A) {
        Set<Integer> res = new HashSet<>();
        Set<Integer> pre = new HashSet<>();

        for (int num : A) {

            Set<Integer> cur = new HashSet<>();
            cur.add(num);
            for (int j : pre) {
                cur.add(j | num);
            }
            pre = cur;
            res.addAll(pre);
        }

        return res.size();
    }
}
