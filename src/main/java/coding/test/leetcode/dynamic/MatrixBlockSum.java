package coding.test.leetcode.dynamic;

public class MatrixBlockSum {

    public int[][] matrixBlockSum(int[][] mat, int K) {
        int m = mat.length;
        int n = mat[0].length;

        int[][] dp = new int[m + 1][n + 1];

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                dp[i][j] = mat[i - 1][j - 1] + dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1];
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int r1 = Math.max(0, i - K);
                int r2 = Math.min(i + K, m - 1);
                int c1 = Math.max(0, j - K);
                int c2 = Math.min(j + K, n - 1);

                mat[i][j] = dp[r2 + 1][c2 + 1] - dp[r1][c2 + 1] - dp[r2 + 1][c1] + dp[r1][c1];
            }
        }

        return mat;
    }

    private static void main(String[] args) {
        int[][] marix = new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};
        new MatrixBlockSum().matrixBlockSum(marix, 1);
    }
}
