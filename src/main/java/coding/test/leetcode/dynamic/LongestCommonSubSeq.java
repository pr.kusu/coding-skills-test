package coding.test.leetcode.dynamic;

public class LongestCommonSubSeq {

    public int longestCommonSubsequence2(String text1, String text2) {
        Integer[][] dp = new Integer[text1.length()][text2.length()];

        return helper(0, 0, dp, text1, text2);
    }

    private int helper(int p1, int p2, Integer[][] dp, String text1, String text2) {
        if (p1 > text1.length() || p2 > text2.length()) {
            return 0;
        }
        if (dp[p1][p2] != null) {
            return dp[p1][p2];
        }

        int len = 0;
        if (text1.charAt(p1) == text2.charAt(p2)) {
            len = 1 + helper(p1 + 1, p2 + 1, dp, text1, text2);
        } else {
            int left = helper(p1 + 1, p2, dp, text1, text2);
            int right = helper(p1, p2 + 1, dp, text1, text2);
            len = Math.max(left, right);
        }

        dp[p1][p2] = len;
        return len;
    }

    public int longestCommonSubsequence(String text1, String text2) {
        int[][] dp = new int[text1.length() + 1][text2.length() + 1];

        for (int i = 1; i <= text1.length(); i++) {
            for (int j = 1; j <= text2.length(); j++) {

                if (text1.charAt(i) == text2.charAt(j)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }

        return dp[text1.length()][text2.length()];
    }
}
