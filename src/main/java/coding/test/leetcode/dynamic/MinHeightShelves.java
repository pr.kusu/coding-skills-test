package coding.test.leetcode.dynamic;

public class MinHeightShelves {

    int[][] mem; // mem[i][j] is the minimum shelf height for adding book i while current level has remaining width j

    public int minHeightShelves(int[][] books, int shelf_width) {
        mem = new int[books.length][shelf_width + 1];

        return helper(books, shelf_width, 0, 0, shelf_width);
    }

    private int helper(int[][] books, int shelf_width, int index,
                       int currentLevelHeight, int currentLevelRemainingWidth) {
        if (index >= books.length) {
            return currentLevelHeight;
        }

        if (mem[index][currentLevelRemainingWidth] > 0) {
            return mem[index][currentLevelRemainingWidth];
        }

        int startNewShelv = Integer.MAX_VALUE;

        if (currentLevelRemainingWidth < shelf_width) {
            startNewShelv = currentLevelHeight + helper(books, shelf_width,
                    index + 1, books[index][1],
                    shelf_width - books[index][0]);
        }

        int useCurrent = Integer.MAX_VALUE;
        if (books[index][0] <= currentLevelRemainingWidth) {
            useCurrent = helper(books, shelf_width, index + 1, Math.max(currentLevelHeight, books[index][1]),
                    currentLevelRemainingWidth - books[index][0]);
        }

        int min = Math.min(startNewShelv, useCurrent);
        mem[index][currentLevelRemainingWidth] = min;

        return min;
    }
}
