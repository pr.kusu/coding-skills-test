package coding.test.leetcode.dynamic;

import java.util.Arrays;

public class DivisorGame {

    public boolean divisorGame(int N) {
        int[] dp = new int[N + 1];
        Arrays.fill(dp, -1);

        return canWin(N, dp);
    }

    boolean canWin(int N, int[] dp) {
        if (N <= 1) {
            return false;
        }

        if (dp[N] != -1) {
            return dp[N] == 1;
        }

        for (int i = 1; i <= N / 2; i++) {
            if (N % i == 0 && !canWin(N - i, dp)) {
                dp[N] = 1;
                return true;
            }
        }
        dp[N] = 0;
        return false;
    }

    public static void main(String[] args) {
        new DivisorGame().divisorGame(8);
    }
}
