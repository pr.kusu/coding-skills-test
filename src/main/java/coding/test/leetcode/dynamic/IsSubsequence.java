package coding.test.leetcode.dynamic;

public class IsSubsequence {

    public boolean isSubsequenceDp(String s, String t) {

        if (s.length() > t.length()) {
            return false;
        }
        boolean[][] dp = new boolean[s.length() + 1][t.length() + 1];

        for (int i = 0; i <= t.length(); i++) {
            //Empty string is subsequence of any string
            dp[0][i] = true;
        }

        for (int j = 1; j <= s.length(); j++) {
            //Any string can't be subsequence of empty string
            dp[j][0] = false;
        }

        for (int i = 1; i <= s.length(); i++) {
            for (int j = 1; j <= t.length(); j++) {
                if (s.charAt(i - 1) == t.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] = dp[i][j - 1];
                }
            }
        }

        return dp[s.length()][t.length()];
    }

    public boolean isSubsequence(String s, String t) {
        if (s.length() > t.length()) {
            return false;
        }
        if (s.equals(t)) {
            return true;
        }
        if (s.isEmpty()) {
            return true;
        }

        char first = s.charAt(0);
        int lastIndex = t.indexOf(first);
        if (lastIndex < 0) {
            return false;
        }

        return isSubsequence(s.substring(1), t.substring(lastIndex + 1));
    }
}
