package coding.test.leetcode.dynamic;

import coding.test.leetcode.curated.TreeNode;

public class MaxProductSplittedBinary {

    private long max = 0;
    private long total;

    public int maxProduct(TreeNode root) {

        total = sum(root);
        sum(root);

        return (int) (max % (int) (1e9 + 7));
    }

    private long sum(TreeNode node) {
        if (node == null) {
            return 0;
        }

        long s = node.val + sum(node.left) + sum(node.right);
        max = Math.max(max, s * (total - s));

        return s;
    }
}
