package coding.test.leetcode.dynamic;

public class PushDominos {

    public String pushDominoes(String d) {
        int n = d.length();

        int[] right = new int[n];
        int[] left = new int[n];

        int start = 0;
        int end = d.length() - 1;

        while (start < n) {
            if (d.charAt(start) == 'R') {
                right[start] = 1;
            }
            if (d.charAt(end) == 'L') {
                left[end] = 1;
            }
            if (start > 0 && d.charAt(start) == '.' && right[start - 1] > 0) {
                right[start] = right[start - 1] + 1;
            }
            if (end < n - 1 && d.charAt(end) == '.' && left[end + 1] > 0) {
                left[end] = left[end + 1] + 1;
            }

            start++;
            end--;
        }

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < n; i++) {
            if (right[i] == 0 && left[i] == 0) {
                result.append('.');
            } else if (right[i] == 0 || left[i] == 0) {
                char ch = right[i] > 0 ? 'R' : 'L';
                result.append(ch);
            } else {
                if (right[i] == left[i]) {
                    result.append('.');
                } else {
                    char ch = right[i] > left[i] ? 'L' : 'R';
                    result.append(ch);
                }
            }

        }

        return result.toString();
    }
}
