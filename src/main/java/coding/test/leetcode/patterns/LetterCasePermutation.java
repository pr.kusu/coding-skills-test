package coding.test.leetcode.patterns;

import java.util.ArrayList;
import java.util.List;

public class LetterCasePermutation {

    public List<String> letterCasePermutation(String s) {
        List<String> res = new ArrayList<>();

        dfs(s.toCharArray(), 0, res);
        return res;
    }

    private void dfs(char[] arr, int pos, List<String> result) {
        if (pos == arr.length) {
            result.add(new String(arr));
            return;
        }

        if (arr[pos] >= '0' && arr[pos] <= '9') {
            dfs(arr, pos + 1, result);
            return;
        }

        arr[pos] = Character.toLowerCase(arr[pos]);
        dfs(arr, pos + 1, result);
        arr[pos] = Character.toUpperCase(arr[pos]);
        dfs(arr, pos + 1, result);
    }
}
