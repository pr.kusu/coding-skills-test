package coding.test.leetcode.patterns;

import java.util.Arrays;

public class HouseRobber {

    public int rob(int[] num) {
        int last = 0;
        int now = 0;
        int tmp = 0;

        for (int n : num) {
            tmp = now;
            now = Math.max(now, last + n);
            last = tmp;
        }

        return now;
    }

    public int rob2(int[] nums) {

        if (nums.length == 0) {
            return 0;
        }

        int[] dp = new int[nums.length + 1];

        if (nums.length < 2) {
            return nums[0];
        }

        dp[0] = nums[0];
        dp[1] = Math.max(dp[0], nums[1]);

        for (int i = 2; i < nums.length; i++) {
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i]);
        }
        return dp[nums.length - 1];
    }

    public int rob3(int[] nums) {

        int[] dp = new int[nums.length];
        Arrays.fill(dp, -1);

        return helper(nums, 0, dp);
    }

    public int helper(int[] nums, int i, int[] dp) {
        if (i >= nums.length) {
            return 0;
        }

        if (dp[i] >= 0) {
            return dp[i];
        }

        int a = helper(nums, i + 2, dp) + nums[i];
        int b = helper(nums, i + 1, dp);

        dp[i] = Math.max(a, b);

        return dp[i];
    }
}
