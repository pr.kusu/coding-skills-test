package coding.test.leetcode.patterns;

import coding.test.leetcode.curated.ListNode;

public class ReverseKGroup {

    public ListNode reverseKGroup(ListNode head, int k) {
        int n = 0;
        for (ListNode start = head; start != null; n++) {
            start = start.next;
        }

        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode prev = dummy, tail = head;

        while (n >= k) {
            //[1,2,3,4,5]
            for (int i = 1; i < k; i++) {
                // reverse the list
                ListNode next = tail.next.next;
                tail.next.next = prev.next;
                prev.next = tail.next;
                tail.next = next;
            }

            prev = tail;
            tail = tail.next;
            n -= k;
        }

        return dummy.next;
    }
}
