package coding.test.leetcode.patterns;

import java.util.Arrays;

public class SudokoSolver {

    public void solveSudoku(char[][] board) {
        dfs(board, 0);
    }

    private boolean dfs(char[][] board, int pos) {
        if (pos == 81) {
            return true;
        }

        int i = pos / 9;
        int j = pos % 9;

        boolean[] valid = new boolean[board.length];

        if (board[i][j] != '.') {
            return dfs(board, pos + 1);
        }

        validate(board, i, j, valid);

        for (int k = 0; k < 9; k++) {
            if (valid[k]) {
                board[i][j] = (char) ('1' + k);
                if (dfs(board, pos + 1)) {
                    return true;
                }
            }
        }

        board[i][j] = '.';
        return false;
    }

    private void validate(char[][] board, int i, int j, boolean[] valid) {
        Arrays.fill(valid, true);

        for (int k = 0; k < 9; k++) {

            if (board[i][k] != '.') valid[board[i][k] - '1'] = false;
            if (board[k][j] != '.') valid[board[k][j] - '1'] = false;
            int r = i / 3 * 3 + k / 3;
            int c = j / 3 * 3 + k % 3;
            if (board[r][c] != '.') valid[board[r][c] - '1'] = false;
        }
    }
}
