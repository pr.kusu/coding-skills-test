package coding.test.leetcode.patterns;

import java.util.TreeSet;

public class CountRangeSum {

    public int countRangeSum(int[] nums, int lower, int upper) {
        Sum[] sum = new Sum[nums.length + 1];

        sum[0] = new Sum(0, 0);

        for (int i = 1; i < sum.length; i++) {
            sum[i] = new Sum(i, sum[i - 1].val + nums[i - 1]);
        }

        int count = 0;

        TreeSet<Sum> treeSet = new TreeSet<>();

        for (int i = sum.length - 1; i >= 0; i--) {

            Sum low = new Sum(-1, sum[i].val + lower);
            Sum high = new Sum(sum.length, sum[i].val + upper);
            count += treeSet.subSet(low, high).size();

            treeSet.add(sum[i]);
        }

        return count;
    }

    class Sum implements Comparable<Sum> {
        private int index;
        private int val;

        public Sum(int index, int value) {
            this.index = index;
            this.val = value;
        }

        @Override
        public int compareTo(Sum o) {
            if (o.val == this.val) {
                return Integer.compare(this.index, o.index);
            }
            return Integer.compare(this.val, o.val);
        }
    }
}
