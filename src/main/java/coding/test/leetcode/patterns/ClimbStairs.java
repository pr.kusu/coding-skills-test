package coding.test.leetcode.patterns;

public class ClimbStairs {

    public int climbStairs(int n) {
        if (n < 0) {
            return 0;
        }

        int[] dp = new int[n + 1];

        return climb(n, 0, dp);
    }

    public int climb(int n, int i, int[] dp) {
        if (i > n) {
            return 0;
        }

        if (i == n) {
            return 1;
        }

        if (dp[i] != 0) {
            return dp[i];
        }

        dp[i] = climb(n, i + 1, dp) + climb(n, i + 2, dp);

        return dp[i];
    }

}
