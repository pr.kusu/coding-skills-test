package coding.test.leetcode.patterns;

import java.util.ArrayList;
import java.util.List;

public class MergeIntervals {

    public int[][] insert(int[][] intervals, int[] newInterval) {
        List<int[]> result = new ArrayList<>();

        for (int i = 0; i < intervals.length; i++) {

            int[] current = intervals[i];

            if (current[1] < newInterval[0]) {
                result.add(current);
                continue;
            }

            if (current[0] <= newInterval[1]) {
                //[1,4] [2,6]
                int min = Math.min(current[0], newInterval[0]);
                int max = Math.max(current[1], newInterval[1]);

                newInterval[0] = min;
                newInterval[1] = max;
                continue;
            }

            result.add(newInterval);
            newInterval = current;
        }

        // last interval is missed. Plus if the input intervals is length 0, this will be the guard
        result.add(newInterval);
        return result.toArray(new int[result.size()][]);
    }
}
