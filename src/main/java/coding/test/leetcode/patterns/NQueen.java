package coding.test.leetcode.patterns;

import java.util.ArrayList;
import java.util.List;

public class NQueen {

    List<List<String>> result = new ArrayList<>();

    public List<List<String>> solveNQueens(int n) {

        char[][] board = new char[n][n];

        solve(board, 0, n);
        return result;
    }

    private boolean solve(char[][] board, int row, int n) {
        if (row == n) {
            result.add(buildResult(board));
            return true;
        }

        for (int k = 0; k < n; k++) {
            if (validate(board, row, k)) {
                board[row][k] = 'Q';
                solve(board, row + 1, n);
                board[row][k] = '\u0000';
            }
        }

        return true;
    }

    boolean validate(char[][] grid, int row, int col) {
        // Ignoring checking bottom diagonals and bottom cells because those cells are not yet filled.
        // i.e. if row is 1 and col is 1, it means that any cell beyond row 1 and col 1 is not yet filled
        for (int i = row - 1; i >= 0; i--) { // checking all cells top of this cell
            if ('Q' == grid[i][col]) return false;
        }
        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--) { // checking top left diagonal of this cell
            if ('Q' == grid[i][j]) return false;
        }
        for (int i = row - 1, j = col + 1; i >= 0 && j < grid.length; i--, j++) { // checking top right diagonal
            if ('Q' == grid[i][j]) return false;
        }
        return true;
    }

    private List<String> buildResult(char[][] board) {
        List<String> result = new ArrayList<>();

        for (int i = 0; i < board.length; i++) {
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == '\u0000') {
                    builder.append('.');
                } else {
                    builder.append(board[i][j]);
                }
            }
            result.add(builder.toString());
        }

        return result;
    }
}
