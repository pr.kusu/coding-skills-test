package coding.test.ctci.tree;

public class ValidateBST {

    public static boolean validate(TreeNode<Integer> treeNode) {

        if (treeNode == null) {
            return true;
        }

        if (treeNode.left != null && treeNode.left.val > treeNode.val) {
            return false;
        }

        if (treeNode.right != null && treeNode.right.val < treeNode.val) {
            return false;
        }

        return validate(treeNode.left) && validate(treeNode.right);
    }

}
