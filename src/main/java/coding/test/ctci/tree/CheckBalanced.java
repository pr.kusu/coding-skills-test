package coding.test.ctci.tree;

public class CheckBalanced {

    public boolean checkBalanced(TreeNode<Integer> root) {

        if (root == null) {
            return true;
        }

        int left = getHeight(root.left, 0);
        int right = getHeight(root.right, 0);

        if (left == -1 || right == -1 || Math.abs(left - right) > 1) {
            return false;
        } else {
            return true;
        }
    }

    private int getHeight(TreeNode<Integer> node, int current) {
        if (node == null) {
            return current;
        }

        int left = getHeight(node.left, current + 1);
        int right = getHeight(node.right, current + 1);

        if (left == -1 || right == -1) {
            return -1;
        }

        if (Math.abs(left - right) > 1) {
            return -1;
        }

        return Math.max(left, right);

    }
}
