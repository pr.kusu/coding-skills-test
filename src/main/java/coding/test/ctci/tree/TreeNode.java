package coding.test.ctci.tree;

public class TreeNode<T> {

    public TreeNode<T> parent;

    public TreeNode<T> left;

    public TreeNode<T> right;

    public T val;

    public TreeNode(T val) {
        this.val = val;
    }

}
