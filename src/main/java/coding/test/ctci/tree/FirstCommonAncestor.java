package coding.test.ctci.tree;

public class FirstCommonAncestor {


    public static TreeNode<Integer> commonAncestor(TreeNode<Integer> root, TreeNode<Integer> t1, TreeNode<Integer> t2) {
        if (!isLeft(root, t2) || !isLeft(root, t1)) {
            return null;
        }

        return ancestorHelper(root, t1, t2);
    }

    private static TreeNode<Integer> ancestorHelper(TreeNode<Integer> root, TreeNode<Integer> p, TreeNode<Integer> q) {
        if (root == null || root == p || root == q) {
            return root;
        }

        boolean pOnLeft = isLeft(root.left, p);
        boolean qOnLeft = isLeft(root.left, q);

        if (pOnLeft != qOnLeft) {
            return root;
        }

        TreeNode<Integer> childSide = pOnLeft ? root.left : root.right;

        return ancestorHelper(childSide, p, q);
    }

    private static boolean isLeft(TreeNode<Integer> root, TreeNode<Integer> node) {
        if (root == null) {
            return false;
        }
        if (root == node) {
            return true;
        }

        return isLeft(root.left, node) || isLeft(root.right, node);

    }

    public static void main(String[] args) {

        // robert guster, 972-499-7858

        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};

        TreeNode<Integer> root = MinHeightTree.buildTree(array, 0, array.length);

        int[] array2 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        TreeNode<Integer> root2 = MinHeightTree.buildTree(array2, 0, array2.length);

        BTreePrinter.printNode(root);

        BTreePrinter.printNode(root2);

    }
}
