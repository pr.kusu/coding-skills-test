package coding.test.ctci.tree;

public class TreeDepth {

    public static long buildTree(long[] values) {

        long max = 0;

        long index = 0;
        int i = 1;
        for (long value : values) {
            if (value >= max) {
                max = value;
                index = i;
            }
            i++;
        }

        if (index != 0) {

            index = Math.round(Math.floor(Math.log(index) / Math.log(2)));

        }
        return index+1;
    }

    public static void main(String[] args) {
        long[] values = new long[]{1,1,1};

        System.out.println(buildTree(values));
    }
}
