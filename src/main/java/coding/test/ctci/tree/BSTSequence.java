package coding.test.ctci.tree;

import java.util.List;

public class BSTSequence {

    public static List<List<Integer>> findArray(TreeNode<Integer> treeNode) {

        return null;
    }

    private static void findArrayHelper(TreeNode<Integer> node, List<Integer> array) {
        if (node == null) {
            return;
        }
        // add root node
        array.add(node.val);
        findArrayHelper(node.left, array);
        findArrayHelper(node.right, array);
    }
}
