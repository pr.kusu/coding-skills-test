package coding.test.ctci.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

import static coding.test.leetcode.curated.NumIslands.Pair;

public class BFSArray {

    public static void bfs(int x, int y, char[][] array, boolean[][] visited) {
        Queue<Pair> queue = new LinkedList<>();

        // Insert the Starting Node into the queue
        queue.add(new Pair(x, y));

        while (!queue.isEmpty()) {
            // Poll the queue for next node to be visited
            Pair current = queue.poll();
            System.out.println("Visiting: x=" + current.x + ",y=" + current.y + " " + array[current.x][current.y]);

            //Get all the adjcent nodes
            List<Pair> adjcents = getAdjcents(current.x, current.y, array);

            // Visit all the Adjcent Nodes
            for (Pair next : adjcents) {
                if (!visited[next.x][next.y]) {
                    queue.add(next);
                    visited[next.x][next.y] = true;
                }
            }

            visited[x][y] = true;
        }
    }

    public static List<Pair> getAdjcents(int i, int j, char[][] grid) {
        List<Pair> list = new ArrayList<>();
        list.add(new Pair(i + 1, j));
        list.add(new Pair(i - 1, j));
        list.add(new Pair(i, j - 1));
        list.add(new Pair(i, j + 1));

        return list.stream().filter(value -> (
                value.x >= 0 && value.y >= 0 && value.x < grid.length && value.y < grid[0].length
        )).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        char[][] grid2 = new char[][]{
                {'A', 'B', 'C', 'D', 'E'},
                {'F', 'G', 'H', 'I', 'J'},
                {'K', 'L', 'M', 'N', 'O'},
                {'P', 'Q', 'R', 'S', 'T'}};
        System.out.println("BFS::");
        bfs(0, 0, grid2, new boolean[4][5]);
    }
}
