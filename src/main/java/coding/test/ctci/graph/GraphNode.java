package coding.test.ctci.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GraphNode {

    int data;
    boolean visited;
    List<GraphNode> links = new ArrayList<>();

    public GraphNode(int data) {
        this.data = data;
    }

    public void addAll(GraphNode... values) {
        links.addAll(Arrays.asList(values));
    }

    private GraphNode linkAt(int i) {
        return links.get(i);
    }
}
