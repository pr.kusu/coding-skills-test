package coding.test.ctci.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class FloodFill {

    static class Pair {
        private int first;
        private int second;

        public Pair(int first, int second) {
            this.first = first;
            this.second = second;
        }
    }

    public static int[][] floodFillDFS(int[][] image, int sr, int sc, int newColor) {

        int startColor = image[sr][sc];

        if (startColor == newColor) {
            return image;
        }

        dfsHelper(image, sr, sc, startColor, newColor);

        return image;
    }

    public static void dfsHelper(int[][] image, int sr, int sc, int startColor, int newColor) {
        if (sr < 0 || sc < 0 || sr >= image.length || sc >= image[0].length) {
            return;
        }

        if(image[sr][sc]==newColor){
            return;
        }

        if (startColor != newColor && image[sr][sc] != 0) {
            image[sr][sc] = newColor;

            dfsHelper(image, sr - 1, sc, startColor, newColor);
            dfsHelper(image, sr + 1, sc, startColor, newColor);
            dfsHelper(image, sr, sc - 1, startColor, newColor);
            dfsHelper(image, sr, sc + 1, startColor, newColor);
        }
    }

    public static int[][] floodFill(int[][] image, int sr, int sc, int newColor) {

        if (sr < 0 || sc < 0 || sr >= image.length || sc >= image[0].length) {
            return image;
        }
        int startColor = image[sr][sc];

        if (startColor == newColor) {
            return image;
        }

        Queue<Pair> queue = new ArrayBlockingQueue<>(1000);

        queue.add(new Pair(sr, sc));

        while (!queue.isEmpty()) {
            Pair current = queue.poll();
            image[current.first][current.second] = newColor;

            List<Pair> neighbors = getNeighbors(image, current.first, current.second, startColor);

            queue.addAll(neighbors);
        }

        return image;
    }

    private static List<Pair> getNeighbors(int[][] image, int x, int y, int startColor) {
        int[][] directions = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        List<Pair> pair = new ArrayList<>();

        for (int[] dir : directions) {
            int newX = dir[0] + x;
            int newY = dir[1] + y;

            if (newX >= 0 && newY >= 0 && newX < image.length && newY < image[0].length && image[newX][newY] == startColor) {
                pair.add(new Pair(newX, newY));
            }
        }

        return pair;

    }
}
