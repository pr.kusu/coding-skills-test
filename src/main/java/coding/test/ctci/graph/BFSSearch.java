package coding.test.ctci.graph;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class BFSSearch {

    public void search(GraphNode node) {
        Queue<GraphNode> queue = new ArrayBlockingQueue<>(100);
        visit(node);
        queue.add(node);

        while (!queue.isEmpty()) {
            GraphNode next = queue.poll();
            for (GraphNode neighbour : next.links) {
                if (!neighbour.visited) {
                    visit(neighbour);
                    queue.add(neighbour);
                }
            }
        }
    }

    public void visit(GraphNode node) {
        System.out.println("Visited graph Node:" + node.data);
        node.visited = true;
    }
}
