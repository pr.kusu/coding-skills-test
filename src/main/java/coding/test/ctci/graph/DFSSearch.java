package coding.test.ctci.graph;

public class DFSSearch {

    public void search(GraphNode node) {
        if (node == null) {
            return;
        }

        visit(node);

        for (GraphNode neighbour : node.links) {
            if (!neighbour.visited) {
                search(neighbour);
            }
        }

    }

    public void visit(GraphNode node) {
        System.out.println("Visited graph Node:" + node.data);
        node.visited = true;
    }
}
