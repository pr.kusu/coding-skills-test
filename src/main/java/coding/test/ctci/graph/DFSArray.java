package coding.test.ctci.graph;

public class DFSArray {

    public static void dfs(int x, int y, char[][] grid, boolean[][] visited) {
        if (x < 0 || y < 0 || x >= grid.length || y >= grid[0].length) {
            return;
        }
        if (visited[x][y]) {
            return;
        }
        visited[x][y] = true;
        System.out.println("Visiting: x=" + x + ",y=" + x + " =" + grid[x][y]);
        dfs(x - 1, y, grid, visited);
        dfs(x + 1, y, grid, visited);
        dfs(x, y - 1, grid, visited);
        dfs(x, y + 1, grid, visited);
    }

    public static void main(String[] args) {
        char[][] grid2 = new char[][]{
                {'A', 'B', 'C', 'D', 'E'},
                {'F', 'G', 'H', 'I', 'J'},
                {'K', 'L', 'M', 'N', 'O'},
                {'P', 'Q', 'R', 'S', 'T'}};
        System.out.println("DFS::");
        dfs(0, 0, grid2, new boolean[4][5]);
    }
}
