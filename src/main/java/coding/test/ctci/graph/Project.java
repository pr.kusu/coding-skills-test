package coding.test.ctci.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Project {
    enum State {
        VISITED, UNVISITED, PARTIAL
    }

    State nodeState = State.UNVISITED;
    List<Project> children = new ArrayList<>();
    Map<String, Project> map = new HashMap<>();
    String name;
    int dependencies = 0;

    public Project(String name) {
        this.name = name;
    }

    public void addNeighbour(Project node) {
        if (!map.containsKey(node.name)) {
            children.add(node);
            map.put(node.name, node);
            node.incrementDependencies();
        }
    }

    public void incrementDependencies() {
        dependencies++;
    }

}
