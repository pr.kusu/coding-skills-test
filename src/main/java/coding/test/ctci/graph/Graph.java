package coding.test.ctci.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {

    List<Project> nodes = new ArrayList<>();
    Map<String, Project> map = new HashMap<>();

    public Project getOrCreateNode(String name) {
        if (!map.containsKey(name)) {
            Project project = new Project(name);
            nodes.add(project);
            map.put(name, project);
        }

        return map.get(name);
    }

    public void addNode(String name) {
        Project node = new Project(name);
        nodes.add(node);
        map.put(name, node);
    }

    public void addEdge(String start, String end) {
        Project p1 = getOrCreateNode(start);
        Project p2 = getOrCreateNode(end);
        p1.addNeighbour(p2);
    }
}
