package coding.test.ctci.graph;

import java.util.Stack;

public class BuildOrder {

    // a, b, c, d, e, f
    // (a, d), (f, b), (b, d), (f, a), (d, c)

    public static Stack<Project> findBuildOrder(String[] projects, String[][] dependencies) {
        Graph graph = buildGraph(projects, dependencies);
        Stack<Project> stack = new Stack<>();

        for (Project p : graph.nodes) {
            if (p.nodeState.equals(Project.State.UNVISITED)) {
                if (!DFS(p, stack)) {
                    return null;
                }
            }
        }

        return stack;
    }

    public static boolean DFS(Project root, Stack<Project> stack) {
        if (root.nodeState.equals(Project.State.PARTIAL)) {
            return false;
        }

        if (root.nodeState.equals(Project.State.UNVISITED)) {
            root.nodeState = Project.State.PARTIAL;

            for (Project node : root.children) {
                if (!DFS(node, stack)) {
                    return false;
                }
            }

            root.nodeState = Project.State.VISITED;
            stack.push(root);
        }

        return true;
    }

    public static Graph buildGraph(String[] projects, String[][] dependencies) {
        Graph graph = new Graph();

        for (String project : projects) {
            graph.addNode(project);
        }

        for (String[] dep : dependencies) {
            String first = dep[0];
            String second = dep[1];

            graph.addEdge(first, second);
        }

        return graph;
    }
}
