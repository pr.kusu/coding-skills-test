package coding.test.ctci.dynamic;

public class RecursiveMultiply {

    public static int recurse(int a, int b) {
        if (b == 1) {
            return a;
        } else {
            return a + recurse(a, b - 1);
        }
    }

    public static void main(String[] args) {
        int value = recurse(5, 4);
        System.out.println(value);
    }
}
