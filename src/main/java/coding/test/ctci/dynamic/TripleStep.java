package coding.test.ctci.dynamic;

public class TripleStep {


    public static int getWays(int n, int[] mem) {
        if (n <= 0) {
            return 0;
        }
        if (n == 1) {
            mem[0] = 1;
            return 1;
        } else if (mem[n - 1] != 0) {
            return mem[n - 1];
        } else {
            int sum = 0;
            for (int i = 1; i <= 3; i++) {
                sum += getWays(n - i, mem);
            }

            return sum;
        }
    }


}
