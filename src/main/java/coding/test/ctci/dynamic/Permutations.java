package coding.test.ctci.dynamic;

import java.util.ArrayList;
import java.util.List;

public class Permutations {

    public static List<String> getPermutations(String s) {

        List<String> perms = new ArrayList<>();

        if (s == null || s.isEmpty()) {
            perms.add("");
            return perms;
        }

        List<String> subperms = getPermutations(s.substring(1));

        for (String word : subperms) {
            for (int j = 0; j <= word.length(); j++) {
                String str = insertCharAt(word, j, s.charAt(0));
                perms.add(str);
            }
        }

        return perms;
    }

    private static String insertCharAt(String s, int i, char c) {

        return s.substring(0, i) + c + s.substring(i);

    }
}
