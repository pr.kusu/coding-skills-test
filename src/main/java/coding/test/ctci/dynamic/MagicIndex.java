package coding.test.ctci.dynamic;

public class MagicIndex {

    public static int findMagicIndex(int[] array) {

        return magicIndexHelper(array, 0, array.length);
    }

    public static int magicIndexHelper(int[] array, int low, int high) {
        int mid = (low + high) / 2;

        if (mid == array[mid]) {
            return mid;
        }

        if (array[mid] < mid) {
            return magicIndexHelper(array, mid + 1, high);
        } else {
            return magicIndexHelper(array, low, mid - 1);
        }
    }
}
