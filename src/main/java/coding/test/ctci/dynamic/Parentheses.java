package coding.test.ctci.dynamic;

import java.util.Arrays;
import java.util.List;

public class Parentheses {

    public static void generate(List<String> array, int leftRem, int rightRem, char[] arr, int index) {
        if (leftRem < 0 || rightRem < leftRem) {
            return;
        }

        if (leftRem == 0 && rightRem == 0) {
            array.add(String.copyValueOf(arr));
            return;
        }

        // System.out.println("LeftRem, RightRem " + leftRem + "," + rightRem);

        arr[index] = '(';

        System.out.println("left, generate " + (leftRem - 1) + "," + rightRem + "," + (index + 1));
        generate(array, leftRem - 1, rightRem, arr, index + 1);

        arr[index] = ')';

        System.out.print("ArrayVal:" + Arrays.toString(arr));
        System.out.println("right, generate " + (leftRem) + "," + (rightRem - 1) + "," + (index + 1));
        generate(array, leftRem, rightRem - 1, arr, index + 1);
    }
}
