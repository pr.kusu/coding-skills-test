package coding.test.ctci.sort;

public class SearchDuplicate {

    public static void printDuplicate(int[] array) {
        BitSet set = new BitSet(32000);

        for (int num : array) {
            if (set.get(num)) {
                System.out.println(num);
            } else {
                set.set(num);
            }
        }
    }


    public static class BitSet {

        int[] elements;

        public BitSet(int size) {
            int bitSize = size >> 5;
            elements = new int[bitSize + 1];
        }

        public void set(int num) {
            int bitPos = num >> 5;

            elements[bitPos] |= 1 << (num % 32);

        }

        public boolean get(int num) {
            int bitPos = num >> 5;
            int bitFlag = elements[bitPos] & (1 << (num % 32));

            return bitFlag != 0;
        }

    }
}
