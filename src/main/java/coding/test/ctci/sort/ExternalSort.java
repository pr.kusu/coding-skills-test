package coding.test.ctci.sort;

import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

public class ExternalSort {

    public static void externalSort(List<Integer> list, int maxRam) {
        int chunk = list.size() / maxRam;

        List<List<Integer>> split = Lists.partition(list, chunk);

        System.out.println();
        System.out.println("Splitting list to RAM size:" + maxRam);
        for (List<Integer> partition : split) {
            Collections.sort(partition);

            partition.forEach(val -> System.out.print(val + ","));
            System.out.println();
        }

        int max = list.size();

        int currentSplit = 0;
        int count = 0;
        int[] indexMap = new int[3];

        System.out.println("Sorting..");
        while (count < max) {
            int currentVal = split.get(currentSplit).get(indexMap[currentSplit]);

            int minValueIndex = getMinVal(split, currentVal, indexMap);
            int minValue = split.get(minValueIndex).get(indexMap[minValueIndex]);
            indexMap[minValueIndex] += 1;
            System.out.print(minValue + ",");
            count++;
        }
    }

    private static int getMinVal(List<List<Integer>> list, int val, int[] indexMap) {
        int min = val;
        int listIndex = 0;
        for (int i = 0; i < list.size(); i++) {
            int current = list.get(i).get(indexMap[i]);
            if (current < min) {
                min = current;
                listIndex = i;
            }
        }
        return listIndex;
    }
}
