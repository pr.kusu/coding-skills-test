package coding.test.ctci.sort;

public class MergeSortedArray {

    public static int[] merge(int[] array1, int[] array2) {
        /// 1 2 4 _ _  // 0 3
        //  0  1  2  3  4


        int[] merge = new int[array1.length + array2.length];

        int first = 0;
        int second = 0;

        int max = Math.max(array1.length, array2.length);

        int i = 0;
        for (i = 0; i < merge.length; i++) {

            if (first < array1.length && array1[first] <= array2[second]) {
                merge[i] = array1[first];
                first++;
            } else if (second < array2.length) {
                merge[i] = array2[second];
                second++;
            }
        }

        int[] maxArray = array1.length > array2.length ? array1 : array2;
        int largeIndex = array1.length > array2.length ? first : second;
        for (int j = i; j < max; j++) {
            merge[j] = maxArray[i - largeIndex];
        }

        return merge;
    }
}
