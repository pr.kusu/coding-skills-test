package coding.test.ctci.sort;

public class SortedSearch {

    public static int sortedSearch(int[] array, int value) {

        int start = 1;

        while (getValueAt(array, start) != -1 && getValueAt(array, start) < value) {
            start *= 2;
        }

        return binarySearch(array, value, start / 2, start);
    }

    private static int binarySearch(int[] array, int value, int start, int end) {

        if (start > end) {
            return -1;
        }

        int mid = (start + end) / 2;

        int midValue = getValueAt(array, mid);

        if (midValue == value) {
            return mid;
        }

        if (midValue > 0 && midValue < value) {
            return binarySearch(array, value, mid + 1, end);
        } else {
            return binarySearch(array, value, start, mid - 1);
        }

    }

    private static int getValueAt(int[] array, int index) {
        if (index < 0 || index >= array.length) {
            return -1;
        }

        return array[index];
    }
}
