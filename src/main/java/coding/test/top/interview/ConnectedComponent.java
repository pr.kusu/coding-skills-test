package coding.test.top.interview;

import java.util.*;

public class ConnectedComponent {

    public int countComponents(int n, int[][] edges) {

        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }

        for (int[] edge : edges) {
            int s = edge[0];
            int t = edge[1];

            graph.get(s).add(t);
            graph.get(t).add(s);
        }

        int count = 0;
        Set<Integer> visited = new HashSet<>();
        for (int i = 0; i < n; i++) {
            if (!visited.contains(i)) {
                dfs(graph, graph.get(i), visited);
                count++;
            }
        }

        return count;
    }

    private void dfs(List<List<Integer>> graph, List<Integer> nodes, Set<Integer> visited) {
        for (Integer node : nodes) {
            if (!visited.contains(node)) {
                visited.add(node);
                dfs(graph, graph.get(node), visited);
            }
        }
    }

    private void bfs(List<List<Integer>> graph, Set<Integer> visited, int start) {
        visited.add(start);
        Queue<Integer> queue = new LinkedList<>();
        queue.add(start);
        while (!queue.isEmpty()) {

            int current = queue.poll();
            List<Integer> childs = graph.get(current);

            for (Integer child : childs) {
                if (visited.contains(child)) {
                    continue;
                }
                queue.add(child);
                visited.add(child);
            }
        }
    }
}
