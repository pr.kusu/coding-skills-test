package coding.test.top.interview;

public class MedianOfArrays {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int left = 0;
        int right = 0;

        int count = 0;
        int total = nums1.length + nums2.length;
        int size = total / 2 + 1;
        int[] merged = new int[size];

        while (count < size) {
            if (left < nums1.length && (right >= nums2.length || nums1[left] <= nums2[right])) {
                merged[count++] = nums1[left++];
            } else if (right < nums2.length) {
                merged[count++] = nums2[right++];
            }
        }

        if (total % 2 != 0) {
            return merged[merged.length - 1];
        } else {
            return (merged[merged.length - 1] + merged[merged.length - 2]) / 2.0;
        }
    }
}
