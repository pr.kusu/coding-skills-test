package coding.test.top.interview;

import java.util.Arrays;

public class PerfectSquares {

    public int numSquares(int n) {
        int[] countMap = new int[n + 1];
        Arrays.fill(countMap, Integer.MAX_VALUE);

        return helper(n, countMap, 0);

    }

    private int helper(int rem, int[] countMap, int count) {

        if (countMap[rem] <= count) {
            return countMap[rem];
        }

        if (rem == 0) {
            int currPath = countMap[rem];
            countMap[rem] = Math.min(currPath, count);
            return countMap[0];
        }

        if (count >= countMap[0]) {
            return countMap[0];
        }

        countMap[rem] = count;

        int sqrt = (int) Math.floor(Math.sqrt(rem));
        for (int i = sqrt; i >= 1; i--) {
            int next = rem - i * i;
            if (countMap[next] <= count) {
                continue;
            }
            helper(rem - i * i, countMap, count + 1);
        }


        return countMap[0];
    }


    public int numSquares2(int n) {
        int[] mem = new int[n + 1];
        Arrays.fill(mem, -1);

        return dfs(n, mem);
    }

    int dfs(int n, int[] mem) {
        if (n < 0) return Integer.MAX_VALUE;
        if (n == 0) return 0;
        if (mem[n] != -1) return mem[n];
        int min = n + 1;
        for (int i = 1; i * i <= n; i++) {
            min = Math.min(dfs(n - (i * i), mem), min);
        }
        mem[n] = min + 1;
        return min + 1;
    }

    public int numSquares3(int n) {
        int[] dp = new int[n + 1];
        for (int i = 0; i < dp.length; i++) {
            dp[i] = i;
        }

        // now try to minimize the no of perfect squares neede to sum upto i
        // for making 1 , we need one perfect square which is 1
        // so dp[1]=1

        for (int i = 2; i < dp.length; i++) {
            // for each i,start traversing from j=1 upto a number whose square is
            // less than or equal to i,because we don't need any number whose square
            // is greater than i
            for (int j = 1; j * j <= i; j++) {
                // now either we can take this j or not
                // if we don't take this j,then dp[i]=dp[i]
                // if we take this j,then dp[i]= dp[i-j*j]+1
                int rest = i - (j * j);
                dp[i] = Math.min(dp[i], dp[rest] + 1);
            }
        }

        return dp[n];
    }

    public static void main(String[] args) {
        int res = new PerfectSquares().numSquares(48);

        System.out.print(res);
    }

}
