package coding.test.top.interview;

import java.util.HashSet;
import java.util.Set;

public class LongRepeatingSubstring {

    public int longestSubstring2(String s, int k) {
        int[] freqMap = new int[26];

        for (int i = 0; i < s.length(); i++) {
            freqMap[s.charAt(i) - 'a']++;
        }

        Set<Character> splitSet = new HashSet<>();
        for (int i = 0; i < freqMap.length; i++) {
            if (freqMap[i] > 0 && freqMap[i] < k) {
                splitSet.add((char) ('a' + i));
            }
        }

        if (splitSet.isEmpty()) {
            return s.length();
        }

        int max = 0;
        int i = 0, j = 0;

        while (j < s.length()) {
            char c = s.charAt(j);
            if (!splitSet.contains(c)) {
                j++;
            } else {
                if (j != i) {
                    max = Math.max(max, longestSubstring(s.substring(i, j), k));
                }
                i = j + 1;
            }
        }

        if (i != j) {
            return Math.max(max, longestSubstring(s.substring(i, j), k));
        }
        return max;
    }


    public int longestSubstring(String s, int k) {
        int left = 0, max = 0;
        int[] count = new int[26];
        ;
        for (int right = 0; right < s.length(); right++) {
            count[s.charAt(right) - 'a']++;

            if (count[s.charAt(right) - 'a'] >= k) {
                while (right - left + 1 >= k) {
                    if (isValid(count, k)) {
                        max = Math.max(max, right - left + 1);
                        break;
                    }
                    count[s.charAt(left) - 'a']--;
                    left++;
                }
                for (int i = 0; i < left; i++) {
                    count[s.charAt(i) - 'a']++;
                }
                left = 0;
            }
        }
        return max;
    }

    public boolean isValid(int[] count, int k) {

        for (int i = 0; i < 26; i++) {
            if (count[i] < k && count[i] != 0) {
                return false;
            }
        }
        return true;
    }
}
