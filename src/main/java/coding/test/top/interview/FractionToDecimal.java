package coding.test.top.interview;

import java.util.HashMap;
import java.util.Map;

public class FractionToDecimal {

    private Map<Long, Integer> hash = new HashMap<>();
    private boolean repeated = false;
    int repeatedIndex = 0;

    public String fractionToDecimal(int n, int d) {
        long numerator = n;
        long denominator = d;
        String sign = numerator * denominator >= 0 ? "" : "-";

        long quoitent = numerator / denominator;
        quoitent = quoitent < 0 ? -quoitent : quoitent;
        String q = Long.toString(quoitent);

        long rem = numerator % denominator;
        rem = Math.abs(rem);
        if (rem == 0) {
            return sign + q;
        } else {
            StringBuilder builder = new StringBuilder(sign).append(q).append(".");
            StringBuilder decimal = new StringBuilder();
            calculateDecimals(rem * 10, Math.abs(denominator), decimal, Integer.MAX_VALUE, 0);
            if (repeated) {
                return builder.append(decimal.substring(0, repeatedIndex)).append("(")
                        .append(decimal.substring(repeatedIndex))
                        .append(")").toString();
            }
            return builder.append(decimal).toString();
        }
    }

    private void calculateDecimals(long rem, long dem, StringBuilder result, int places, int count) {
        if (count == places) {
            return;
        }
        if (hash.containsKey(rem)) {
            repeatedIndex = hash.get(rem);
            repeated = true;
            return;
        }
        hash.put(rem, count);
        long q = rem / dem;
        result.append(q);
        long r = rem % dem;
        if (r == 0) {
            return;
        }
        calculateDecimals(r * 10, dem, result, places, count + 1);

    }

}
