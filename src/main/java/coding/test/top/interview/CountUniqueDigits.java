package coding.test.top.interview;

public class CountUniqueDigits {

    public int countNumbersWithUniqueDigits(int n) {
        if (n == 0) {
            return 1;
        }

        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 10;

        int prod = 9;
        for (int i = 1; i < n; i++) {
            prod *= (10 - i);
            dp[i + 1] = dp[i] + prod;
        }

        return dp[n];
    }
}
