package coding.test.top.interview;

import java.util.HashSet;
import java.util.Set;

public class LongestRepeating2 {

    public int longestSubstring(String s, int k) {
        int[] freqMap = new int[26];

        for (int i = 0; i < s.length(); i++) {
            freqMap[s.charAt(i) - 'a']++;
        }

        Set<Character> splitSet = new HashSet<>();

        for (int i = 0; i < freqMap.length; i++) {
            int curFreq = freqMap[i];
            if (curFreq > 0 && curFreq < k) {
                // add the split point at which we encounter character whose freq is less than k on given string
                splitSet.add((char) ('a' + i));
            }
        }

        if (splitSet.isEmpty()) {
            // if split point is empty then return current length as max
            return s.length();
        }

        int i = 0, j = 0, len = 0;

        while (j <= s.length()) {
            if (j == s.length() || splitSet.contains(s.charAt(j))) {
                // recursively calculate the max length upto current split point
                len = Math.max(len, longestSubstring(s.substring(i, j), k));
                // reset next start point to upcoming character
                i = j + 1;
            }

            j++;
        }

        return len;
    }
}
