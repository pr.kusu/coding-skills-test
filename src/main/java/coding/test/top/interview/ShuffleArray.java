package coding.test.top.interview;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ShuffleArray {

    private int[] nums;
    private Random random = new Random();
    private List<List<Integer>> permutations = new ArrayList<>();


    public ShuffleArray(int[] nums) {
        this.nums = nums;
    }

    /**
     * Resets the array to its original configuration and return it.
     */
    public int[] reset() {
        return nums;
    }

    /**
     * Returns a random shuffling of the array.
     */
    public int[] shuffle() {
        permutations.clear();
        calculatePerms(new LinkedList<>(), nums);
        return permutations.get(0).stream().mapToInt(i -> i).toArray();
    }

    private void calculatePerms(LinkedList<Integer> perm, int[] nums) {
        if (perm.size() == nums.length) {
            permutations.add(new ArrayList<>(perm));
            return;
        }
        if (permutations.size() == 1) {
            return;
        }

        for (int i = 0; i < nums.length; i++) {
            int randIndex = random.nextInt(nums.length);
            int value = nums[randIndex];
            if (perm.contains(value)) {
                continue;
            }
            perm.add(value);
            calculatePerms(perm, nums);
            perm.removeFirst();
        }
    }
}
