package coding.test.top.interview;


import coding.test.leetcode.curated.ListNode;

public class SortLinkedList {

    public ListNode sortList(ListNode head) {
        // base case
        if (head == null || head.next == null) return head;

        // recursive case
        int len = 0;
        ListNode middle = head;
        for (ListNode current = head; current != null; current = current.next) len++;
        for (int i = 0; i < (len - 1) / 2; i++) middle = middle.next;

        ListNode right = middle.next, left = head;
        middle.next = null;
        return merge(sortList(left), sortList(right));
    }

    ListNode merge(ListNode leftSorted, ListNode rightSorted) {
        if (leftSorted == null) return rightSorted;
        if (rightSorted == null) return leftSorted;
        ListNode small = leftSorted.val < rightSorted.val ? leftSorted : rightSorted;
        ListNode big = (leftSorted == small) ? rightSorted : leftSorted;
        small.next = merge(small.next, big);
        return small;
    }
}
