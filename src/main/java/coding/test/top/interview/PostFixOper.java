package coding.test.top.interview;

import java.util.ArrayDeque;
import java.util.Deque;

public class PostFixOper {

    public int evalRPN(String[] tokens) {
        Deque<String> stack = new ArrayDeque<>();

        for (String s : tokens) {
            int res = 0;
            switch (s) {
                case "+":
                    res = add(stack, 1);
                    break;
                case "-":
                    res = add(stack, -1);
                    break;
                case "*":
                    res = mul(stack, false);
                    break;
                case "/":
                    res = mul(stack, true);
                    break;
                default:
                    res = Integer.parseInt(s);
                    break;
            }

            stack.push(String.valueOf(res));
        }

        return Integer.parseInt(stack.pop());
    }

    private int add(Deque<String> stack, int op) {
        int first = Integer.parseInt(stack.pop());
        int second = Integer.parseInt(stack.pop());

        return second + op * first;
    }

    private int mul(Deque<String> stack, boolean div) {
        int first = Integer.parseInt(stack.pop());
        int second = Integer.parseInt(stack.pop());

        return div ? second / first : first * second;
    }

    public static void main(String[] args) {
        new PostFixOper().evalRPN(new String[]{"4", "13", "5", "/", "+"});
    }

}
