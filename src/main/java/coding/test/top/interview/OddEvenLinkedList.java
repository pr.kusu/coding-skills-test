package coding.test.top.interview;

import coding.test.leetcode.curated.ListNode;

public class OddEvenLinkedList {

    public ListNode oddEvenList(ListNode head) {

        if (head == null || head.next == null) {
            return head;
        }

        ListNode first = head;
        ListNode second = head.next;
        ListNode secondHead = head.next;

        while (first.next != null || second.next != null) {
            first.next = second.next;
            if (first.next != null) {
                first = first.next;
            }

            second.next = first.next;
            if (second.next != null) {
                second = second.next;
            }
        }

        second.next = null;
        first.next = secondHead;

        return head;
    }

}
