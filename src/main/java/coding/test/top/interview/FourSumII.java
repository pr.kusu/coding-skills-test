package coding.test.top.interview;

import java.util.HashMap;
import java.util.Map;

public class FourSumII {

    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        int cnt = 0;
        Map<Integer, Integer> countMap = new HashMap<>();

        for (int a : A) {
            for (int b : B) {
                int cur = a + b;
                countMap.put(cur, countMap.getOrDefault(cur, 0) + 1);
            }
        }
        int count = 0;

        for (int c : C) {
            for (int d : D) {
                int cur = -(c + d);
                if (countMap.containsKey(cur)) {
                    count += countMap.get(cur);
                }
            }
        }

        return count;
    }
}
