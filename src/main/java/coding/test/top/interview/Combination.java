package coding.test.top.interview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Combination {

    public List<List<Integer>> combine(int n, int k) {
        if (n < k) {
            return Collections.emptyList();
        }

        List<Integer> input = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            input.add(i);
        }
        List<List<Integer>> finalResult = new ArrayList<>();

        combination(input, finalResult, new ArrayList<>(), 0, k);

        return finalResult;
    }

    private void combination(List<Integer> input, List<List<Integer>> finalResult,
                             List<Integer> tempResult, int index, int k) {

        if (tempResult.size() == k) {
            finalResult.add(new ArrayList<>(tempResult));
            return;
        }

        for (int i = index; i < input.size(); i++) {
            tempResult.add(input.get(i));
            combination(input, finalResult, tempResult, i + 1, k);
            tempResult.remove(tempResult.size() - 1);
        }
    }
}
