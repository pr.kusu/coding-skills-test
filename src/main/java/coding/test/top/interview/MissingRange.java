package coding.test.top.interview;

import java.util.ArrayList;
import java.util.List;

public class MissingRange {

    /**
     * Input: nums = [0, 1, 3, 50, 75], lower = 0 and upper = 99,
     * Output: ["2", "4->49", "51->74", "76->99"]
     *
     * @param nums
     * @param lower
     * @param upper
     * @return
     */
    public List<String> findMissingRanges(int[] nums, int lower, int upper) {
        List<String> result = new ArrayList<>();
        if (nums.length == 0) {
            if (lower == upper) {
                result.add(lower + "");
            } else {
                result.add(lower + "->" + upper);
            }
            return result;
        }

        int expected = lower;
        for (int i = 0; i <= nums.length; i++) {
            int right = i < nums.length ? nums[i] : upper;

            if (i > 0 && nums[i - 1] == right) {
                continue;
            }

            if (right != expected) {

                int left = i - 1 < 0 ? lower : nums[i - 1] + 1;
                right = right == upper ? upper : right -1;

                if (left == right) {
                    result.add(String.valueOf(left));
                } else {
                    result.add(left + "->" + right);
                }
                expected = right;
            }

            expected++;
        }

        return result;
    }

    public List<String> findMissingRanges2(int[] A, int lower, int upper) {
        List<String> result = new ArrayList<String>();
        int pre = lower - 1;
        // 1,2,3,4,10
        for (int i = 0; i <= A.length; i++) {
            int after = i == A.length ? upper + 1 : A[i];
            if (pre + 2 == after) {
                result.add(String.valueOf(pre + 1));
            } else if (pre + 2 < after) {
                result.add((pre + 1) + "->" + (after - 1));
            }
            pre = after;
        }
        return result;
    }
}
