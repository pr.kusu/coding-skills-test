package coding.test.top.interview;

import java.util.*;

public class NestedIterator implements Iterator<Integer> {

    public interface NestedInteger {

        // @return true if this NestedInteger holds a single integer, rather than a nested list.
        public boolean isInteger();

        // @return the single integer that this NestedInteger holds, if it holds a single integer
        // Return null if this NestedInteger holds a nested list
        public Integer getInteger();

        // @return the nested list that this NestedInteger holds, if it holds a nested list
        // Return null if this NestedInteger holds a single integer
        public List<NestedInteger> getList();
    }

    Deque<NestedInteger> s;

    public NestedIterator(List<NestedInteger> nestedList) {
        s = new ArrayDeque<>(nestedList == null ? Arrays.asList() : nestedList);
    }

    @Override
    public Integer next() {
        if (!hasNext()) {
            return null;
        }
        return s.poll().getInteger();
    }

    @Override
    public boolean hasNext() {
        while (!s.isEmpty() && !s.peek().isInteger()) {
            List<NestedInteger> list = s.pop().getList();
            for (int i = list.size() - 1; i >= 0; i--) s.push(list.get(i));
        }
        return !s.isEmpty();
    }
}
