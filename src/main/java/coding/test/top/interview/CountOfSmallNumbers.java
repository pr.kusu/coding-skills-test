package coding.test.top.interview;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class CountOfSmallNumbers {

    static class TreeNode {
        TreeNode left;
        TreeNode right;
        int val;
        //Count left nodes
        int leftCount = 0;

        TreeNode(int val) {
            this.val = val;
        }
    }


    public List<Integer> countSmaller(int[] nums) {
        int[] result = new int[nums.length];
        TreeNode root = null;

        for (int i = nums.length - 1; i >= 0; i--) {
            root = insertIntoBST(root, result, nums[i], i, 0);
        }

        return Arrays.stream(result).boxed().collect(Collectors.toList());
    }


    public TreeNode insertIntoBST(TreeNode root, int[] result, int val, int index, int count) {
        if (root == null) {
            root = new TreeNode(val);
            result[index] = count;
            return root;
        }

        if (val > root.val) {
            count += root.leftCount + 1;
            root.right = insertIntoBST(root.right, result, val, index, count);
        } else {
            root.left = insertIntoBST(root.left, result, val, index, count);
            root.leftCount += 1;
        }

        return root;
    }
}