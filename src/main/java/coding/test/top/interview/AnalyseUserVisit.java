package coding.test.top.interview;

import java.util.*;

public class AnalyseUserVisit {

    public List<String> mostVisitedPattern(String[] username, int[] timestamp, String[] website) {
        Map<String, List<String>> userWebsite = new HashMap<>();
        List<Integer> indexList = new ArrayList<>();
        for (int i = 0; i < timestamp.length; i++) {
            indexList.add(i);
        }

        indexList.sort(Comparator.comparingInt(o -> timestamp[o]));

        for (Integer index : indexList) {
            String cur = username[index];
            if (!userWebsite.containsKey(cur)) {
                userWebsite.put(cur, new ArrayList<>());
            }
            userWebsite.get(cur).add(website[index]);
        }

        Map<String, Integer> finalResult = new LinkedHashMap<>();

        for (Map.Entry<String, List<String>> user : userWebsite.entrySet()) {
            Set<String> current = new HashSet<>();
            combination(user.getValue(), current, new ArrayList<>(), 0);
            current.forEach(val -> {
                finalResult.put(val, finalResult.getOrDefault(val, 0) + 1);
            });
        }

        List<String> result = new ArrayList<>(finalResult.keySet());
        Collections.sort(result, (o1, o2) -> {
            int freq = Integer.compare(finalResult.get(o1), finalResult.get(o2));
            if (freq == 0) {
                return o2.compareTo(o1);
            }
            return freq;
        });

        return Arrays.asList(result.get(result.size() - 1).split(","));
    }

    private void combination(List<String> input, Set<String> finalResult, List<String> tempResult, int index) {
        if (tempResult.size() == 3) {
            String tmpStr = String.join(",", tempResult);
            finalResult.add(tmpStr);
            System.out.println(tmpStr);
            return;
        }

        for (int i = index; i < input.size(); i++) {
            tempResult.add(input.get(i));
            combination(input, finalResult, tempResult, i + 1);
            tempResult.remove(tempResult.size() - 1);
        }
    }

    public static void main(String[] args) {
        String[][] input = new String[][]{
                {"joe", "joe", "joe", "james", "james", "james", "james", "mary", "mary", "mary"},
                {"home", "about", "career", "home", "cart", "maps", "home", "home", "about", "career"}
        };
        int[] timestamp = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        new AnalyseUserVisit().combination(Arrays.asList("home", "about", "career", "home"), new HashSet<>(),
                new ArrayList<>(), 0);
    }

}
