package coding.test.top.interview;

public class Vector2D {

    private int[][] vector;
    int row = 0;
    int col = 0;

    public Vector2D(int[][] v) {
        this.vector = v;
    }

    public int next() {

        while (vector[row].length == 0) {
            row++;
        }

        int val = vector[row][col++];
        if (col >= vector[row].length) {
            col = 0;
            row++;
        }
        return val;

    }

    public boolean hasNext() {
        int currentRow = row;
        while (currentRow < vector.length &&
                col >= vector[currentRow].length) {
            currentRow++;
        }
        return currentRow < vector.length;
    }

}
