package coding.test.top.interview;

public class SortColors {
    //Input: [2,0,2,1,0,2,0,1,1,0]
    //Output: [0,0,1,1,2,2]

    public void sortColors(int[] nums) {

        int left = 0;
        int right = nums.length - 1;
        int zeroIndex = 0;

        while (left <= right) {
            if (nums[left] == 0) {
                if (left != zeroIndex) {
                    swap(zeroIndex, left, nums);
                }
                if (nums[zeroIndex] == 0) {
                    zeroIndex++;
                }
                left++;
            } else if (nums[left] == 2) {
                swap(left, right, nums);
                right--;
            } else if (nums[left] == 1) {
                left++;
            }
        }

    }

    private void swap(int i, int j, int[] nums) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }

    public static void main(String[] args) {
        new SortColors().sortColors(new int[]{0, 1, 0});
    }
}
