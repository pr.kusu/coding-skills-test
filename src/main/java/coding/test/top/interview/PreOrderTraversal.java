package coding.test.top.interview;

import coding.test.leetcode.curated.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;

public class PreOrderTraversal {

    public List<Integer> preOrder(TreeNode treeNode) {
        if (treeNode == null) {
            return Collections.emptyList();
        }

        List<Integer> result = new ArrayList<>();

        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.add(treeNode);

        while (!queue.isEmpty()) {
            TreeNode current = queue.poll();

            result.add(current.val);

            if (current.left != null) {
                queue.add(current.left);
            }

            if (current.right != null) {
                queue.add(current.right);
            }
        }

        return result;
    }


}
