package coding.test.top.interview;

import java.util.HashMap;
import java.util.Map;

public class LongestSubstringKDistinct {

    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        int len = 0;
        int start = 0;
        // s = "eceba", k = 2

        char[] charArr = s.toCharArray();
        Map<Character, Integer> freqTable = new HashMap<>();

        for (int i = 0; i < charArr.length; i++) {
            char ch = charArr[i];
            freqTable.put(ch, freqTable.getOrDefault(ch, 0) + 1);

            if (freqTable.size() <= k) {
                len = Math.max(len, i - start + 1);
            } else {
                char first = s.charAt(start);
                freqTable.put(first, freqTable.get(first) - 1);
                if (freqTable.get(first) == 0) {
                    freqTable.remove(first);
                }
                start++;
            }
        }

        return len;
    }
}
