package coding.test.top.interview;

import java.util.Comparator;
import java.util.PriorityQueue;

public class KthSmallest {

    class Element {
        int[] arr;
        int row = 0;

        public Element(int[] arr) {
            this.arr = arr;
        }

        public int getTop() {
            return row < arr.length ? arr[row] : Integer.MAX_VALUE;
        }
    }

    public int kthSmallest(int[][] matrix, int k) {

        PriorityQueue<Element> queue =
                new PriorityQueue<>(Comparator.comparingInt(Element::getTop));

        for (int[] mat : matrix) {
            queue.add(new Element(mat));
        }

        for (int i = 0; i < k - 1; i++) {
            Element cur = queue.poll();

            if (cur.row < cur.arr.length) {
                cur.row++;
            }

            queue.add(cur);

        }

        return queue.peek().getTop();
    }
}
