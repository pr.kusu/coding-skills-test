package coding.test.top.interview;

public class GameOfLife {

    public void gameOfLife(int[][] board) {
        int[][] dir = new int[][]{{1, 0}, {0, 1}, {-1, 0}, {0, -1},
                {1, 1}, {-1, -1}, {1, -1}, {-1, 1}};

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {

                int liveNeighborCount = 0;
                for (int[] d : dir) {
                    int x = i + d[0];
                    int y = j + d[1];

                    if (x >= 0 && y >= 0 && x < board.length && y < board[0].length) {
                        if (Math.abs(board[x][y]) == 1) {
                            liveNeighborCount++;
                        }
                    }
                }

                if (board[i][j] == 0 && liveNeighborCount == 3) {
                    board[i][j] = 2;
                } else if (board[i][j] == 1) {
                    if (liveNeighborCount < 2) {
                        board[i][j] = -1;
                    } else if (liveNeighborCount > 3) {
                        board[i][j] = -1;
                    }
                }
            }
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] > 0) {
                    board[i][j] = 1;
                } else {
                    board[i][j] = 0;
                }
            }
        }

    }
}
