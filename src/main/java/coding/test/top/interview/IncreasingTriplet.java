package coding.test.top.interview;

public class IncreasingTriplet {

    public boolean increasingTriplet(int[] nums) {
        // initialize the minimums with MAX_INT value, and count of increasing subsequence elements observed so far to 0.
        int firstMinimum = Integer.MAX_VALUE, secondMinimum = Integer.MAX_VALUE, count = 0;

        for (int num : nums) {
            if (num < firstMinimum) {
                // set the first minimum
                firstMinimum = num;
                // increase only once for first minimum
                count = (count == 0) ? count + 1 : count;
            } else {
                // we have already found 2, and this is the third one, return true
                if (count == 2 && num > secondMinimum) {
                    return true;
                    // we have found a new second minimum
                } else if (num > firstMinimum && num < secondMinimum) {
                    secondMinimum = num;
                    // increase only once for second minimum
                    count = (count == 1) ? count + 1 : count;
                }
            }
        }
        // there is no increasing triplet subsequence in the given array
        return false;
    }
}
