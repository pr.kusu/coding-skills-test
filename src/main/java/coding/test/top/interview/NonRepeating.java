package coding.test.top.interview;

import java.util.*;

public class NonRepeating {

    private static Map<Integer, Boolean> repeat = new HashMap<>();

    static void countNumbers(List<List<Integer>> arr) {

        for (List<Integer> num : arr) {
            System.out.println(countNonRepeating(num));
        }

    }

    private static int countNonRepeating(List<Integer> range) {
        if (range.size() < 2) {
            return 0;
        }

        int start = range.get(0);
        int end = range.get(1);

        int count = 0;
        for (int i = start; i <= end; i++) {
            boolean rep;
            if (repeat.containsKey(i)) {
                rep = repeat.get(i);
            } else {
                rep = repeats(i);
                repeat.put(i, rep);
            }

            if (!rep) {
                count++;
            }
        }

        return count;
    }

    private static boolean repeats(int n) {
        boolean repeats = false;
        Set<Integer> digits = new HashSet<>();
        if (n % 100 == 0) {
            return true;
        }
        //21
        while (n > 0) {
            int rem = n % 10;
            if (digits.contains(rem)) {
                return true;
            }
            digits.add(rem);
            n = n / 10;
        }
        return false;
    }

    public static void main(String[] args) {
        countNumbers(Arrays.asList(Arrays.asList(1, 20)));
    }
}
