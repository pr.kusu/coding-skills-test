package coding.test.top.interview;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class Surrounded {

    public void solve(char[][] board) {
        if (board.length == 0) {
            return;
        }

        int[][] dir = new int[][]{{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        Queue<Integer> queue = new LinkedList<>();

        for (int i = 0; i < board.length; i++) {
            if (board[i][0] == 'O') {
                queue.add(i * board[0].length);
                board[i][0] = '*';
            }

            if (board[i][board[0].length - 1] == 'O') {
                int j = board[0].length - 1;
                board[i][j] = '*';
                queue.add(i * board[0].length + j);
            }
        }

        for (int i = 0; i < board[0].length; i++) {
            if (board[0][i] == 'O') {
                board[0][i] = '*';
                queue.add(i);
            }

            if (board[board.length - 1][i] == 'O') {
                int j = board.length - 1;
                board[j][i] = '*';
                queue.add(j * (board[0].length) + i);
            }
        }

        Set<Integer> visited = new HashSet<>();

        while (!queue.isEmpty()) {
            int current = queue.poll();
            int i = current / board[0].length;
            int j = current % board[0].length;

            visited.add(current);

            for (int[] d : dir) {
                int x = i + d[0];
                int y = j + d[1];

                int cur = x * board[0].length + y;

                if (!visited.contains(cur)) {
                    if (x >= 0 && y >= 0 && x < board.length && y < board[0].length
                            && board[x][y] == 'O') {
                        board[x][y] = '*';
                        queue.add(cur);
                    }
                }
            }
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == '*') {
                    board[i][j] = 'O';
                } else {
                    board[i][j] = 'X';
                }
            }
        }
    }
}

/**
 * [["O","X","X","O","X"],
 * ["X","O","O","X","O"],
 * ["X","O","X","O","X"],
 * ["O","X","O","O","O"],
 * ["X","X","O","X","O"]]
 * <p>
 * [["O","X","X","O","X"],
 * ["X","X","X","X","O"],
 * ["X","X","X","O","X"],
 * ["O","X","O","O","O"],
 * ["X","X","O","X","O"]]
 * <p>
 * [["O","X","X","O","X"],
 * ["X","X","X","X","O"],
 * ["X","X","X","X","X"],
 * ["O","X","O","O","O"],
 * ["X","X","O","X","O"]]
 * <p>
 * [["O","X","X","O","X"],
 * ["X","X","X","X","O"],
 * ["X","X","X","O","X"],
 * ["O","X","O","O","O"],
 * ["X","X","O","X","O"]]
 * <p>
 * [["O","X","X","O","X"],
 * ["X","X","X","X","O"],
 * ["X","X","X","X","X"],
 * ["O","X","O","O","O"],
 * ["X","X","O","X","O"]]
 * <p>
 * [["O","X","X","O","X"],
 * ["X","X","X","X","O"],
 * ["X","X","X","O","X"],
 * ["O","X","O","O","O"],
 * ["X","X","O","X","O"]]
 * <p>
 * [["O","X","X","O","X"],
 * ["X","X","X","X","O"],
 * ["X","X","X","X","X"],
 * ["O","X","O","O","O"],
 * ["X","X","O","X","O"]]
 */

/**
 * [["O","X","X","O","X"],
 * ["X","X","X","X","O"],
 * ["X","X","X","O","X"],
 * ["O","X","O","O","O"],
 * ["X","X","O","X","O"]]
 */

/**
 * [["O","X","X","O","X"],
 * ["X","X","X","X","O"],
 * ["X","X","X","X","X"],
 * ["O","X","O","O","O"],
 * ["X","X","O","X","O"]]
 */
