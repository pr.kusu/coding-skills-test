package coding.test.top.interview;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class LargestNumber {

    public String largestNumber(int[] nums) {
        PriorityQueue<String> queue = new PriorityQueue<>((o1, o2) -> {
            String s1 = o1 + o2;
            String s2 = o2 + o1;

            return s2.compareTo(s1);
        });


        for (int num : nums) {
            queue.add(Integer.toString(num));
        }

        StringBuilder result = new StringBuilder();
        while (!queue.isEmpty()) {
            result.append(queue.poll());
        }

        if (result.charAt(0) == '0') return "0";

        return result.toString();
    }

    public static String largestNumber2(int[] nums) {

        Comparator<String> comparator = (o1, o2) -> {
            String s1 = o1 + o2;
            String s2 = o2 + o1;

            return s2.compareTo(s1);
        };

        String[] toString = new String[nums.length];
        int i = 0;
        for (int num : nums) {
            toString[i++] = String.valueOf(num);
        }

        Arrays.sort(toString, comparator);
        if (toString[0].equals("0")) {
            return "0";
        }

        StringBuilder builder = new StringBuilder();
        for (String s : toString) {
            builder.append(s);
        }

        return builder.toString();
    }
}
