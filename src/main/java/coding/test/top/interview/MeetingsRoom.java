package coding.test.top.interview;

import java.util.Arrays;
import java.util.Comparator;

public class MeetingsRoom {

    public boolean canAttendMeetings(int[][] intervals) {

        Arrays.sort(intervals, Comparator.comparingInt(o -> o[0]));

        for (int i = 1; i < intervals.length; i++) {
            int prevEnd = intervals[i - 1][1];
            int currentStart = intervals[i][0];

            if (prevEnd > currentStart) {
                return false;
            }
        }

        return true;
    }
}
