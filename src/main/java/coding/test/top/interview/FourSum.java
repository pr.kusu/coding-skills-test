package coding.test.top.interview;

import java.util.*;

public class FourSum {

    public List<List<Integer>> fourSum(int[] nums, int target) {
        Map<Integer, List<List<Integer>>> sumMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                int currentSum = nums[i] + nums[j];

                sumMap.computeIfAbsent(currentSum, k -> new ArrayList<>()).add(Arrays.asList(i, j));
            }
        }

        Set<List<Integer>> resultSet = new HashSet<>();

        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                int currentTarget = target - (nums[i] + nums[j]);

                if (sumMap.containsKey(currentTarget)) {
                    for (List<Integer> complement : sumMap.get(currentTarget)) {
                        if (complement.get(0) > j && complement.get(1) > j) {
                            int k = complement.get(0);
                            int l = complement.get(1);

                            List<Integer> quarduple = Arrays.asList(nums[i], nums[j], nums[k], nums[l]);

                            Collections.sort(quarduple);
                            resultSet.add(quarduple);
                        }
                    }
                }
            }
        }

        return new ArrayList<>(resultSet);
    }
}
