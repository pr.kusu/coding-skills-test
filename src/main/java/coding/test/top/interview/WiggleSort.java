package coding.test.top.interview;

import java.util.Arrays;

public class WiggleSort {

    public void wiggleSort(int[] nums) {
        if (nums.length < 2) {
            return;
        }
        // 1,2,3,4,5,6
        int[] result = new int[nums.length];
        Arrays.sort(nums);

        int mid = (nums.length - 1) / 2;
        int right = nums.length - 1;

        int count = 0;
        while (mid >= 0 || right > (nums.length - 1) / 2) {
            if (count % 2 == 0) {
                result[count] = nums[mid];
                mid--;
            } else {
                result[count] = nums[right];
                right--;
            }
            count++;
        }

        System.arraycopy(result, 0, nums, 0, nums.length);
    }

}
