package coding.test.top.interview;

import java.util.ArrayList;
import java.util.List;

public class PalindromePartition {

    private List<List<String>> result = new ArrayList<>();

    public List<List<String>> partition(String s) {
        partitionPalindrome(result, new ArrayList<>(), s, 0);
        return result;
    }

    private void partitionPalindrome(List<List<String>> result, List<String> temp, String s, int index) {
        if (index >= s.length()) {
            result.add(new ArrayList<>(temp));
            return;
        }

        for (int i = index; i < s.length(); i++) {
            String cur = s.substring(index, i + 1);
            if (isPalindrome(cur)) {
                temp.add(cur);
                partitionPalindrome(result, temp, s, i + 1);
                temp.remove(temp.size() - 1);
            }

        }

    }

    private static boolean isPalindrome(String s) {
        int start = 0;
        int end = s.length() - 1;

        while (start < end) {
            if (s.charAt(start++) != s.charAt(end--)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] ars) {
        System.out.println(isPalindrome("aaa"));
    }
}
