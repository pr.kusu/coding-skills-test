package coding.test.top.interview;

import java.util.*;

public class GraphValidTree {

    public boolean validTree(int n, int[][] edges) {
        if (edges.length != n - 1) {
            return false;
        }

        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }

        for (int[] connection : edges) {
            int E1 = connection[0];
            int E2 = connection[1];

            graph.get(E1).add(E2);
            graph.get(E2).add(E1);
        }

        return dfs(graph, graph.get(0), new HashSet<>(), n);
    }

    private boolean dfs(List<List<Integer>> graph, List<Integer> node, Set<Integer> visited, int n) {

        for (Integer i : node) {
            if (visited.contains(i)) {
                continue;
            }
            visited.add(i);
            dfs(graph, graph.get(i), visited, n);
        }

        return visited.size() == n;
    }

    private boolean bfs(List<List<Integer>> graph, Set<Integer> visited, int n) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(0);
        visited.add(0);


        while (!queue.isEmpty()) {
            int current = queue.poll();

            List<Integer> childs = graph.get(current);

            for (Integer child : childs) {
                if (visited.contains(child)) {
                    continue;
                }
                queue.add(child);
                visited.add(child);
            }
        }

        return visited.size() == n;
    }

}
