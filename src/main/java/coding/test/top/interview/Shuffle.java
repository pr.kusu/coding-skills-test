package coding.test.top.interview;

import java.util.Random;

public class Shuffle {

    int[] original;
    Random random = new Random();

    public int[] shuffle() {

        int[] clone = original.clone();

        for (int i = 0; i < clone.length; i++) {
            int randIndex = rangeRandom(i, clone.length);
            swap(i, randIndex, clone);
        }

        return clone;
    }

    private void swap(int i, int j, int[] arr) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public int[] reset() {
        return original;
    }

    private int rangeRandom(int low, int high) {
        return low + random.nextInt(high - low);
    }
}
