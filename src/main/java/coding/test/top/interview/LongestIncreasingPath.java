package coding.test.top.interview;

public class LongestIncreasingPath {
    /**
     * [[7,8,9],
     * [9,7,6],
     * [7,2,3]]
     * Output: 4
     * Explanation: The longest increasing path is [1, 2, 6, 9].
     *
     * @param matrix
     * @return
     */
    public int longestIncreasingPath(int[][] matrix) {
        if (matrix.length == 0) {
            return 0;
        }

        int[][] visited = new int[matrix.length][matrix[0].length];
        int max = 1;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {

                if (visited[i][j] != 0) {
                    continue;
                }
                int depth = dfs(matrix, i, j, visited);
                max = Math.max(depth, max);
            }
        }

        return max;
    }

    private int dfs(int[][] matrix, int x, int y, int[][] visited) {

        if (visited[x][y] != 0) {
            return visited[x][y];
        }

        int[][] dir = new int[][]{{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
        int max = 1;

        for (int[] direction : dir) {
            int i = x + direction[0];
            int j = y + direction[1];

            if (checkBoundry(i, j, matrix) && matrix[i][j] > matrix[x][y]) {
                int cur = dfs(matrix, i, j, visited) + 1;
                max = Math.max(cur, max);
            }
        }

        visited[x][y] = max;

        return max;
    }

    private boolean checkBoundry(int x, int y, int[][] matrix) {
        return x >= 0 && y >= 0 && x < matrix.length && y < matrix[0].length;
    }
}
