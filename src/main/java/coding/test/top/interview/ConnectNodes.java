package coding.test.top.interview;

import coding.test.leetcode.curated.TreeNode;

public class ConnectNodes {

    public TreeNode connect(TreeNode root) {
        if (root == null) {
            return null;
        }

        if (root.left != null) {
            root.left.next = root.right;

            if (root.next != null) {
                root.right.next = root.next.left;
            }
        }
        connect(root.left);
        connect(root.right);
        return root;
    }

}
