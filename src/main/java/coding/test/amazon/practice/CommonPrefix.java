package coding.test.amazon.practice;

public class CommonPrefix {

    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        String first = strs[0];
        int index = 0;

        while (index < first.length()) {
            for (String s : strs) {
                if (index >= s.length()) {
                    return builder.toString();
                }

                if (s.charAt(index) != first.charAt(index)) {
                    return builder.toString();
                }

            }
            builder.append(first.charAt(index));
            index++;
        }

        return builder.toString();
    }
}
