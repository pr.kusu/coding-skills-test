package coding.test.amazon.practice;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

public class KthLargest {

    public int findKthLargest(int[] nums, int k) {

        Queue<Integer> priorityQueue = new PriorityQueue<>();

        for (int n : nums) {
            if (priorityQueue.size() < k) {
                priorityQueue.add(n);
            } else {
                if (n > priorityQueue.peek()) {
                    priorityQueue.poll();
                    priorityQueue.add(n);
                }
            }

        }


        return priorityQueue.peek();
    }

    public static void main(String[] args) {
        new KthLargest().findKthLargest(new int[]{3, 3, 3, 3, 4, 3, 3, 3, 3}, 1);
    }
}
