package coding.test.amazon.practice;


import coding.test.leetcode.curated.ListNode;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class MergeKSorted {

    public ListNode mergeKLists(ListNode[] lists) {
        if (lists.length == 0) {
            return null;
        }

        Queue<ListNode> priority = new PriorityQueue<>(Comparator.comparingInt(o -> o.val));
        for (ListNode listNode : lists) {
            if (listNode != null) {
                priority.add(listNode);
            }
        }

        ListNode start = new ListNode(0);
        ListNode head = start;
        while (!priority.isEmpty()) {
            ListNode currentMin = priority.poll();
            start.next = currentMin;
            if (currentMin.next != null) {
                priority.add(currentMin.next);
            }
            start = start.next;
        }

        return head.next;
    }
}
