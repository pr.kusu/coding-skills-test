package coding.test.amazon.practice;


import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordLadderII {

    class Pair<K, V> {
        K key;
        V value;

        Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }

        K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }

    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        boolean flag = false;
        List<List<String>> output = new ArrayList<>();
        Map<String, List<String>> map = new HashMap<>();

        for (String word : wordList) {
            if (word.equals(endWord)) flag = true;

            for (int i = 0; i < word.length(); ++i) {
                String key = word.substring(0, i) + "*" + word.substring(i + 1);

                if (!map.containsKey(key)) map.put(key, new ArrayList<>());

                map.get(key).add(word);
            }
        }

        if (!flag) return output;

        flag = false;
        Deque<Pair<String, List<String>>> que = new ArrayDeque<>();
        que.addLast(new Pair<>(beginWord, Collections.singletonList(beginWord)));
        Map<String, Integer> visited = new HashMap<>();
        visited.put(beginWord, 0);
        int count = 0;

        while (!que.isEmpty()) {
            int size = que.size();

            for (int i = 0; i < size; ++i) {
                Pair<String, List<String>> top = que.removeFirst();
                String word = top.getKey();

                for (int j = 0; j < word.length(); ++j) {
                    String key = word.substring(0, j) + "*" + word.substring(j + 1);

                    if (map.containsKey(key)) {
                        for (String next : map.get(key)) {
                            if (visited.containsKey(next) && visited.get(next) != count) continue;

                            List<String> list = new ArrayList<>(top.getValue());
                            list.add(next);

                            if (next.equals(endWord)) {
                                flag = true;
                                output.add(list);
                            } else {
                                visited.put(next, count);
                                que.addLast(new Pair<>(next, list));
                            }
                        }
                    }
                }
            }

            if (flag) break;

            ++count;
        }

        return output;
    }
}
