package coding.test.amazon.practice;

public class CountAndSay {

    public String countAndSay(int n) {
        String s = "1";

        return getNext(1, n, "1");
    }

    private String getNext(int start, int n, String s) {
        if (start == n) {
            return s;
        }

        StringBuilder builder = new StringBuilder(0);

        for (int i = 0; i < s.length(); i++) {
            int freq = 1;
            while (i < s.length() - 1 && s.charAt(i) == s.charAt(i + 1)) {
                freq++;
                i++;
            }
            builder.append(freq).append(s.charAt(i));
        }

        return getNext(start + 1, n, builder.toString());

    }
}
