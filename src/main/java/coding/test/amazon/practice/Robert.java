package coding.test.amazon.practice;

import java.util.Scanner;

public class Robert {

    static class Robot {

        int currentX;
        int currentY;
        int previousX;
        int previousY;

        java.util.Stack<String> moves = new java.util.Stack<String>();

        Robot() {
            this.currentX = 0;
            this.currentY = 5;
            this.previousX = 0;
            this.previousY = 5;
        }

        Robot(int x, int y) {
            this.previousX = 0;
            this.previousY = 5;
            this.currentX = x;
            this.currentY = y;
        }

        void moveX(int dx) {
            this.previousX = this.currentX;
            this.currentX += dx;
            moves.push("x " + dx);
        }

        void moveY(int dy) {
            this.previousY = this.currentY;
            this.currentY += dy;
            moves.push("y " + dy);

        }

        void printCurrentCoordinates() {
            System.out.println(this.currentX + " " + this.currentY);
        }


        void printLastCoordinates() {
            System.out.println(this.previousX + " " + this.previousY);
        }

        void printLastMove() {
            System.out.println(moves.pop());
        }

    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int x = scan.nextInt();
        int y = scan.nextInt();
        int dx = scan.nextInt();
        int dy = scan.nextInt();

        Robot firstRobot = new Robot();
        firstRobot.printCurrentCoordinates();

        Robot secondRobot = new Robot(x, y);
        secondRobot.printCurrentCoordinates();

        for (int i = 1; i < 3; i++) {
            secondRobot.moveX(dx);
            secondRobot.printLastMove();
            secondRobot.printCurrentCoordinates();
            secondRobot.moveY(dy);
            secondRobot.printLastCoordinates();

            dx += i * i;
            dy -= i * i;
        }
    }
}
