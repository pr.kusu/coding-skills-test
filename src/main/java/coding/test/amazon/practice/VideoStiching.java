package coding.test.amazon.practice;

import java.util.Comparator;
import java.util.PriorityQueue;

public class VideoStiching {

    public int videoStitching(int[][] clips, int T) {
        PriorityQueue<int[]> pq = new PriorityQueue<>(Comparator.comparingInt(a -> a[0]));

        for (int[] clip : clips) {
            pq.offer(clip);
        }

        int end = 0;
        int clipsCount = 0;
        while (!pq.isEmpty() && end < T) {
            if (pq.peek()[0] > end) {
                return -1;
            }

            int nextEnd = 0;
            while (!pq.isEmpty() && pq.peek()[0] <= end) {
                nextEnd = Math.max(nextEnd, pq.poll()[1]);
            }

            end = nextEnd;
            clipsCount++;
        }

        return end >= T ? clipsCount : -1;
    }
}
