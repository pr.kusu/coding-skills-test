package coding.test.amazon.practice;

import java.util.*;

public class WordLadder {

    public int ladderLength(String beginWord, String endWord, List<String> wordList) {

        Queue<String> queue = new LinkedList<>();
        Set<String> visited = new HashSet<>(wordList);
        visited.remove(beginWord);
        queue.add(beginWord);

        int length = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            length++;

            while (size > 0) {

                String current = queue.poll();

                if (current.equals(endWord)) {
                    return length;
                }

                for (int j = 0; j < current.length(); j++) {
                    char[] currentChars = current.toCharArray();

                    for (char i = 'a'; i <= 'z'; i++) {

                        currentChars[j] = i;
                        String edited = new String(currentChars);

                        if (visited.contains(edited)) {
                            queue.add(edited);
                            visited.remove(edited);
                        }
                    }
                }

                size--;
            }

        }

        return 0;
    }

    public static void main(String[] args) {

    }
}
