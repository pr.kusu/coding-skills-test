package coding.test.amazon.practice;

import java.util.*;

public class ConcatenatedWords {

    public List<String> findAllConcatenatedWordsInADict(String[] words) {

        List<String> result = new ArrayList<>();
        Set<String> wordSet = new HashSet<>(Arrays.asList(words));

        for (String word : words) {
            if (contains(word, wordSet)) {
                result.add(word);
            }
        }

        return result;
    }

    public boolean contains(String word, Set<String> wordSet) {

        for (int i = 1; i < word.length(); i++) {
            String prefix = word.substring(0, i);
            String suffix = word.substring(i);

            if (wordSet.contains(prefix)) {
                if (wordSet.contains(suffix)) {
                    wordSet.add(word);
                    return true;
                } else {
                    return contains(suffix, wordSet);
                }
            }

        }

        return false;
    }
}
