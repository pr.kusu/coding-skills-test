package coding.test.amazon.practice;


import coding.test.leetcode.curated.TreeNode;

import java.util.*;

public class KDistanceTree {

    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        if (root == null || target == null) {
            return Collections.emptyList();
        }
        if (K == 0) {
            return Collections.singletonList(target.val);
        }

        List<Integer> result = new ArrayList<>();
        Map<TreeNode, Integer> distances = new HashMap<>();
        calculateDistance(root, target, distances);
        dfs(root, result, distances, distances.get(root), K);

        return result;

    }

    private void dfs(TreeNode root, List<Integer> result,
                     Map<TreeNode, Integer> distance, int depth, int K) {

        if (root == null) {
            return;
        }

        if (distance.containsKey(root)) {
            depth = distance.get(root);
        }
        if (depth == K) {
            result.add(root.val);
        }

        dfs(root.left, result, distance, depth + 1, K);
        dfs(root.right, result, distance, depth + 1, K);
    }

    private int calculateDistance(TreeNode root, TreeNode target,
                                  Map<TreeNode, Integer> distance) {
        if (root == target) {
            distance.put(root, 0);
            return 0;
        }
        if (root == null) {
            return -1;
        }

        int left = calculateDistance(root.left, target, distance);
        int right = calculateDistance(root.right, target, distance);

        if (left >= 0) {
            distance.put(root, left + 1);
            return left + 1;
        }
        if (right >= 0) {
            distance.put(root, right + 1);
            return right + 1;
        }

        return -1;
    }

}
