package coding.test.amazon.practice;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LRUCache {

    private final Map<Integer, Integer> data = new HashMap<>();
    private final Deque<Integer> queue = new LinkedList<>();
    int capacity;
    int size;

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    public int get(int key) {
        Integer val = data.get(key);

        if (val == null) {
            return -1;
        }

        queue.remove(key);
        queue.addLast(key);
        return val;
    }

    public void put(int key, int value) {
        if (!data.containsKey(key)) {
            size++;
        } else {
            queue.remove(key);
        }

        queue.addLast(key);
        data.put(key, value);


        if (size > capacity) {
            int first = queue.removeFirst();
            data.remove(first);
            size--;
        }

    }

    public static void main(String[] args) {
        LRUCache cache = new LRUCache(2);
        cache.put(2, 1);
        cache.put(3, 2);
        System.out.println(cache.get(3));
        System.out.println(cache.get(2));
        cache.put(4, 3);
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));
        System.out.println(cache.get(4));

    }
}
