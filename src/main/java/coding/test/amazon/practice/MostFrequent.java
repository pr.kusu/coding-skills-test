package coding.test.amazon.practice;

import java.util.*;

public class MostFrequent {

    public static String mostCommonWord(String paragraph, String[] banned) {
        if (paragraph == null || paragraph.isEmpty()) {
            return null;
        }

        Set<String> bannedSet = new HashSet<>(Arrays.asList(banned));

        Map<String, Integer> freq = new HashMap<>();

        String[] words = paragraph.split("[?.!,\\s]");
        String word = null;
        int maxFreq = 0;

        for (String s : words) {
            String current = s.trim();

            if (!bannedSet.contains(current)) {
                freq.put(current, freq.getOrDefault(current, 0) + 1);
                int currentFreq = freq.get(current);

                if (currentFreq > maxFreq) {
                    word = current;
                    maxFreq = currentFreq;
                }

            }
        }

        return word;
    }

    public static void main(String[] args) {
        mostCommonWord("Bob. hIt, baLl", new String[0]);
    }
}
