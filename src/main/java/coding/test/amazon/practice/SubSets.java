package coding.test.amazon.practice;

import java.util.ArrayList;
import java.util.List;

public class SubSets {

    private List<List<Integer>> result = new ArrayList<>();

    public List<List<Integer>> subsets(int[] nums) {

        findSubs(new ArrayList<>(), nums, 0);

        return result;
    }

    private void findSubs(List<Integer> tempSet, int[] nums, int start) {
        result.add(new ArrayList<>(tempSet));

        for (int i = start; i < nums.length; i++) {
            tempSet.add(nums[i]);
            findSubs(tempSet, nums, i + 1);

            tempSet.remove(tempSet.size() - 1);
        }
    }
}
