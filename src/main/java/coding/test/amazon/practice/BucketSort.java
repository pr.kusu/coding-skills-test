package coding.test.amazon.practice;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BucketSort {

    public static int firstMissingPositive(int[] arr) {
        int i = 0;
        while (i < arr.length) {
            if (arr[i] <= 0) {
                i++;
            } else if (arr[arr[i] - 1] != arr[i]) {
                swap(arr, arr[i] - 1, i);
            } else {
                i++;
            }
        }

        int missing = 0;
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] != j + 1) {
                missing = j + 1;
                break;
            }
        }

        return missing;
    }

    private static void swap(int[] A, int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }

    public static void main(String[] args) {

        List<Integer> list = IntStream.range(-10, 11).boxed().collect(Collectors.toList());
        Collections.shuffle(list);

        firstMissingPositive(new int[]{-1, -9, -7, -2, 1, 7, 8, 9, 10, 2, 4, 3});
    }
}
