package coding.test.amazon.practice;

import java.util.PriorityQueue;

public class KNearest {

    public int[][] kClosest(int[][] points, int K) {

        PriorityQueue<int[]> priorityQueue = new PriorityQueue<>((o1, o2) -> {
            int d1 = o1[0] * o1[0] + o1[1] * o1[1];
            int d2 = o2[0] * o2[0] + o2[1] * o2[1];
            return Integer.compare(d2, d1);
        });

        int count = 0;
        for (int[] point : points) {
            priorityQueue.add(point);

            if (count >= K) {
                priorityQueue.poll();
            } else {
                count++;
            }

        }

        int[][] result = new int[K][];

        for (int i = 0; i < K; i++) {
            result[i] = priorityQueue.poll();
        }

        return result;
    }

    public static void main(String[] args) {
        new KNearest().kClosest(new int[][]{{1, 3}, {-2, 2}}, 1);
    }
}
