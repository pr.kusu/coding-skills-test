package coding.test.amazon.practice;

public class InsertFBefore {

    private static String insertKBefore(String s) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = -1; i < s.length() - 1; i++) {
            if (s.charAt(i + 1) == 'F' || s.charAt(i + 1) == 'f') {
                stringBuilder.append("K");
                stringBuilder.append(s.charAt(i + 1));
            } else {
                stringBuilder.append(s.charAt(i + 1));
            }

        }

        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        String str = insertKBefore("forceforce");
        System.out.println(str);
    }
}
