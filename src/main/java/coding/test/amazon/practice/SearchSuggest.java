package coding.test.amazon.practice;

import java.util.*;

public class SearchSuggest {

    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        Comparator<String> comparator = String::compareTo;
        Arrays.sort(products, comparator);

        List<List<String>> result = new ArrayList<>();

        for (int i = 1; i < searchWord.length(); i++) {
            List<String> tempList = new ArrayList<>();

            String prefix = searchWord.substring(0, i);

            for (String product : products) {
                if (product.startsWith(prefix) && tempList.size() < 3) {
                    tempList.add(product);
                }
            }

            result.add(tempList);
        }

        return result;
    }
}
