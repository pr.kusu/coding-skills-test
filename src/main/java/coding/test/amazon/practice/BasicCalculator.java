package coding.test.amazon.practice;

import java.util.ArrayDeque;
import java.util.Deque;

public class BasicCalculator {

    //"31+2+2*3"


    public int calculate(String s) {

        char[] expr = (s + '+').toCharArray();

        Deque<Integer> stack = new ArrayDeque<>();
        char op = '+';
        int num = 0;

        for (int i = 0; i < expr.length; i++) {
            char curr = expr[i];

            if (curr == ' ') {
                continue;
            }

            if (Character.isDigit(curr)) {
                num = (num * 10) + curr - '0';

            } else {
                if (op == '+') {
                    stack.push(num);
                } else if (op == '-') {
                    stack.push(-num);
                } else if (op == '*') {
                    stack.push(num * stack.pop());
                } else if (op == '/') {
                    stack.push(stack.pop() / num);
                }

                num = 0;
                op = curr;
            }
        }

        return stack.stream().mapToInt(i -> i).sum();
    }

    public static void main(String[] args) {
        int res = new BasicCalculator().calculate("2+3+4*2");
        System.out.println(res);
    }
}
