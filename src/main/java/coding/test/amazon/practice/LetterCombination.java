package coding.test.amazon.practice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LetterCombination {

    public List<String> letterCombinations(String digits) {
        if (digits == null || digits.isEmpty()) {
            return Collections.emptyList();
        }

        Map<String, List<String>> map = new HashMap<>();
        char start = 'a';

        for (int i = 2; i <= 9; i++) {
            map.put(Integer.toString(i), new ArrayList<>());
            int j;

            int end = i == 7 || i == 9 ? 4 : 3;

            for (j = 0; j < end; j++) {
                char next = (char) (start + j);

                map.get(Integer.toString(i)).add(Character.toString(next));
            }
            start = (char) (start + j);

        }

        return dfs(digits, map);
    }

    private List<String> dfs(String digit, Map<String, List<String>> memo) {
        if (memo.containsKey(digit)) {
            return memo.get(digit);
        }
        List<String> result = new ArrayList<>();

        List<String> right = dfs(digit.substring(1), memo);
        List<String> left = memo.get(Character.toString(digit.charAt(0)));

        for (String l : left) {
            for (String r : right) {
                result.add(l + r);
            }
        }

        memo.put(digit, result);

        return result;

    }
}
