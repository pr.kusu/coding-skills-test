package coding.test.amazon.practice;

import java.util.*;

public class CriticalConnection {

    private int count = 0;

    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {

        List<List<Integer>> graph = new ArrayList<>();

        for (int i = 0; i < connections.size(); i++) {
            graph.add(new ArrayList<>());
        }

        for (List<Integer> conn : connections) {
            int E1 = conn.get(0);
            int E2 = conn.get(1);

            graph.get(E1).add(E2);
            graph.get(E2).add(E1);
        }

        List<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i < connections.size(); i++) {
            count = 0;

            int x = connections.get(i).get(0);
            int y = connections.get(i).get(1);

            dfs(graph, y, x, y, new HashSet<>());


            if (count < n) {
                result.add(Arrays.asList(x, y));
            }
        }

        return result;

    }

    private void dfs(List<List<Integer>> graph, Integer start, int x, int y, Set<Integer> visited) {
        if (visited.contains(start)) {
            return;
        }

        count++;
        visited.add(start);
        List<Integer> children = graph.get(start);

        for (Integer child : children) {
            if ((start == x && child == y) || (start == y && child == x)) {

            } else {

                dfs(graph, child, x, y, visited);
            }
        }

    }
}
