package coding.test.amazon.practice;


import java.util.HashMap;
import java.util.Map;

public class DeepCopy {

    class Node {
        int val;
        Node next;
        Node random;

        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }
    }

    public Node copyRandomList(Node head) {
        if (head == null) {
            return null;
        }
        Map<Integer, Node> randomMap = new HashMap<>();
        Map<Node, Integer> randomTracker = new HashMap<>();
        Node clone = new Node(head.val);
        Node cloneStart = clone;

        Node start = head;
        int i = 0;
        while (start != null) {
            randomMap.put(i, cloneStart);
            randomTracker.put(start, i);
            i++;

            Node next = start.next;
            if (next != null) {
                cloneStart.next = new Node(next.val);
            }
            start = start.next;
            cloneStart = cloneStart.next;
        }

        start = head;
        cloneStart = clone;

        while (start != null) {
            if (start.random != null) {
                Integer randomIndex = randomTracker.get(start.random);
                cloneStart.random = randomMap.get(randomIndex);
            }
            start = start.next;
            cloneStart = cloneStart.next;

        }

        return clone;
    }
}
