package coding.test.amazon.practice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class CourseScheduleII {

    //Input: 4, [[1,0],[2,0],[3,1],[3,2]]
    //Output: [0,1,2,3] or [0,2,1,3]

    public int[] findOrder(int numCourses, int[][] prerequisites) {

        int[] indegree = new int[numCourses];

        Map<Integer, List<Integer>> adj = new HashMap<>();

        for (int[] prereq : prerequisites) {
            int course = prereq[0];
            int pre = prereq[1];
            indegree[course]++;

            if (!adj.containsKey(pre)) {
                adj.put(pre, new ArrayList<>());
            }
            adj.get(pre).add(course);
        }

        Set<Integer> visited = new HashSet<>();
        Queue<Integer> queue = new LinkedList<>();

        int count = 0;
        int[] result = new int[numCourses];

        for (int i = 0; i < indegree.length; i++) {
            if (indegree[i] == 0) {
                queue.add(i);
                visited.add(i);
                result[count++] = i;
            }
        }


        while (!queue.isEmpty()) {

            int current = queue.poll();

            List<Integer> courses = adj.getOrDefault(current, Collections.emptyList());

            for (Integer course : courses) {
                indegree[course]--;

                if (indegree[course] == 0 && !visited.contains(course)) {
                    queue.add(course);
                    visited.add(course);
                    result[count++] = course;
                }
            }

        }

        return count == numCourses ? result : new int[0];
    }

    public static void main(String[] args) {

        int[][] preq = new int[][]{{3, 0}, {0, 1}};

        new CourseScheduleII().findOrder(4, preq);

    }
}
