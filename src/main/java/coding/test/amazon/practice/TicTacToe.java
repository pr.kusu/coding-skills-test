package coding.test.amazon.practice;

public class TicTacToe {

    char[][] board;

    /**
     * Initialize your data structure here.
     */
    public TicTacToe(int n) {
        board = new char[n][n];
    }

    /**
     * Player {player} makes a move at ({row}, {col}).
     *
     * @param row    The row of the board.
     * @param col    The column of the board.
     * @param player The player, can be either 1 or 2.
     * @return The current winning condition, can be either:
     * 0: No one wins.
     * 1: Player 1 wins.
     * 2: Player 2 wins.
     */
    public int move(int row, int col, int player) {
        if (player == 1) {
            board[row][col] = 'X';
            if (checkGame('X', board, row, col)) {
                return 1;
            }
        } else {
            board[row][col] = '0';
            if (checkGame('0', board, row, col)) {
                return 2;
            }
        }
        return 0;
    }

    private boolean checkGame(char player, char[][] board, int x, int y) {

        int row = x;
        int col = 0;
        boolean rowComplete = true;
        boolean colComplete = true;
        boolean diagonal = true;
        while (col < board.length) {
            if (board[row][col++] != player) {
                rowComplete = false;
                break;
            }
        }

        col = y;
        row = 0;
        while (row < board.length) {
            if (board[row++][col] != player) {
                colComplete = false;
                break;
            }
        }

        if (x == y) {
            x = 0;
            y = 0;
            while (x < board.length && y < board.length) {
                if (board[x++] != board[y++]) {
                    diagonal = false;
                    break;
                }
            }
        } else if (x + y == board.length) {
            x = 0;
            y = board.length;
            while (x < board.length && y >= 0) {
                if (board[x++] != board[y--]) {
                    diagonal = false;
                    break;
                }
            }
        } else {
            diagonal = false;
        }

        return rowComplete || colComplete || diagonal;
    }
}
