package coding.test.amazon.practice;

import coding.test.leetcode.curated.Trie;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class WordBreak {

    public static boolean wordBreak(String s, List<String> wordDict) {

        Trie trie = new Trie();

        for (String str : wordDict) {
            trie.insert(str);
        }

        int rem = s.length();
        int i = 1;

        Queue<Integer> start = new LinkedList<>();
        start.add(0);

        while (!start.isEmpty()) {
            int nextIndex = start.poll();

            for (int j = nextIndex; j <= s.length(); j++) {
                String cur = s.substring(nextIndex, j);

                if (trie.search(cur)) {
                    start.add(j);

                    if (j == s.length()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static void main(String[] args) {
        System.out.println(wordBreak("leetcode", Arrays.asList("leet", "code")));
    }
}
