package coding.test.amazon.practice;

import java.util.*;

class RandomizedSet {

    Random random = new Random();
    Map<Integer, Integer> valueMap = new HashMap<>();
    Map<Integer, Integer> indexMap = new HashMap<>();
    int size = 0;

    public RandomizedSet() {

    }

    /**
     * Inserts a value to the set. Returns true if the set did not already contain the specified element.
     */
    public boolean insert(int val) {
        boolean res = valueMap.containsKey(val);
        if (!res) {
            indexMap.put(size, val);
            valueMap.put(val, size);
            size++;
            return true;
        }
        return false;
    }

    /**
     * Removes a value from the set. Returns true if the set contained the specified element.
     */
    public boolean remove(int val) {
        Integer index = valueMap.get(val);
        if (index == null) {
            return false;
        }

        valueMap.remove(val);
        int lastVal = indexMap.get(size - 1);
        indexMap.remove(size);

        if (index < size - 1) {
            indexMap.put(index, lastVal);
            valueMap.put(lastVal, index);

        }
        size--;
        return true;
    }

    /**
     * Get a random element from the set.
     */
    public int getRandom() {
        int rand = random.nextInt(valueMap.size());
        return indexMap.get(rand);
    }
}
