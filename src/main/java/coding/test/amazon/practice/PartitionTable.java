package coding.test.amazon.practice;

import java.util.ArrayList;
import java.util.List;

public class PartitionTable {

    //Input: S = "ababcbacadefegdehijhklij"
    //Output: [9,7,8]

    public List<Integer> partitionLabels2(String s) {
        int[] lastIndex = new int[26];

        for (int i = 0; i < s.length(); i++) {
            lastIndex[s.charAt(i) - 'a'] = i;
        }

        List<Integer> result = new ArrayList<>();
        int start = 0;
        int anchor = 0;
        for (int i = 0; i < s.length(); i++) {
            start = Math.max(start, lastIndex[s.charAt(i) - 'a']);

            if (start == i) {
                result.add(i - anchor + 1);
                anchor = i + 1;
            }
        }

        return result;
    }


    public List<Integer> partitionLabels(String s) {
        int[] charMap = new int[26];

        for (int i = 0; i < s.length(); i++) {
            charMap[s.charAt(i) - 'a']++;
        }
        List<Integer> result = new ArrayList<>();

        int start = 0;
        for (int i = 1; i < s.length(); i++) {
            charMap[s.charAt(i) - 'a']--;
            String prefix = s.substring(0, i);

            if (!contains(prefix, charMap)) {
                result.add(i - start);
                start = i + 1;
            }
        }

        return result;

    }

    private boolean contains(String s, int[] charMap) {
        for (int i = 0; i < s.length(); i++) {
            if (charMap[s.charAt(i) - 'a'] > 0) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        new PartitionTable().partitionLabels("ababcbacadefegdehijhklij");
    }
}
