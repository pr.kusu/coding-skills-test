package coding.test.amazon.practice;

import coding.test.leetcode.curated.TreeNode;

import java.util.*;

public class PreOrder {

    public List<Integer> preorderTraversal(TreeNode root) {

        List<Integer> result = new ArrayList<>();
        TreeNode start = root;
        Deque<TreeNode> stack = new ArrayDeque<>();

        while (!stack.isEmpty() || start != null) {
            while (start != null) {
                result.add(start.val);
                stack.push(start);
                start = start.left;
            }
            TreeNode prev = stack.pop();
            start = prev.right;
        }


        return result;
    }
}
