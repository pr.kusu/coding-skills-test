package coding.test.amazon.practice;

/**
 * https://leetcode.com/problems/find-peak-element/
 */
public class FindPeak {

    public int findPeakElement(int[] nums) {

        return binarySearch(0, nums.length - 1, nums);
    }

    private int binarySearch(int l, int r, int[] nums) {
        if (l == r) {
            return l;
        }
        int mid = (l + r) / 2;

        if (nums[mid] > nums[mid + 1]) {
            return binarySearch(l, mid, nums);
        }

        return binarySearch(mid + 1, r, nums);
    }
}
