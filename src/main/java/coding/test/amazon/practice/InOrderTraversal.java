package coding.test.amazon.practice;

import coding.test.leetcode.curated.TreeNode;

import java.util.*;

public class InOrderTraversal {

    public List<Integer> inorderTraversal2(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        }

        Deque<TreeNode> deque = new ArrayDeque<>();
        List<Integer> result = new ArrayList<>();

        TreeNode start = root;

        while (!deque.isEmpty() || start != null) {

            while (start != null) {
                deque.push(start);
                start = start.left;
            }

            start = deque.pop();
            result.add(start.val);
            start = start.right;
        }

        return result;
    }

    public static void main(String[] args) {
        TreeNode rt = new TreeNode(1);
        rt.right = new TreeNode(2);
        rt.right.left = new TreeNode(3);

        new InOrderTraversal().inorderTraversal2(rt);
    }


    public List<Integer> inorderTraversal(TreeNode root) {

        return helper(root, new ArrayList<>());
    }

    private List<Integer> helper(TreeNode root, List<Integer> ans) {
        if (root == null) {
            return ans;
        }
        helper(root.left, ans);
        ans.add(root.val);
        helper(root.right, ans);

        return ans;
    }
}
