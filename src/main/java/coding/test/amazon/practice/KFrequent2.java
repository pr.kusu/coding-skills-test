package coding.test.amazon.practice;

import java.util.*;

public class KFrequent2 {

    private Map<Integer, TreeSet<String>> map = new HashMap<>();

    public List<String> topKFrequent(String[] words, int k) {
        if (words.length == 0) {
            return Collections.emptyList();
        }

        Map<String, Integer> freqTable = new HashMap<>();

        for (String s : words) {
            freqTable.put(s, freqTable.getOrDefault(s, 0) + 1);
        }

        Set<Integer> dupCheck = new HashSet<>();
        for (Map.Entry<String, Integer> entry : freqTable.entrySet()) {
            String word = entry.getKey();
            Integer freq = entry.getValue();
            dupCheck.add(freq);
            if (!map.containsKey(freq)) {
                map.put(freq, new TreeSet<>());
            }
            map.get(freq).add(word);
        }

        List<Integer> freqList = new ArrayList<>(dupCheck);
        Collections.sort(freqList, Collections.reverseOrder());

        List<String> result = new ArrayList<>();

        int count = 0;
        for (int i = 0; i < k; i++) {

            Set<String> currentList = map.get(freqList.get(i));

            for (String s : currentList) {
                result.add(s);
                count++;

                if (count >= k) {
                    return result;
                }
            }
        }

        return result;
    }


}
