package coding.test.amazon.practice;

public class GasStation {

    public int canCompleteCircuit(int[] gas, int[] cost) {

        for (int i = 0; i < gas.length; i++) {
            if (canComplete(i, gas, cost)) {
                return i + 1;
            }
        }

        return -1;
    }

    private boolean canComplete(int index, int[] gas, int[] cost) {
        int rem = 0;
        int prevCost = 0;

        for (int i = 0; i <= gas.length; i++) {
            int next = (index + i);
            if (next >= gas.length) {
                next = next % gas.length;
            }
            rem -= prevCost;

            if (rem <= 0) {
                return false;
            }
            rem += gas[next];
            prevCost = cost[next];
        }

        return true;
    }

    public static void main(String[] args) {
        new GasStation().canCompleteCircuit(new int[]{3, 4, 4}, new int[]{3, 3, 4});
    }
}
