package coding.test.amazon.practice;


import coding.test.leetcode.curated.TreeNode;

import java.util.*;

public class ZigZagTree {

    public List<List<Integer>> zigzagLevelOrder2(TreeNode root) {

        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }

        boolean order = true;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        int depth = 0;
        while (!queue.isEmpty()) {
            result.add(new ArrayList<>());

            for (int i = queue.size(); i > 0; i--) {
                TreeNode current = queue.poll();

                if (order) {
                    result.get(depth).add(current.val);
                } else {
                    result.get(depth).add(0, current.val);
                }

                if (root.left != null) {
                    queue.add(root.left);
                }
                if (root.right != null) {
                    queue.add(root.right);
                }
            }
            order = !order;
        }

        return result;
    }

    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {

        List<List<Integer>> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        int dir = 0;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            result.add(new ArrayList<>());

            Stack<TreeNode> stack = new Stack<>();

            for (int i = queue.size(); i > 0; i--) {
                TreeNode current = queue.poll();
                result.get(dir).add(current.val);

                if (dir % 2 == 0) {
                    if (current.left != null) {
                        stack.add(current.left);
                    }
                    if (current.right != null) {
                        stack.add(current.right);
                    }
                } else {
                    if (current.right != null) {
                        stack.add(current.right);
                    }
                    if (current.left != null) {
                        stack.add(current.left);
                    }
                }
            }

            while (!stack.isEmpty()) {
                queue.add(stack.pop());
            }

            dir++;
        }

        return result;
    }


}
