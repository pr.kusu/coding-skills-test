package coding.test.amazon.practice;

import java.util.LinkedList;
import java.util.Queue;

public class WallAndGates {

    public void wallsAndGates(int[][] rooms) {
        Queue<Integer> queue = new LinkedList<>();

        for (int i = 0; i < rooms.length; i++) {
            for (int j = 0; j < rooms[0].length; j++) {
                if (rooms[i][j] == 0) {
                    int val = i * rooms[0].length + j;
                    queue.add(val);
                }
            }
        }

        bfs(queue, rooms);
    }

    private void bfs(Queue<Integer> queue, int[][] rooms) {
        int[][] dir = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

        while (!queue.isEmpty()) {
            int current = queue.poll();
            int x = current / rooms[0].length;
            int y = current % rooms[0].length;

            for (int[] d : dir) {
                int newx = x + d[0];
                int newy = y + d[1];

                if (newx >= 0 && newy >= 0 && newx < rooms.length && newy < rooms[0].length) {

                    if (rooms[newx][newy] == Integer.MAX_VALUE) {
                        rooms[newx][newy] = rooms[x][y] + 1;
                        queue.add(newx * rooms[0].length + newy);
                    }
                }
            }
        }
    }
}
