package coding.test.amazon.practice;

public class LongestPalindrome {


    public String longestPalindrome(String s) {

        int start = 0;
        int end = 0;
        int max = 0;

        for (int i = 0; i < s.length(); i++) {

            int len1 = length(i, i, s);
            int len2 = length(i, i + 1, s);

            int len = Math.max(len1, len2);

            if (len > max) {
                start = i - (len - 1) / 2;
                end = i + len / 2;
                max = len;
            }
        }

        return s.substring(start, end + 1);
    }

    private int length(int start, int end, String s) {
        while (start >= 0 && end < s.length() && s.charAt(start) == s.charAt(end)) {
            start--;
            end++;
        }

        return end - start - 1;
    }

    public static void main(String[] args) {
        String str = new LongestPalindrome().longestPalindrome("cbbd");

        System.out.println(str);
    }
}
