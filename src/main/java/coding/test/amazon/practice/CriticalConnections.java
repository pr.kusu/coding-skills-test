package coding.test.amazon.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CriticalConnections {

    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < connections.size(); i++) {
            graph.add(new ArrayList<>());
        }

        for (List<Integer> connection : connections) {
            int E1 = connection.get(0);
            int E2 = connection.get(1);

            graph.get(E1).add(E2);
            graph.get(E2).add(E1);
        }


        List<List<Integer>> result = new ArrayList<>();
        int i = 0;

        for (List<Integer> edge : graph) {
            for (Integer conn : edge) {
                int[] count = new int[1];

                int x = edge.get(i);
                int y = conn;

                dfs(graph, edge, new HashSet<>(), count, x, y);

                if (count[0] < n) {
                    result.add(Arrays.asList(x, y));
                }

            }

            i++;

        }

        return result;

    }

    private void dfs(List<List<Integer>> connections, List<Integer> edges, Set<Integer> visited, int[] count, int x, int y) {

        for (Integer values : edges) {

            List<Integer> edge = connections.get(values);

            if (edge.get(0) == x && edge.get(1) == y) {

            }

            if (visited.contains(values)) {
                continue;
            } else {
                visited.add(values);
                count[0]++;


            }
        }
    }
}
