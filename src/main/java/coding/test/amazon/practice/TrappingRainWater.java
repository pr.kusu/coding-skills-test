package coding.test.amazon.practice;

public class TrappingRainWater {

    public int trap2(int[] height) {
        if (height.length == 0) {
            return 0;
        }

        int result = 0;
        int size = height.length;
        int[] leftMax = new int[size];
        int[] rightMax = new int[size];

        leftMax[0] = height[0];
        rightMax[size - 1] = height[size - 1];

        int i = 1;
        int j = size - 1;

        while (i < size) {
            leftMax[i] = Math.max(leftMax[i - 1], height[i]);
            rightMax[j] = Math.max(rightMax[j + 1], height[j]);
            i--;
            j++;
        }

        for (i = 1; i < height.length; i++) {
            result += Math.min(leftMax[i], rightMax[i]) - height[i];
        }

        return result;
    }

    public int trap(int[] height) {

        int result = 0;

        for (int i = 0; i < height.length; i++) {
            int leftMax = 0;
            int rightMax = 0;
            int j = i;

            while (j < height.length) {
                leftMax = Math.max(leftMax, height[j]);
                rightMax = Math.max(rightMax, height[j]);
                j++;
            }


            int cur = Math.min(leftMax, rightMax) - height[i];
            if (cur > 0) {
                result += cur;
            }
        }

        return result;
    }
}
