package coding.test.amazon.practice;

public class FirstAndLast {

    public int[] searchRange(int[] nums, int target) {
        int index = binarySearch(nums, target);

        if (index == -1) {
            return new int[]{-1, -1};
        }

        int start = index;
        int end = start;

        while (start >= 0 && nums[start] == target) {
            start--;
        }
        while (end < nums.length && nums[end] == target) {
            end++;
        }

        return new int[]{start + 1, end - 1};
    }

    private int binarySearch(int[] nums, int target) {
        int low = 0;
        int high = nums.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (nums[mid] == target) {
                return mid;
            }

            if (nums[mid] > target) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }

        return -1;
    }
}
