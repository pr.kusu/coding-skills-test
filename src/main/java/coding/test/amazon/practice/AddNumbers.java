package coding.test.amazon.practice;

import coding.test.leetcode.curated.ListNode;

public class AddNumbers {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) {
            return null;
        }
        if (l1 == null) {
            return reverse(l2);
        }
        if (l2 == null) {
            return reverse(l1);
        }

        ListNode first = l1;
        ListNode second = l2;

        ListNode result = new ListNode(0);
        ListNode finalResult = result;
        int carry = 0;
        while (first != null || second != null) {
            int firstVal = first != null ? first.val : 0;
            int secondVal = second != null ? second.val : 0;
            int sum = firstVal + secondVal + carry;
            carry = sum / 10;
            sum = sum % 10;
            result.next = new ListNode(sum);
            result = result.next;
            first = first != null ? first.next : null;
            second = second != null ? second.next : null;
        }
        if (carry > 0) {
            result.next = new ListNode(carry);
        }

        return finalResult.next;
    }

    public ListNode reverse(ListNode head) {

        ListNode prev = null;

        while (head != null) {
            ListNode tmp = head.next;
            head.next = prev;
            prev = head;
            head = tmp;
        }

        return prev;
    }
}
