package coding.test.amazon.practice;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

class LFUCache {

    long tracker = 0;

    class Node {
        int freq;
        int key;
        int val;
        long timeStamp;

        public Node(int key, int val) {
            this.freq = 0;
            this.key = key;
            this.val = val;
            this.timeStamp = tracker++;
        }

        public void incrementTime() {
            this.timeStamp = tracker++;
        }
    }

    int size = 0;
    int capacity = 0;
    Map<Integer, Node> map = new HashMap<>();

    PriorityQueue<Node> queue = new PriorityQueue<>((o1, o2) -> {
        if (o1.freq == o2.freq) {
            return Long.compare(o1.timeStamp, o2.timeStamp);
        } else {
            return Integer.compare(o1.freq, o2.freq);
        }
    });

    public LFUCache(int capacity) {
        this.capacity = capacity;
    }

    public int get(int key) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            queue.remove(node);
            node.freq++;
            node.incrementTime();
            queue.add(node);
            return node.val;
        }

        return -1;
    }

    public void put(int key, int value) {
        if (capacity == 0) {
            return;
        }
        if (map.containsKey(key)) {
            Node node = map.get(key);
            queue.remove(node);
            node.freq++;
            node.incrementTime();
            node.val = value;
            map.put(key, node);
            queue.add(node);
        } else {
            if (size >= capacity) {
                Node leastFreq = queue.poll();
                map.remove(leastFreq.key);
                size--;
            }

            size++;

            Node node = new Node(key, value);
            map.put(key, node);
            queue.add(node);
        }

    }
}

/**
 * Your LFUCache object will be instantiated and called as such:
 * LFUCache obj = new LFUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */