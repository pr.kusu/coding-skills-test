package coding.test.amazon.practice;

import java.util.*;

public class LogMover {

    public String[] reorderLogFiles(String[] logs) {
        List<String> result = new LinkedList<>();
        List<String> letter = new ArrayList<>();

        Comparator<String> comparator = (o1, o2) -> {
            int space1 = o1.indexOf(' ');
            int space2 = o2.indexOf(' ');
            int lex = o2.substring(space2 + 1).compareTo(o1.substring(space1 + 1));

            if (lex == 0) {
                String id1 = o1.substring(0, space1);
                String id2 = o2.substring(0, space2);
                return id2.compareTo(id1);
            }

            return lex;
        };

        for (String s : logs) {
            if (Character.isDigit(s.charAt(s.length() - 1))) {
                result.add(s);
            } else {
                letter.add(s);
            }
        }

        letter.sort(comparator);
        String[] results = new String[logs.length];

        int i = letter.size() - 1;
        int j = 0;

        while (i >= 0) {
            results[j++] = letter.get(i--);
        }

        for (String s : result) {
            results[j++] = s;
        }

        return results;
    }
}
