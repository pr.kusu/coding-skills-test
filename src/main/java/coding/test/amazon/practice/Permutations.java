package coding.test.amazon.practice;

import java.util.*;

public class Permutations {

    private final List<List<Integer>> result = new ArrayList<>();

    public List<List<Integer>> permute(int[] nums) {
        permutate(new LinkedList<>(), nums, new HashSet<>());

        return result;
    }

    private void permutate(LinkedList<Integer> tempList, int[] nums, Set<Integer> hash) {
        if (tempList.size() == nums.length) {
            result.add(new ArrayList<>(tempList));
        }

        for (int num : nums) {
            if (!hash.contains(num)) {
                tempList.add(num);
                hash.add(num);
                permutate(tempList, nums, hash);
                tempList.removeLast();
            }
        }
    }
}
