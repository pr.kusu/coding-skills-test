package coding.test.amazon.practice;

public class Sqrt {

    public int mySqrt(int x) {

        long ret = (int) binarySearch(x, x, 1);

        if (ret * ret > x) {
            ret = ret - 1;
        }

        return (int) ret;
    }

    private long binarySearch(long x, long high, long low) {

        long mid = (high + low) / 2;

        if (low >= high) {
            return low;
        }

        if (mid * mid == x) {
            return mid;
        }

        if (mid * mid > x) {
            return binarySearch(x, mid - 1, low);
        }

        return binarySearch(x, high, mid + 1);

    }
}
