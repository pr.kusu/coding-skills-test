package coding.test.amazon.practice;

public class MyPower {

    public double myPow(double x, int n) {
        double ret = 1;

        boolean neg = n < 0;
        int total = neg ? n : -n;

        while (total < 0) {
            double num = x;
            int count = 1;
            int rem = total;

            while (-count >= rem && count <= Integer.MAX_VALUE / 2) {
                count *= 2;
                if (-count >= rem) {
                    num *= num;
                }
            }
            total += count / 2;

            if (neg) {
                ret = ret / num;
            } else {
                ret = ret * num;
            }
        }

        return ret;
    }

    public static void main(String[] args) {
        new MyPower().myPow(2,
                1024);
    }
}
