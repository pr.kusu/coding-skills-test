package coding.test.amazon.practice;

import coding.test.leetcode.curated.ListNode;

public class MergeSortedLinkedList {

    //1->2->4, 1->3->4

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) {
            return null;
        }
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }
        ListNode root = l1.val < l2.val ? l1 : l2;
        ListNode begin = root;
        ListNode first = l2.val > l1.val ? l2 : l1;
        ListNode second = root.next;

        while (first != null || second != null) {
            if (first != null && second != null) {
                if (first.val < second.val) {
                    begin.next = first;
                    first = first.next;
                } else {
                    begin.next = second;
                    second = second.next;
                }
            } else if (first == null) {
                begin.next = second;
                second = second.next;
            } else {
                begin.next = first;
                first = first.next;
            }

            begin = begin.next;

        }

        return root;
    }
}
