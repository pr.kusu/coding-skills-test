package coding.test.amazon.practice;

import java.util.*;

public class KFrequent {

    class Node {
        String s;
        int freq;

        public Node(String s, int freq) {
            this.s = s;
            this.freq = freq;
        }
    }

    public List<String> topKFrequent(String[] words, int k) {
        if (words.length == 0) {
            return Collections.emptyList();
        }

        Map<String, Integer> freqTable = new HashMap<>();

        for (String s : words) {
            freqTable.put(s, freqTable.getOrDefault(s, 0) + 1);
        }

        PriorityQueue<Node> priorityQueue = new PriorityQueue<>(new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                if (o2.freq == o1.freq) {
                    return o2.s.compareTo(o1.s);
                }

                return Integer.compare(o2.freq, o1.freq);
            }
        });

        for (Map.Entry<String, Integer> entry : freqTable.entrySet()) {
            String word = entry.getKey();
            Integer freq = entry.getValue();

            priorityQueue.add(new Node(word, freq));
        }

        List<String> result = new ArrayList<>();

        for (int i = 0; i < k; i++) {
            result.add(priorityQueue.poll().s);
        }
        return result;
    }
}
