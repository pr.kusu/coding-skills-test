package coding.test.amazon.practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkBreakII {

    public List<String> wordBreak(String s, List<String> wordDict) {

        return dfs(s, wordDict, new HashMap<>());
    }

    public List<String> dfs(String s, List<String> dict, Map<String, List<String>> memo) {
        if (memo.containsKey(s)) {
            return memo.get(s);
        }
        //s = "catsanddog"
        //wordDict = ["cat", "cats", "and", "sand", "dog"]
        List<String> result = new ArrayList<>();

        for (String word : dict) {
            if (s.startsWith(word)) {
                if (s.length() == word.length()) {
                    result.add(word);
                    continue;
                }

                List<String> right = dfs(s.substring(word.length()), dict, memo);

                for (String r : right) {
                    result.add(word + " " + r);
                }
            }
        }
        memo.put(s, result);

        return result;
    }
}
