package coding.test.amazon.practice;

import coding.test.leetcode.curated.ListNode;

public class PalindromeList {

    public boolean isPalindrome(ListNode head) {
        ListNode rev = reverse(head);

        ListNode sec = rev;
        ListNode first = head;

        while (sec != null) {
            if (sec.val != first.val) {
                return false;
            }
            sec = sec.next;
            first = first.next;
        }

        return true;
    }

    public ListNode reverse(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;

        while (fast!=null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }

        ListNode start = slow;
        ListNode prev = null;

        //1->2->3
        while (start != null) {
            ListNode tmp = start.next;
            start.next = prev;
            prev = start;
            start = tmp;
        }

        return prev;

    }
}
