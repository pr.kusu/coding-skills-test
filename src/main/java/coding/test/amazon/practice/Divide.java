package coding.test.amazon.practice;

public class Divide {

    public int divide(int dividend, int divisor) {
        long divd = dividend;
        long divi = divisor;

        divd = Math.abs(divd);
        divi = Math.abs(divi);
        long count = 0;

        while (divd > divd) {
            divd -= divi;
            count++;
        }

        return (int) count;
    }
}
