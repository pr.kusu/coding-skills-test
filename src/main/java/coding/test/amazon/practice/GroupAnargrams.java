package coding.test.amazon.practice;

import java.util.*;

public class GroupAnargrams {

    public List<List<String>> groupAnagrams2(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();

        for (String s : strs) {
            char[] chars = s.toCharArray();

            String key = getKey(chars);

            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<>());
            }

            map.get(key).add(s);
        }

        List<List<String>> result = new ArrayList<>();

        result.addAll(map.values());

        return result;
    }

    private String getKey(char[] chars) {
        int[] freq = new int[26];

        for (int i = 0; i < chars.length; i++) {
            freq[chars[i] - 'a']++;
        }
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < freq.length; i++) {
            for (int j = 0; j < freq[i]; j++) {
                builder.append('a'+i);
            }
        }

        return builder.toString();
    }

    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();

        for (String s : strs) {
            char[] chars = s.toCharArray();
            Arrays.sort(chars);
            String key = new String(chars);

            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<>());
            }

            map.get(key).add(s);
        }

        List<List<String>> result = new ArrayList<>();

        result.addAll(map.values());

        return result;
    }
}
